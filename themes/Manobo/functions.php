<?php

/* ----------------------------------------------- [ #1 : Don't remove this ] ------------------------*/

require('base/base.php');
include('lib/responsive.php');

register_nav_menu( 'primary', __( 'Primary Menu', 'atom' ) );
register_nav_menu( 'footer', __( 'Footer Menu', 'atom' ) );

add_theme_support( 'post-thumbnails', array( 'post', 'page', 'slider' ) );
add_theme_support( 'automatic-feed-links' );

/* ----------------------------------------------- [/ #1 ] ------------------------*/

function manobo_enqueue(){
	wp_enqueue_style("supersized",THEME_URL."/css/supersized.css");
	wp_enqueue_style("shutter",THEME_URL."/css/supersized.shutter.css");

	wp_enqueue_script("supersized",THEME_URL."/js/supersized.3.2.7.min.js");
	wp_enqueue_script("shutter",THEME_URL."/js/supersized.shutter.min.js");
	wp_enqueue_script("easing",THEME_URL."/js/jquery.easing.min.js");
	wp_enqueue_script("custom",THEME_URL."/js/custom.js");
	wp_enqueue_script("addthis","//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-508e46b34d4b1cfe");
}
add_action('wp_enqueue_scripts', 'manobo_enqueue');

add_action('wp_footer','slider_script');
function slider_script(){
	?>
	<script>
	jQuery(function($){	
		$.supersized({	
			// Functionality
			slide_interval          :   5000,		// Length between transitions
			transition              :   1, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
			transition_speed		:	1000,		// Speed of transition
													   
			// Components							
			slide_links				:	'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
			slides 					:  	
				[			// Slideshow Images
				<?php 
				query_posts("post_type=slider"); 
				if(have_posts()) : while(have_posts()): the_post(); ?>
					{ image : "<?php get_featured_image('post_id='. get_the_ID() .'&size=full&display=0') ?>", 
					 title : "<?php the_title() ?>", 
					 thumb : "<?php get_featured_image('post_id='. get_the_ID() .'&size=full&display=0'); ?>", 
					 url : "<?php the_permalink() ?>"}, <?php
				endwhile; endif; wp_reset_query();
				?>
				]			
		});			
	});
	<?php if(is_home()): ?>
	jQuery(window).scroll(function (){
		if(jQuery(this).scrollTop()>400){
			jQuery('#featured-nav, #progress-back').fadeOut(1000);
			jQuery('#header').stop().animate({
				top:parseInt(screen_height+10)
			},700,'easeOutExpo');			
		}else{
			jQuery('#header').stop().animate({
				top:"-15"
			},700,'easeOutExpo');
			jQuery('#featured-nav, #progress-back').fadeIn(800);
		}
	});	
	<?php endif; ?>

	</script>
	<?php
}

// Start Slider Meta Box
add_action( 'add_meta_boxes', 'slider_add_custom_boxes' );
add_action( 'save_post', 'slider_save_postdata' );
function slider_add_custom_boxes() {
    $screens = array( 'slider' );
    foreach ($screens as $screen) {
        add_meta_box(
            'slider_text_position',
            __( 'Slider Text Position', 'base' ),
            'slider_inner_custom_box',
            $screen
        );
    }
}
function slider_inner_custom_box( $post ) {
  	wp_nonce_field( plugin_basename( __FILE__ ), 'myplugin_noncename' );
  	$value = get_post_meta( $post->ID, 'text_position', true ); 
  	$arr = array("left","right");
  	?>
  	<label for="myplugin_new_field"><?php _e("Set either left or right, where you want the text to be displayed on the homepage.", 'base' ); ?></label>

  	<select id="text_position" name="text_position">
  		<?php foreach($arr as $val): ?>
			<option value="<?php echo $val ?>" <?php if(esc_attr($value)==$val) echo "selected"; ?> ><?php echo ucwords($val) ?></option>
		<?php endforeach; ?>
	</select>
  <?php
}
function slider_save_postdata( $post_id ) {
  if ( 'page' == $_POST['post_type'] ) {
    if ( ! current_user_can( 'edit_page', $post_id ) ) return;
  } else {
    if ( ! current_user_can( 'edit_post', $post_id ) ) return;
  }
  if ( ! isset( $_POST['myplugin_noncename'] ) || ! wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename( __FILE__ ) ) )
      return;

  $post_ID = $_POST['post_ID'];
  $mydata = sanitize_text_field( $_POST['text_position'] ); //sanitize user input
  update_post_meta($post_ID, 'text_position', $mydata);
}
// end Slider Meta Box

// Panel Options
function add_panel_option(){
	ob_start();
	?>
	<ol>
		<li>Wordpress Installation</li>
		<li>WP Ecommerce</li>
	</ol>
	<?php
	$requirements = ob_get_contents();
	ob_end_clean();

	$arr2=array(
		'Documentation'=>array(
			0=>array(
				'Requirements'=>array(
					'type'=>'docs',
					'content'=>$requirements
				),
			)
		)
	);
	return $arr2;
}

function product_modal(){ 
	global $post;
	?>
	<div id="product-details" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div id='modal-body'>
			<?php
			if(isset($_REQUEST['pid'])):
				query_posts("post_type=wpsc-product&p=".$_REQUEST['pid']);
				if(have_posts()) : while(have_posts()): the_post();					
					$meta = get_post_meta($post->ID);								
					?><div class='orig-details' style="display:none;"><?php the_content(); ?></div>
				  <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>			    
				    <h3 id="product-name"><?php the_title() ?></h3>				    
				  </div>			  
				  <div class="modal-body">				  	
				  	<div style='float:left'>
					  	<img id='big-thumb' width="250" height="250" style='min-height:250px;display: block;margin:auto;' src="<?php echo wpsc_the_product_thumbnail(250,250,'','single'); ?>" alt="<?php echo get_the_title($_REQUEST['pid']) ?>" />					
					  	<div class='thumbs'>
						  	<?php
						  	$args = array(
							    'post_type' => 'attachment',
							    'numberposts' => null,
							    'post_status' => null,
							    'post_parent' => $post->ID
							);
							$attachments = get_posts($args);							
							if ($attachments) {
							    foreach ($attachments as $attachment) {
							        echo wp_get_attachment_link($attachment->ID, array(30,30),false,true);
							    }
							}
							?>						
						</div>
				  	</div>

				  	<div style='float:right; width:320px; '>
				  		<?php remove_filter( 'the_content', 'wpsc_single_template',12 ); ?>
				  		<?php the_excerpt();  ?>
				  		
				  		<div class="alert alert-success" style="display:none;">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Well done!</strong> This product has been added to cart successfully! View <a href='products-page/'>cart</a>.
						</div>

				  		<label for="qty" style="display: inline-block;">Qty: </label> <input type='text' value="1" id="qty" name="qty" style="width: 20px;text-align: center; margin: 0 20px;" />
						<h3 class='price' style="display: inline-block; font-size: 38px; vertical-align: bottom; margin: 0; color:#0184C6;">$<?php echo $meta['_wpsc_price'][0] ?></h3>

						<button class="btn btn-primary btn-buy" style="margin: 0 0px 0 10px;" data-loading-text="adding to cart..">Add to Cart</button>							
						
					    <hr />
					   <!-- AddThis Button BEGIN -->
						<div class="addthis_toolbox addthis_default_style ">
						<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
						<a class="addthis_button_tweet"></a>
						<a class="addthis_button_pinterest_pinit"></a>
						<a class="addthis_counter addthis_pill_style"></a>
						</div>

						<!-- AddThis Button END -->
				  	</div>
				  
				  </div>			
				
				<?php endwhile; endif; 
			endif;
			?>
			
		</div>
	</div> <?php
}
?>