<?php
add_action('wp_head','responsive_head');
function responsive_head(){
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
}

add_action('wp_enqueue_scripts', 'responsive_css');
function responsive_css(){
	wp_enqueue_style("bootstrap-responsive",BASE_URL."/css/bootstrap-responsive.css");
	wp_enqueue_style("responsive",THEME_URL."/css/responsive.css");	
}
?>