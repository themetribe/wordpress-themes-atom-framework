<?php get_header(); ?>

	<section id="main">

		<div class='container'>

			<!-- Content -->
			<section id='content'>
				<?php if(have_posts()) : while(have_posts()): the_post(); 
				
					//get_template_part( 'content', get_post_format() ); 
					the_content();
				
				endwhile; endif; ?>
			</section>
			<!-- end Content -->

			<!-- Sidebar -->
			<?php get_sidebar() ?>
			<!-- end Sidebar -->

		</div>

	</section>

<?php get_footer(); ?>