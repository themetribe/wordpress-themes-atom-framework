<!DOCTYPE HTML>
<html lang="en">
<head>
	<title><?php atom_title() ?></title>
	<?php atom_description(); ?>
	<?php atom_keywords(); ?>
	<?php atom_canonical(); ?>
	<meta charset="utf-8">
	<meta name="author" content="Themetribe">
	
	<?php wp_head(); ?>
	
	<link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/base/css/bootstrap.css" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />	
	
</head>
<body <?php body_class(); ?>>
	<section id="header">
		<div class="container">
			<!-- SITE LOGO and Description -->
			<section id='branding'>
				<div class='row'>
					<div class='span5'>
						<?php site_title_logo(); ?>
						<?php site_description(); ?>
					</div>	
					<!-- MENU -->
					<nav id="access" class='span7'>
						<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'sf-menu', 'container'=>false ) ); ?>
					</nav>	
					<!-- end Menu -->			
				</div>
			</section>
		</div>
	</section>

	
	<!--Arrow Navigation-->
	<a id="prevslide" class="load-item"></a>
	<a id="nextslide" class="load-item"></a>
	
	<!--Time Bar-->
	<div id="progress-back" class="load-item">
		<div id="progress-bar"></div>
	</div>
	

