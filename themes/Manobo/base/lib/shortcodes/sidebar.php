<?php
function atom_sidebar($atts=NULL, $content = NULL){
	$s='';
	global $type;
	extract($atts);
	$s=sidebar_format($type, $name);
	return $s;
}
add_shortcode('sidebar','atom_sidebar');
function sidebar_format($type, $name){ 
	ob_start();
	?>
	<section class="sidebar <?php echo $type ?>" role="complementary">
        <ul>
        <?php if ( ! dynamic_sidebar( $name ) ) : ?>
            <aside id="archives" class="widget-container">
                <h3 class="widget-title"><?php _e( 'Archives', 'twentyeleven' ); ?></h3>
                <ul>
                    <?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
                </ul>
            </aside>
    
            <aside id="meta" class="widget-container">
                <h3 class="widget-title"><?php _e( 'Meta', 'twentyeleven' ); ?></h3>
                <ul>
                    <?php wp_register(); ?>
                    <li><?php wp_loginout(); ?></li>
                    <?php wp_meta(); ?>
                </ul>
            </aside>
    
        <?php endif; // end sidebar widget area ?>
        </ul>
    </section>
	<?php
	$output = ob_get_contents();
	ob_end_clean();
	add_action('wp_footer', 'js_sidebar');
	return $output;
}
function js_sidebar($x){ 
	global $type;
	?>
	<script>		
		<?php if($type=="left") { ?>
			jQuery("#content *:not(.sidebar, .sidebar *, #content .entry *, .page-navi *)").width('620').css({'float':'right'});
		<?php }
		else { ?>
			jQuery("#content *:not(.sidebar, .sidebar *, #content .entry *, .page-navi *)").width('620');
		<?php } ?>
	</script>
	<?php
}
?>