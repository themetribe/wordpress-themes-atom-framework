<?php get_header(); ?>

	<section id="main">

		<div class='container'>

			<!-- Content -->
			<section id='content'>
				<?php if(have_posts()) : while(have_posts()): the_post(); ?>
					<div class='entry-thumb'>
						<?php get_featured_image("post_id=".get_the_ID()."&size=thumbnail&h=200&w=200") ?>
					</div>
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>
						<header class="entry-header">
							<h3 class="entry-title"><?php the_title(); ?></h3>
						</header>
						<div class="entry-meta">
							<div class='posted-on'>
								<span class="lbl">Posted on: </span> <?php post_meta_date() ?>
								<a href='<?php the_permalink() ?>#comment'><?php comments_number(); ?></a>
							</div>
						</div>
						<div class="entry-content">							
							<?php the_content(); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'twentytwelve' ), 'after' => '</div>' ) ); ?>
						</div><!-- .entry-content -->
						<footer class="entry-meta">
							<?php edit_post_link( __( 'Edit', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?>
						</footer><!-- .entry-meta -->
					</article><!-- #post -->
					<?php
					comments_template( '', true );
				endwhile; endif; ?>
			</section>
			<!-- end Content -->

			<!-- Sidebar -->
			<?php get_sidebar() ?>
			<!-- end Sidebar -->

		</div>

	</section>

<?php get_footer(); ?>