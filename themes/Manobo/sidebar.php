<section id='sidebar' role="complementary">
	<?php if ( is_active_sidebar( 'sidebar-widget-area' ) ) : ?>
		<ul class="widget-area">
			<?php dynamic_sidebar( 'sidebar-widget-area' ); ?>
		</ul><!-- #secondary -->
	<?php endif; ?>
</section>