<?php get_header() ?>
<section id="featured-details" class="container">
	<div class="row">
		<div class="span12">
			<?php 
			query_posts('post_type=slider');
			if(have_posts()) : while(have_posts()): the_post();	
			$text_position = " ".get_post_meta($post->ID,"text_position",true); ?>			
			<div class="entry<?php echo $text_position ?>">
				<h4 class='title'><?php the_title() ?></h4>
				<div class='excerpt'>
					<?php the_excerpt() ?>
				</div>
			</div>
			<?php endwhile; endif; wp_reset_query(); ?>
		</div>
	</div>
</section>

<section id="featured-nav" class="row">
		<span id='what_we_do'>This Is What We Do... </span>
		<a href="#">Coding</a>
		<a href="#">Graphics</a>
		<a href="#">Animation</a>
		<a href="#">SEO</a>
		<p id="generator">Manobo Copyright 2013</p>
</section>

<section id="main" class='clearfix'>
	<div class='container'>
		<div id='tagline' class='row'>
			<h2 class='span12'>Hurry! Free Shippings On All Orders. <small>Promo ends May 15th</small></h2>
		</div>
		<div id='products' class='row'>
			<h3 class='span12'>Our Latest Products</h3>
			<?php 
			$args = 'paged='.get_query_var( 'paged' ).'&'.
					'posts_per_page=8&post_type=wpsc-product';
			query_posts($args);
			if(have_posts()) : while(have_posts()): the_post(); ?>
				<div class='span3'>
					<a href="#product-details" role="button" data-toggle="modal" class='thumb animated'>
						<span class="pid" style='display:none'><?php the_ID() ?></span>
						<span class="frame"></span>
						<span class='view animated'>View</span>
    					<img src="<?php echo wpsc_the_product_thumbnail(220,170,'','single'); ?>" alt="<?php the_title();?>"/>
	                </a>
				</div>
			<?php endwhile; endif; wp_reset_query(); ?>
		</div>		

		<div id='page-navi' class='row'>
			<?php atom_pagination(); ?>
		</div>
		<?php product_modal() ?>
	</div>
</section>
<?php get_footer(); ?>
