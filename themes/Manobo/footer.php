	<section id="footer">
		<div id='footer-widgets'>	
			<div class='container'>	
				<div class='row-fluid'>	
				<?php 
					$arr = array('first','second','third','fourth');
					foreach($arr as $key=>$val):
						if ( is_active_sidebar( $val.'-footer-widget-area' ) ) : ?>
						<div class="<?php echo $val ?> widget-area span3">
						  	<ul class="xoxo">
						    	<?php dynamic_sidebar( $val.'-footer-widget-area' ); ?>
						  	</ul>
						</div>
					<?php endif; endforeach; ?>
				</div>
			</div>
		</div>
		<div id="site-generator">
			Designed by <a href="http://themetribe.com">Themetribe</a> | Powered by <a href="http://wordpress.org">Wordpress</a> 
	   	</div>  
	   	<nav id="footer-menu">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'sf-menu', 'container'=>false, 'depth'=>1 ) ); ?>
		</nav>	
	</section>

	
</body>
<?php wp_footer() ?>
</html>