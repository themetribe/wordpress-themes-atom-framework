jQuery(document).ready(function($){	
	// [ B E G I N I N G ]
	// [ Header Slide ]

	screen_height = $(window).innerHeight();

	jQuery('#header').css({
		position: "absolute",
		top: "-82px",
		opacity: 0.25
	}).animate({
		opacity: 1,
		top:"-15px"
	}
	 ,700,function(){

	// [ Featured Animation ]
	 	jQuery('#featured-details .title').animate({
	 		left: "0px",
	 		opacity: 1
	 	},400,function(){
	 		jQuery('#featured-details .entry').fadeIn(300);
	 	});		
	 }).delay(500).css({position:'relative'}
	);	

	// [ Footer Slide ]
	jQuery('#featured-nav').css({
		bottom: "-160px",
		opacity: 0.25
	}).animate({
		opacity: 1,
		bottom: "0px"
	},800);

	// [ M E N U ]

	jQuery("ul.sf-menu ul li a").hover(
		function () {
			jQuery(this).stop().animate({paddingLeft: 30}, 200);
		},
		function () {
			jQuery(this).stop().animate({paddingLeft: 20}, 500);
		}
	);


	// [ S L I D E R ]
	// supersized is in functions.php	
	header_height = $("#header").outerHeight();
	featured_top = $("#featured-details").css("marginTop");

	$('#featured-details .span12').cycle({
		fx:'fade', 
		delay:1,
		timeout:  5000,
		next:   '#nextslide', 
    	prev:   '#prevslide',
		before:function(){$(this).find(".excerpt p").animate({left:"-500px"},1000).delay(500)},
		after:function() {$(this).find(".excerpt p").stop().animate({left:"0px",opacity:1})}		
	});
	
	main_top = parseInt(screen_height) - parseInt(header_height) - parseInt(featured_top);
	$("#main").css({marginTop:main_top+"px"});

	// [ SCREEN EFFECT FOR BG IMAGE ]
	$("#supersized").prepend("<span id='screen_bg'></span>");

	// LATEST PRODUCTS [ HOMEPAGE ]
	$("#products .thumb").hover(function(){
		$(this).find('.frame').fadeTo('slow',0.4);
		$(this).find('.view').fadeIn().removeClass('fadeOutDown').addClass('fadeInUp');
	}, function(){
		$(this).find('.frame').fadeTo('slow',0.1);
		$(this).find('.view').removeClass('fadeInUp').addClass('fadeOutDown');
	});

	// SIDEBAR
	$('#sidebar .widget-container ul').not('.recentcomments, .tf_recent_posts, .ads_125x125').find('a').prepend("<span class='list-pre'>»</span> ").hover(function() {
		$(this).stop().animate({marginLeft:'5px'},'fast');
	},function(){
		$(this).stop().animate({marginLeft:'0'},'fast');
	});
	$('#site-logo').addClass('animated bounceInUp');


	// COMMENT SECTION
	$(".comment-reply-link").addClass('blue');

	$("#products .thumb").on('click',function(){
		var data="pid="+$(this).find('.pid').html();
		$("#product-details").html("<img class='loader' style='margin:20px auto; display:block;' src='http://64.19.142.13/theme.themetribe.com/manobo/wp-content/themes/Manobo/images/progress.gif' />");
		$("#product-details").load(" #modal-body",data,function(){
			var script = '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-508e46b34d4b1cfe';
			if (window.addthis) {
			    window.addthis = null;
			    window._adr = null;
			    window._atc = null;
			    window._atd = null;
			    window._ate = null;
			    window._atr = null;
			    window._atw = null;
			}
			$.getScript(script);

			$("#qty").on('change',function(){
		 		console.log($(this).val());
		 		$(".wpsc_quantity_update input[name='wpsc_quantity_update']").val($(this).val());
		 	});
			$(".btn-buy").on('click',function(){
			 	$(".wpsc_buy_button").trigger('click');			 	
			 	var _this = $(this);
			 	_this.button('loading');
			 	var intervalHandle = null;
			 	intervalHandle = setInterval(function () {
			 		if($(".wpsc_loading_animation").css('visibility','hidden')){
			 			_this.button('reset');
			 			$('.alert').fadeIn();
			 			clearInterval(intervalHandle);
			 		}
		        }, 1000)
			});
			$("#modal-body .thumbs a").on('click',function(e){
				_src = $(this).attr('href');
				_title = $(this).attr('title');
				console.log(_src);
				console.log(_title);
				$("#big-thumb").fadeOut(function(){
					console.log(_src);
					console.log(_title);
					$(this).attr({
						src: _src,
						title: _title
					});
					$(this).fadeIn();
				});
				e.preventDefault();
				return false;
			});				
		});
	});	
	
	// OVERIDE Checkout Page
	$(".entry-content, .excerpt").find('table').addClass('table');

	$(".checkout_cart").addClass('table table-bordered');
	$("#checkout_page_container input[type='submit']").addClass('btn');
	$(".wpsc_product_remove input[type='submit']").addClass('btn-danger');
	$(".wpsc_make_purchase input[type='submit']").addClass('btn-primary btn-large');
});

