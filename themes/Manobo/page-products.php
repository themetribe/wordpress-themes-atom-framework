<?php
/** Template Name: Products Page  */
get_header(); ?>

<div id="main">
	<div class='container'>
		<div id="content" role="main">
			<?php
			if(have_posts()) : while(have_posts()): the_post();
				$page_title = get_the_title();
				$page_content = tt_get_the_content();
			endwhile; endif; ?>
			<div class="clearfix entry">        	
	            <div class="excerpt">
	                <h3 class="title"><?php echo $page_title ?></h3>                 
		            <div class="excerpt">
		                <?php echo $page_content ?>
		            </div>
		            <div id='products' class='row'>
					<?php 
					$args = 'paged='.get_query_var( 'paged' ).'&'.
							'post_type=wpsc-product';
					query_posts($args);
					if(have_posts()) : while(have_posts()): the_post(); ?>
						<div class='span3'>
							<h4 class='product-name'><?php the_title(); ?></h4>
							<a href="#product-details" role="button" data-toggle="modal" class='thumb animated'>
								<span class="pid" style='display:none'><?php the_ID() ?></span>
								<span class="frame"></span>
								<span class='view animated'>View</span>
		    					<img src="<?php echo wpsc_the_product_thumbnail(220,170,'','single'); ?>" alt="<?php the_title();?>"/>
			                </a>
						</div>
					<?php endwhile; endif; wp_reset_query(); ?>
						<div class='span3'>	
							<div class='more-soon'>
								<p>More Products Comming Soon...</p>
							</div>		    				
						</div>
					</div>
	            </div>
	        </div>
			
		</div>
	</div>
	<?php product_modal() ?>
</div>

<?php get_footer(); ?>