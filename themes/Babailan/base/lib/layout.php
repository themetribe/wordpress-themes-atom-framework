<?php

function site_title_logo(){
    if(get_atom_option('set_custom_logo')!='on') : ?>
        <h1 id="site-title">
            <span><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
        </h1>
    <?php 
    else: ?>
        <h1 id="site-logo">
            <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                <img src="<?php echo THEME_URL."/images/logo.png" ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
            </a>
        </h1>
    <?php
    endif;
}

function site_description(){ 
    ?> <small id="site-description"><?php bloginfo( 'description' ); ?></small> <?php 
}

function slug($str){return str_replace(' ','_',strtolower($str));}

function atom_pagination($pages = '', $range = 2, $label = ''){
    global $wp_query;
    $big = 999999999; // need an unlikely integer
    
    echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => '?paged=%#%',
        'current' => max( 1, get_query_var('paged') ),
        'total' => $wp_query->max_num_pages
    ) );
}

function post_meta_date(){
    global $post;
    ?>
            <span class="M"><?php echo esc_attr( get_the_date('M') ) ?></span>
            <span class="j"><?php echo esc_attr( get_the_date('j') ) ?>,</span>
            <span class="y"><?php echo esc_attr( get_the_date('Y') ) ?></span>
    <?php
}
function base_posted_by() {
    printf( __( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>', 'atom' ),
        esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
        sprintf( esc_attr__( 'View all posts by %s', 'atom' ), get_the_author() ),
        esc_html( get_the_author() )
    );
}
function base_posted_in($lbl=TRUE, $tag=TRUE){ 
    $show_sep = false;  
    /* translators: used between list items, there is a space after the comma */
    $categories_list = get_the_category_list( __( ', ', 'atom' ) );
    if ( $categories_list ): ?>
        <span class="cat-links">
            <?php 
            if($lbl){
                printf( __( '<span class="%1$s">Posted in</span> %2$s', 'atom' ), 'entry-utility-prep entry-utility-prep-cat-links', $categories_list );
            }
            else{
                printf( __( '%2$s', 'atom' ), 'entry-utility-prep entry-utility-prep-cat-links', $categories_list );
            }
            $show_sep = true; ?>
        </span>
    <?php endif; // End if categories ?>
    <?php
    if($tag) :
            /* translators: used between list items, there is a space after the comma */
            $tags_list = get_the_tag_list( '', __( ', ', 'atom' ) );
            if ( $tags_list ):
            if ( $show_sep ) : ?>
        <span class="sep"> | </span>
            <?php endif; // End if $show_sep ?>
        <span class="tag-links">
            <?php printf( __( '<span class="%1$s">Tagged</span> %2$s', 'atom' ), 'entry-utility-prep entry-utility-prep-tag-links', $tags_list );
            $show_sep = true; ?>
        </span>
        <?php endif; // End if $tags_list 
    endif; 
}

function get_featured_image($args){

    $default = array(
        'post_id'    => '',
        'size'       => 'full',        
        'h'          =>  150,
        'w'          =>  150,
        'zc'          =>  1,
        'display'    =>  1
    );        
    $args = wp_parse_args($args,$default);

    extract($args);
    $ttatr = "h=$h&amp;w=$w&amp;zc=$zc";
    $img = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),$size);
    
        if($display==1){
            if(has_post_thumbnail($post_id)){
                if($size!='full'){
                    echo "<img src='".THEME_URL."/base/lib/timthumb.php?src=".$img[0]."&".$ttatr."' alt='".get_the_title()."' title='".get_the_title()."' >";    
                }
                else{
                    the_post_thumbnail();
                }
            }
            else{
                ?><div class='no-featured-image' style="width:<?php echo ($w-10) ?>px; height:<?php echo ($h-10) ?>px;">No Featured Image</div><?php
            }
        }
        else{
            if($size!='full')
                echo THEME_URL."/base/lib/timthumb.php?src=".$img[0]."&".$ttatr;
            else
                echo $img[0];
        }
}
function tt_truncate($content, $limit, $more='&hellip;') {
    if(strlen($content)<$limit)
        $more="";
    return substr_replace($content, '', $limit)."".$more;
}
function tt_get_the_content() {   
    ob_start();
    the_excerpt();
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}
function breadcrumbs(){ ?>
    <div id="breadcrumbs">
      <a href="<?php bloginfo('url'); ?>"><?php _e('Home','atom') ?></a> <span class="raquo">&raquo;</span>      
      <?php if( is_tag() ) { ?>
          <?php _e('Posts Tagged ','atom') ?><span class="raquo">&quot;</span><?php single_tag_title(); echo('&quot;'); ?>
      <?php } elseif (is_day()) { ?>
          <?php _e('Posts made in','atom') ?> <?php the_time('F jS, Y'); ?>
      <?php } elseif (is_month()) { ?>
          <?php _e('Posts made in','atom') ?> <?php the_time('F, Y'); ?>
      <?php } elseif (is_year()) { ?>
          <?php _e('Posts made in','atom') ?> <?php the_time('Y'); ?>
      <?php } elseif (is_search()) { ?>
          <?php _e('Search results for','atom') ?> <?php the_search_query() ?>
      <?php } elseif (is_single()) { ?>
          <?php $category = get_the_category();
                $catlink = get_category_link( $category[0]->cat_ID );
                echo ('<a href="'.$catlink.'">'.$category[0]->cat_name.'</a> '.'<span class="raquo">&raquo;</span> '.get_the_title()); ?>
      <?php } elseif (is_category()) { ?>
          <?php single_cat_title(); ?>
      <?php } elseif (is_author()) { ?>
          <?php _e('Posts by ','atom'); echo ' ',$curauth->nickname; ?>
      <?php } elseif (is_page()) { ?>
          <?php wp_title(''); ?>
      <?php }; ?>
    </div> <!-- end #breadcrumbs -->
    <?php 
}
?>