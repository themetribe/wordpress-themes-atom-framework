<?php
function atom_gmap($atts=NULL, $content = NULL){
	extract($atts);
	ob_start();
	/* atts:
	width
	height
	scroll = yes,no
	zoom
	address
	label
	*/
	if(!$width) $width = "100%";
	if(!$height) $height= 300;
	if(!$scroll) $scroll="yes";
	if(!$zoom) $zoom="10";
	
	if($latitude || $longitude){
		$src = "&q=".$latitude.",".$longitude;
	}
	elseif($address){
		$src="&q=".urlencode($address);
	}
	?>
    <iframe width="<?php echo $width ?>" height="<?php echo $height ?>" 
    	frameborder="0" scrolling="<?php echo $scroll ?>" marginheight="0" marginwidth="0" 
        src="http://maps.google.com/maps?&hl=en&t=m&z=<?php echo $zoom ?>&amp;output=embed<?php echo $src ?>">
   </iframe>
   <?php
	$s = ob_get_contents();
	ob_end_clean();
	return $s;
}
add_shortcode('gmap','atom_gmap');
?>