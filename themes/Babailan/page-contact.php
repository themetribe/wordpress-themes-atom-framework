<?php
/** Template Name: Contact Us Page  */
get_header();
if( isset($_POST['atom_contactform']) ){
		global $wpdb;
		
		extract($_POST);
		$name = $wpdb->escape( ${"contact_name_$rand"} );
		$email = $wpdb->escape( ${"contact_email_$rand"} );
		$message = $wpdb->escape( ${"contact_message_$rand"} . '<br /><br />Message sent by: <strong>' . ${"contact_name_$rand"}."</strong>");
		$admin_email = get_option("admin_email");
		
		$mail_status = wp_mail($admin_email, "A message from $name", $message, "Content-Type: text/html; charset=iso-8859-1 \r\nFrom: {$email}");				
	}	
	ob_start();
	$rand = rand(100,999);
	?>
<?php  ?>
<div id="main" class="clearfix row contact_page">
	<div id='modal-response' class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		    <h3 id="myModalLabel" style='text-align:center;'><?php echo ($mail_status) ? "Email Message Sent Successfully!" : "Sorry! Email Sending Failed. Please try again later."; ?></h3>
		  </div>
		  <div class="modal-footer">
		    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>		    
		  </div>
	</div>
	<div id="content" role="main" class="span8">
    	<?php
		if(have_posts()) : while(have_posts()): the_post(); ?>
		<div class="clearfix entry">        	
            <h3 class="title"><?php the_title(); ?></h3>                 
            <div class="excerpt">
                <?php the_content() ?>
            </div>
            <form name="atom_contactform" action="<?php the_permalink(); ?>" method="post" class="atom_contactform">
                <input type="hidden" name="rand" value="<?php echo $rand ?>" />
                <p><input type="text" required name="contact_name_<?php echo $rand ?>" id="contact_name_<?php echo $rand ?>" /> <label for="contact_name_<?php echo $rand ?>">Name <span class="required">*</span></label></p>
                <p><input type="text" required name="contact_email_<?php echo $rand ?>" id="contact_email_<?php echo $rand ?>" /> <label for="contact_email_<?php echo $rand ?>">Email <span class="required">*</span></label></p>
                <p><textarea required cols="30" rows="5" name="contact_message_<?php echo $rand ?>" id="contact_message_<?php echo $rand ?>"></textarea></p>
                <p><input type="submit" name="atom_contactform" class="btn btn_contct" value="Send" /></p>
            </form>
        </div>
		<?php endwhile; endif; ?>
    </div>
    <div class="span4 sidebar_def">
        <?php get_sidebar(); ?>
    </div>
</div>    
<?php get_footer();

if(isset($mail_status)) {
	?>
	<script>
		jQuery('#modal-response').modal('show');
	</script>
	<?php
}


?>