<?php get_header(); ?>
<section id="main" class="row single-page">
	<div class="container">
		<section id="content">
			<?php if(have_posts()) : while(have_posts()): the_post(); ?>
			<article class="clearfix entry">        	
				<h3 class="title"><?php the_title() ?></h3> 
				<div class="entry-meta">
					<div class='posted-on'>
						<span class="lbl">Posted on: </span> <?php post_meta_date() ?>
						<a href='<?php the_permalink() ?>#comment'><?php comments_number(); ?></a>
					</div>
				</div>
				<?php if(has_post_thumbnail()) : ?>
					<div class="thumb"><a href="<?php the_permalink(); ?>"><?php get_featured_image("post_id=".get_the_ID()."&size=large&h=200&w=630") ?></a></div>            
				<?php endif; ?>
				<div class="entry-content">
					<?php the_content();
					wp_link_pages(); ?>
				</div>
			</article>            
			<?php endwhile; endif; ?>                   
			<?php comments_template( '', true ); ?>         
		</section>
		<?php get_sidebar(); ?>	
	</div>
</section>
<?php get_footer(); ?>