<?php get_header(); ?>
<div id="main" class="clearfix row">	
	<div id="content" role="main" class="span8">    
    	<?php
		if(!is_archive()){
			query_posts( array( 'post_type' => 'post', 'paged' => get_query_var('paged') ) );
		}
		if(have_posts()) : while(have_posts()): the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>        	
            <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>                        
            <div class="meta"><?php base_posted_on(); echo " | "; base_posted_in(); ?></div>
            <?php if(has_post_thumbnail()) : ?>
            	<div class="thumb"><a href="<?php the_permalink(); ?>"><?php get_featured_image($post->ID,'large','h=180&w=294&zc=1') ?></a></div>            
            <?php endif; ?>
            <div class="excerpt">
            	<?php echo patico_truncate(patico_get_the_content(),500) ?>
            </div>
            <a class="atom_button gray small" href="<?php the_permalink() ?>">Read More</a>
        </div>
		<?php endwhile; endif; ?>
        <div class="page-navi clearfix">
            <?php atom_pagination(); ?>
        </div>
    </div>
    <div class="span4">
        <?php get_sidebar(); ?>
    </div>
</div>
<?php get_footer(); ?>