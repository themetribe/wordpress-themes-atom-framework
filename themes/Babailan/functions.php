<?php

/* ----------------------------------------------- [ #1 : Don't remove this ] ------------------------*/

define( 'THEME_NAME', 'babailan' );
define( 'SITE_DIR', get_template_directory() );
define( 'SITE_URL', get_template_directory_uri() );

require('base/base.php');

register_nav_menu( 'primary', __( 'Primary Menu', 'atom' ) );
register_nav_menu( 'primary', __( 'Primary Menu', 'atom' ) );
register_nav_menu( 'footer', __( 'Footer Menu', 'atom' ) );

add_theme_support( 'post-thumbnails', array( 'post', 'page', 'slider' ) );
add_theme_support( 'automatic-feed-links' );


if ( ! isset( $content_width ) ) $content_width = 650;
if ( is_singular() ) wp_enqueue_script( "comment-reply" );

#TITLE
function babailan_wp_title( $title, $separator ) {
	if ( is_feed() )
		return $title;
	global $paged, $page;

	if ( is_search() ) {
		$title = sprintf( __( 'Search results for %s', 'fjar' ), '"' . get_search_query() . '"' );
		if ( $paged >= 2 )
			$title .= " $separator " . sprintf( __( 'Page %s', 'fjar' ), $paged );
		$title .= " $separator " . get_bloginfo( 'name', 'display' );
		return $title;
	}

	$title .= get_bloginfo( 'name', 'display' );

	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title .= " $separator " . $site_description;

	if ( $paged >= 2 || $page >= 2 )
		$title .= " $separator " . sprintf( __( 'Page %s', 'babailan' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'babailan_wp_title', 10, 2 );

#EXCERPT
function custom_excerpt_length( $length ) {
	return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

#COMMENT.
function atom_comment( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' :
    ?>
    <li class="post pingback">
        <p><?php _e( 'Pingback:', 'twentyeleven' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?></p>
    <?php
            break;
        default :
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <article id="comment-<?php comment_ID(); ?>" class="comment clearfix">
            <footer class="comment-meta">
                <div class="comment-author vcard">
                    <span class="frame"></span>
                    <?php
                        $avatar_size = 83;
                        if ( '0' != $comment->comment_parent )
                            $avatar_size = 83;

                        echo get_avatar( $comment, $avatar_size );

                        /* translators: 1: comment author, 2: date and time */
                        printf( __( '%1$s <span class="comment-date">%2$s</span> ', 'atom' ),
                            sprintf( '<span class="fn">%s</span>', get_comment_author_link() ),
                            sprintf( '<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
                                esc_url( get_comment_link( $comment->comment_ID ) ),
                                get_comment_time( 'c' ),
                                /* translators: 1: date, 2: time */
                                sprintf( __( '%1$s at %2$s', 'atom' ), get_comment_date(), get_comment_time() )
                            )
                        );
                    ?>

                    <?php edit_comment_link( __( 'Edit | ', 'atom' ), '<span class="edit-link">', '</span>' ); ?>
                    <div class="comment-content"><?php comment_text(); ?></div>

                    <div class="reply">
                        <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'atom' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                    </div><!-- .reply -->
                </div><!-- .comment-author .vcard -->
                
                <?php if ( $comment->comment_approved == '0' ) : ?>
                    <em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'atom' ); ?></em>
                    <br />
                <?php endif; ?>

            </footer>
        </article><!-- #comment-## -->

    <?php
            break;
    endswitch;
}

$swag = new WP_Query();
$swag->query('post_type=custom_sidebar_area'); 
while ($swag->have_posts()) : $swag->the_post();    
    register_sidebar( array(
        'name' => __( get_the_title(), 'atom' ),
        'id' => "sidebar-{$post->post_name}",
        'description' => __( 'A custom sidebar widget area created from Sidebar Generator', 'atom' ),
        'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
endwhile;        

#COIN SLIDER
add_action('get_header','coin_include');
function coin_include(){
	wp_enqueue_script("coinslider",SITE_URL."/js/coin-slider.js",array("jquery"));
	wp_enqueue_style("coinslider",SITE_URL."/css/coin-slider.css");
}

#SIDEBARS
register_sidebar( array(
    'name' => __( 'Sidebar', 'atom' ),
    'id' => 'sidebar-footer-widget-area',
    'description' => __( 'The sidebar widget area', 'atom' ),
    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );
register_sidebar( array(
    'name' => __( 'First Footer Widget Area', 'atom' ),
    'id' => 'first-footer-widget-area',
    'description' => __( 'The first footer widget area', 'atom' ),
    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );
register_sidebar( array(
    'name' => __( 'Second Footer Widget Area', 'atom' ),
    'id' => 'second-footer-widget-area',
    'description' => __( 'The second footer widget area', 'atom' ),
    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );
register_sidebar( array(
    'name' => __( 'Third Footer Widget Area', 'atom' ),
    'id' => 'third-footer-widget-area',
    'description' => __( 'The third footer widget area', 'atom' ),
    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );
register_sidebar( array(
    'name' => __( 'Fourth Footer Widget Area', 'atom' ),
    'id' => 'fourth-footer-widget-area',
    'description' => __( 'The fourth footer widget area', 'atom' ),
    'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
    'after_widget' => '</li>',
    'before_title' => '<h3 class="widget-title">',
    'after_title' => '</h3>',
) );


#TESTIMONIALS.
function babailan_testi() {
	$labels = array(
		'name' => 'Testimonials',
		'singular_name' => 'Testimonials',
		'add_new' => 'Add New',
		'add_new_item' => 'Add New Testimonials',
		'edit_item' => 'Edit Testimonials',
		'new_item' => 'New Testimonials',
		'all_items' => 'All Testimonials',
		'view_item' => 'View Testimonial'
	);
	
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => true,
		'rewrite' => array( 'slug' => 'testi' ),
		'capability_type' => 'post',
		'has_archive' => true, 
		'hierarchical' => false,
		'menu_position' => null
	); 
	
	register_post_type('testimonials',$args);
}
add_action('init','babailan_testi');

#CYCLE and other JQUERY
function jsfoot() { ?>
	<script type="text/javascript">	
		jQuery(document).ready(function() {
			jQuery("#testimonials").cycle({next:'#testimonial-nav-r', prev:'#testimonial-nav-l',before: autoheight});
			jQuery("#bgimages").cycle({next:'#cs-next-slider', prev:'#cs-prev-slider',timeout:0});
			jQuery("#bgimages").cycle('prev');
			jQuery('#slider').coinslider({width: 954, height: 395});
			jQuery("#submit").addClass("btn");
		});
		function autoheight(curr, next, opts, fwd) {
			var index = opts.currSlide;
			var ht = jQuery(this).height();
			var wt = jQuery(this).width();
			jQuery(this).parent().animate({"height":ht,'width':wt});
		}		
		jQuery(document).ready(function(){
			jQuery(".wpsc_buy_button").addClass("btn atom_button excrpt-btn");
			jQuery(".sf-menu li:last-child a").css("border-right","0px")
			jQuery(".sf-menu li:first-child a").css("border-left","0px")
		});
	</script>
<?php } 
add_action('wp_footer', 'jsfoot');

#ICON url META
add_action( 'add_meta_boxes', 'icon_meta' );
add_action( 'save_post', 'icon_save_postdata' );

function icon_meta() {
    $screens = array( 'post', 'page' );
    foreach ($screens as $screen) {
        add_meta_box(
            'icon_id',
            __( 'Blurb Icon', 'icon_textdomain' ),
            'icon_inner_custom_box',
            $screen 
        );
    }
}

function icon_inner_custom_box( $post ) {
                                        
    wp_nonce_field( plugin_basename( __FILE__ ), 'icon_noncename' );

    $mydata_icon = get_post_meta($post->ID,'icon_field',true);
    echo '<label for="icon_field">';
        _e("Blurb Icon URL", 'icon_textdomain' );
    echo '</label> ';
    echo '<input id="icon_field" type="text" name="icon_field" value="'.esc_attr($mydata_icon).'" size="80" style="margin-left:10px;" /> <br />';
}

function icon_save_postdata( $post_id ) {

    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;

    if ( !wp_verify_nonce( $_POST['icon_noncename'], plugin_basename( __FILE__ ) ) )
        return;

    if ( 'page' == $_POST['post_type'] ){
        if ( !current_user_can( 'edit_page', $post_id ) )
            return;
    }else{
        if ( !current_user_can( 'edit_post', $post_id ) )
            return;
    }     
    
    $mydata_icon_o = get_post_meta($post->ID,'icon_field',true);
    $mydata_icon = $_POST['icon_field'];
    
    update_post_meta($post_id, icon_field, $mydata_icon, $mydata_icon_o);

}

?>