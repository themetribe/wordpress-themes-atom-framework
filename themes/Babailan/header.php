<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
	<title><?php wp_title( '|', true, 'right' );?></title>
	<meta charset="utf-8">
	<meta name="author" content="Themetribe">
	
	<?php wp_head(); ?>
	
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/base/css/bootstrap.css" />	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />	
	
</head>
<body <?php body_class(); ?>>
<div class="body-wrap">
	<div class="container bg-wrap">            
        <section id="header">		
			<section id='branding'>
                
                <!-- SITE LOGO and Description -->
				<div class='row'>
                    <div class="span6">
					    <div class='logo'>
						    <?php site_title_logo(); ?>											
                        </div>
					</div>
                    <div class="span6">
                        <?php if(get_atom_option('set_468x60_banner')=='on') : ?>
							<div id="ads_468X60" class="clearfix">
								<a href="http://themetribe.com"><img src="<?php echo bloginfo('template_url') ?>/images/ads_648x60.gif" /></a>
							</div>
						<?php else: ?>
						<?php endif; ?>
                    </div>
				</div><!-- end Site Logo and Description -->               
                
                <!-- MENU -->
                <div class='row'>
                    <div class="span3">
                        <?php site_description(); ?>
                    </div>
                    <div class="span9">
                        <nav id="access" class=''>
                            <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'sf-menu' ) ); ?>
                        </nav>    
                    </div>
                </div><!-- end Menu -->                
			</section>
        </section>
	</div> <!-- end CONTAINER -->	
            <?php if(is_home()){ 
            query_posts(array('category_name'=>get_atom_option('featured_post_category'),'posts_per_page'=>10,'order'=>'ASC'));
            //query_posts(array('post_type'=>'slider','posts_per_page'=>5,'order'=>'ASC'));
            if(have_posts()) :
                $feat_post_id=$permalink=array();                
                if(get_atom_option('display_featured_slider')=='on') : ?>
					<div id="featured" class="clearfix">
						<div class="overlay"></div>
						<div id="bgimages">                        
							<?php  
							$c=0; while(have_posts()): the_post(); if(has_post_thumbnail()) :
								$feat_post_id[$c] = get_the_ID();
								$permalink[$c] = get_permalink();
								get_featured_image("post_id=".get_the_ID()."&size=large&zc=1&f=8|8|8|8");							
								$c++;
							endif; endwhile; wp_reset_query(); ?>
						</div>
							<div id="sliderwrap" class="container">
								<div id="slider" class="container">
									<?php $c=0; foreach($feat_post_id as $val) : 
										  // $feat_post_id[$c] = get_the_ID(); ?>
										<a href="<?php echo $permalink[$c] ?>"><?php get_featured_image("post_id=".$val."&size=large&h=395&w=954"); ?></a>
									<?php $c++; endforeach; ?>
								</div>
							</div>
					</div>
				<?php else: ?>
				<?php endif; ?>
            <?php  endif; } ?>
    <div class="container  bg-wrap">
        
