<?php get_header(); ?>
    
    <div id="main" class="clearfix row err_page">
		<div id="content" role="main" class="span8">
			<?php
			if(have_posts()) : while(have_posts()): the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>        	
				<h3 class="title"><?php the_title() ?></h3>               
				<div class="excerpt">
					<?php the_content() ?>
					<h2>Page not Found</h2>
				</div>
			</div>
			<?php endwhile; endif; wp_reset_query(); ?>
		</div>
        <div class="span4 sidebar_err_page">
            <?php get_sidebar(); ?>
        </div>
    </div>
<?php get_footer(); ?>