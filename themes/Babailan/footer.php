    </div>    
    <div class="container bg-wrap">
		<footer id="footer" class="">        
			<section id="footer-widget" class="clearfix">
				<div class="row foot-in">
					<?php 
					  $arr = array('first','second','third','fourth');
					  foreach($arr as $key=>$val):
					  if ( is_active_sidebar( $val.'-footer-widget-area' ) ) : ?>
					<div class="<?php echo $val ?> widget-area span3">
						<ul class="xoxo">
							<?php dynamic_sidebar( $val.'-footer-widget-area' ); ?>
						</ul>
					</div>
					<?php endif; endforeach; ?>
				</div>
			</section>
			
			
			
				<section id="site-info" class="clearfix">        
					<section id="credits" class="">
					Powered by <a href="http://wordpress.com">Wordpress</a> and <a href="http://themetribe.com">Themetribe</a>
					</section>
				</section>
			
		</footer>
    </div>
</div>
</body>
<script type="text/javascript" src="<?php echo THEME_URL ?>/base/js/bootstrap.js"></script>
<?php wp_footer() ?>
</html>