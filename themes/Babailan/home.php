<?php get_header(); ?>
        <div id="main" class="clearfix">
            
			<?php
			$testi_loop = new WP_Query('post_type=testimonials'); 
			if($testi_loop->have_posts()) :
			?>
				<div id="testimonials-wrapper" class="element">
					<div class="row">            
						<nav class="nav span12">
							<a class="left" id="testimonial-nav-l">Prev</a>
							<a class="right" id="testimonial-nav-r">Next</a>
						</nav>               
						<div id="testimonials" class="">
							<?php while($testi_loop->have_posts()): $testi_loop->the_post(); ?>
								<div class="testimonial clearfix span10">
									<?php the_content(); ?>
								</div>
							<?php endwhile; ?>
						</div>                    
						<div class="clearfix"></div>
						<span class="title">Testimonials</span>      
					</div>				 
				</div>
			<?php endif; wp_reset_postdata();  ?> 
			<?php if(get_atom_option('set_blurbs')=='on') : ?>
				<div id="blurbs" class="clearfix element">         
					<div class="row blurb-in">
						<?php 
						$count=array("first"=>"1","second"=>"2","third"=>"3");
						foreach($count as $key=>$val):
						query_posts('showposts=1&post_type=page&page_id=' . get_atom_option("blurb_{$val}"));
						if(have_posts()) : while(have_posts()): the_post(); ?>        
						
						<div class="span4">
							<div class="blurb">
								<?php $icon = get_post_meta($post->ID,"icon_field");
								 //$subtitle = get_post_meta($post->ID,"Sub Title"); ?>
								<img class="blurb-thumb" src="<?php echo $icon[0]; ?>" />
								<h3 class="title"><?php the_title(); ?></h3>
								<h3 class="sub-title"><?php echo $subtitle[0] ?></h3>
								<div class="blurb-description">
									<?php the_excerpt(); ?>
								</div>
								<a class="atom_button btn" href="<?php the_permalink() ?>">Read More</a>
							</div>
						</div>
						<?php endwhile; endif; wp_reset_query(); endforeach; ?>
						<div class="offset10">
							<span class="title">Services</span> 
						</div>  
					</div>  
				</div>
			<?php else: ?>
			<?php endif; ?>
		</div>
<?php get_footer(); ?>