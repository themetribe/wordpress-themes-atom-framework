<?php /** Template Name: Full Width Page  */ ?>
<?php get_header(); ?>
<div class="row">
    <div id="main" class="clearfix full-width">	
        <div id="content" role="main" class="span12">
    	    <?php if(have_posts()) : while(have_posts()): the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>        	
					<h3 class="title"><?php the_title() ?></h3>               
					<div class="excerpt">
						<?php the_content() ?>
					</div>
				</div>
			<?php endwhile; endif; wp_reset_query(); ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>