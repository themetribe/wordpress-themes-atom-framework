<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Under Construction</title>
	<meta charset="utf-8">
	<meta name="author" content="Themetribe">
	
	<?php wp_head(); ?>
	
	<link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/base/css/bootstrap.css" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />	
	
</head>
<body <?php body_class(); ?>>

	<section id="header">
		<div class="container">

			<!-- SITE LOGO and Description -->
			<section id='branding'>
				<div class='row'>
					<div class='span12'>
						<?php site_title_logo(); ?>
					</div>					
				</div>
				<div class='row'>
					<div class='span12'>
						<?php site_description(); ?>
					</div>
				</div>
			</section>
			<!-- end Site Logo and Description -->

			<hr />

			<!-- MENU -->
			<div class='row'>
				<nav id="access" class='span12'>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'sf-menu' ) ); ?>
				</nav>	
			</div>
			<!-- end Menu -->

		</div>
	</section>
