
<?php get_header(); ?>

	<section id="main">

		<div class='container'>

			<!-- Content -->
			<section id='content'>
				<?php if(have_posts()) : while(have_posts()): the_post(); 
				
					//get_template_part( 'content', get_post_format() ); 
					get_template_part( 'content', 'page' ); 
				
				endwhile; endif; ?>
			</section>
			<!-- end Content -->

			<!-- Sidebar -->
			<section id='sidebar'>
				<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
					<div id="secondary" class="widget-area" role="complementary">
						<?php dynamic_sidebar( 'sidebar-1' ); ?>
					</div><!-- #secondary -->
				<?php endif; ?>
			</section>
			<!-- end Sidebar -->

		</div>

	</section>

<?php get_footer(); ?>