<?php
function atom_thead($atts=NULL, $content = NULL){
	return "<thead><tr>".do_shortcode($content)."</tr></thead>";
}
function atom_tfoot($atts=NULL, $content = NULL){
	return "<tfoot><tr><td colspan='99'>".do_shortcode($content)."</td></tr></tfoot>";
}
function atom_table($atts=NULL, $content = NULL){
	if($atts)
		extract($atts);
	$style = ($style) ? $style : "style1";
	ob_start();
	?>
    <table class="<?php echo $style ?>">
    	<?php echo do_shortcode($content) ?>
    </table>
    <?php
	$s = ob_get_contents();
	ob_end_clean();
	return $s;
}
add_shortcode('table','atom_table');
add_shortcode('thead','atom_thead');
add_shortcode('tfoot','atom_tfoot');
?>