<?php
add_action( 'add_meta_boxes', 'tt_blog_meta_box' );
add_action( 'save_post', 'tt_blog_save' );

function tt_blog_meta_box($post) {
    $screens = array( 'post', 'page' );
    foreach ($screens as $screen) {
        add_meta_box(
            'tt_blog_meta_box',
            __( 'Themetribe : Blog Settings', 'base' ),
            'tt_blog_inner_box',
            $screen,
            'side'
        );
    }
}
function tt_blog_inner_box($post){
	wp_nonce_field( plugin_basename( __FILE__ ), 'myplugin_noncename' );
	$tt_blog_type = get_post_meta( $post->ID,'tt_blog_type',true); 
	$arrs = array('Full Thumbnail Left Sidebar','Full Thumbnail Right Sidebar','Medium Thumbnail Left Sidebar', 'Medium Thumbnail Right Sidebar');
	?>	
	<label for='tt_blog_type'>Type:</label>
	<select id='tt_blog_type' name='tt_blog_type'>
		<?php foreach($arrs as $arr) {
		echo "<option value='".sanitize_title($arr)."'";
		if($tt_blog_type==sanitize_title($arr)) echo "selected='selected'";
		echo ">".$arr."</option>";
		} ?>
	</select>

	<?php
}
function tt_blog_save($post_id){
	if ( 'page' == $_POST['post_type'] ) {
	    if ( ! current_user_can( 'edit_page', $post_id ) )
	        return;
	} 
	else {
	    if ( ! current_user_can( 'edit_post', $post_id ) )
	        return;
	}
	if ( ! isset( $_POST['myplugin_noncename'] ) || ! wp_verify_nonce( $_POST['myplugin_noncename'], plugin_basename( __FILE__ ) ) )
      return;

	$tt_blog_type = sanitize_text_field( $_POST['tt_blog_type'] );
	update_post_meta($post_id, 'tt_blog_type', $tt_blog_type);	
}

// Hide meta box on all pages except for blog page template
//tt_blog_meta_box
function tt_blog_script(){ ?>
	<script>
		jQuery(document).ready(function($){
			$("#tt_blog_meta_box").hide();
			var page_template = $('#page_template').val();
		  	if (page_template == "page-blog.php")
		  		$("#tt_blog_meta_box").show();
		  	$('#page_template').on('change',function(){
		  		if (this.value == "page-blog.php")
		  			$("#tt_blog_meta_box").show(function(){
		  				$('#tt_blog_type').focus();
		  			});
		  		else
		  			$("#tt_blog_meta_box").fadeOut();
		  	});
		  	
		});
	</script>
	<?php
}
add_action('admin_head','tt_blog_script');