<?php get_header() ?>
<div id="header-banner" class="clearfix wrapper">
	<div id="header-banner-img">
    	<?php if(has_post_thumbnail()) { ?>
		<img src="<?php get_featured_image($post->ID,'large','h=138&w=951&zc=1',FALSE); ?>" />
        <?php } ?>
    </div>
    <div id="breadcrumb">
        <span>You are here:</span> 
        <?php breadcrumbs() ?>
    </div>
    <h2 id="page-title" class="title"><?php the_title() ?></h2>
    <?php atom_post_navi('post-navi','&laquo;','&raquo;'); ?>
</div>
<div id="main" class="wrapper clearfix">
	<div id="content">
    	
		<?php if(have_posts()) : while(have_posts()): the_post(); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class($add_class); ?>>            	
                <div class="entry-excerpt">
                    <?php the_content(); ?>
                </div>                
            </div>
            
			<?php comments_template( '', true ); ?>
            
        <?php endwhile; endif; ?>        
        
    </div>
    <?php get_sidebar(); ?>
</div>
<?php get_footer() ?>