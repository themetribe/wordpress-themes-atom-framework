<?php 
/** Template Name: Full*/ 
get_header() ?>

<div id="main" class="wrapper clearfix">
	<div id="content" class="full-width">    	
		<?php 		
		if(have_posts()) : while(have_posts()): the_post();
			$add_class=" big"; $feat_size='h=210&w=614&zc=1'; ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class($add_class); ?>>
            	<h2 class="title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
                <?php if(has_post_thumbnail()) : ?>
                    <div class="entry-thumb">
                        <span class="frame"></span>
                        <?php get_featured_image($post->ID,'large',$feat_size); ?>
                    </div>
                <?php endif; ?>                
                <div class="entry-excerpt">
                    <?php the_content(); ?>
                </div>
            </div>
        <?php endwhile; endif; ?>        
    </div>
</div>
<?php get_footer() ?>