<?php 
/** Template Name: Contact*/ 
if( isset($_POST['atom_contactform']) ){
	global $wpdb;
	
	extract($_POST);
	
	$name = $wpdb->escape( $contact_name_{$rand} );
	$email = $wpdb->escape( $contact_email_{$rand} );
	$message = $wpdb->escape( $contact_message_{$rand} . '\r\n\r\n' . $contact_name_{$rand});
	$admin_email = $atts['emailto'];
	
	mail($admin_email, "A message from $name", $message, "Content-Type: text/html; charset=iso-8859-1 \r\nFrom: {$email}");		
}

ob_start();
$rand = rand(100,999);
?>

<?php get_header(); ?>
<div id="main" class="clearfix wrapper">
	<div id="content" role="main">
    	<?php
		if(have_posts()) : while(have_posts()): the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class($add_class); ?>>       	
            <h2 class="title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
            <div class="entry-excerpt">
				<?php the_content(); ?>
            </div>
            <form action="<?php the_permalink(); ?>" method="post" class="atom_contactform" name="atom_contactform">
                <input type="hidden" name="emailto" value="<?php echo $atts['emailto'] ?>" />
                <input type="hidden" name="rand" value="<?php echo $rand ?>" />
                <p><input type="text" required name="contact_name_<?php echo $rand ?>" id="contact_name_<?php echo $rand ?>" /> <label for="contact_name_<?php echo $rand ?>">Name <span class="required">*</span></label></p>
                <p><input type="text" required name="contact_email_<?php echo $rand ?>" id="contact_email_<?php echo $rand ?>" /> <label for="contact_email_<?php echo $rand ?>">Email <span class="required">*</span></label></p>
                <p><textarea required cols="30" rows="5" name="contact_message_<?php echo $rand ?>" id="contact_message_<?php echo $rand ?>"></textarea></p>
                <p class="clearfix"><a class="atom_button" href="javascript: document.atom_contactform.submit();" >Submit <span>&rarr;</span></a></p>
            </form>
        </div>
		<?php endwhile; endif; ?>
    </div>
    <?php get_sidebar(); ?>
</div>    
<?php get_footer(); ?>