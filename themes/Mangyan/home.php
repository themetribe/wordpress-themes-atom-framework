<?php get_header() ?>
	<?php if(get_atom_option('display_featured_slider')=='on'): ?>    
		<?php if(!try_mobile()) : ?>
            <div id="featured" class="clearfix wrapper">
                <span class="overlay"></span>
                <?php query_posts(array('cat'=>get_atom_option('featured_category'),'order'=>'ASC')); ?>
                <div id="slider" class="nivoSlider">
                <?php 
                $num_of_posts = 5; 
                
                if(have_posts()) : while(have_posts()): the_post();
                    if(has_post_thumbnail() && $num_of_posts>=0) { 
                        ?><a href="<?php the_permalink() ?>"><?php get_featured_image($post->ID,'large','h=373&w=938&zc=1'); ?></a><?php 
                        $num_of_posts--;
                    }
                endwhile; endif; wp_reset_query(); ?>
                </div>
            </div>
        <?php else: //try_mobile() ?>
        	<div id="featured-container">
                <div id="featured" class="clearfix wrapper">
                    
                    <?php query_posts(array('cat'=>get_atom_option('featured_category'),'order'=>'ASC')); ?>
                    <div id="slider" class="nivoSlider">
                    <?php 
                    $num_of_posts = 5;
                    if(have_posts()) : while(have_posts()): the_post();
                        if(has_post_thumbnail() && $num_of_posts>=0) { 
                            
                            get_featured_image($post->ID,'full','h=353&w=300&zc=1');						
                            $num_of_posts--;
                        }
                    endwhile; endif; wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        <?php endif; //try_mobile() ?>
    <?php endif; //if(get_atom_option('display_featured_slider')=='on') ?>
    
    
	<div id="main" class="wrapper clearfix">
    	
		<?php if(!try_mobile()): ?>  
        
        <div id="content">
        	<?php if(get_atom_option("set_intro_page")=='on') : ?>
            <div id="intro-page" class="clearfix">
                <?php query_posts('showposts=1&post_type=page&page_id=' . get_atom_option("intro_page")); 
                if(have_posts()) : while(have_posts()): the_post(); ?>
                <h2 class="title"><?php the_title(); ?></h2>
                <div id="post-<?php the_ID(); ?>" <?php post_class($add_class); ?>>
                    <?php the_excerpt(); ?>
                </div>
                <?php endwhile; endif; wp_reset_query(); ?>
            </div>
            <?php endif; ?>
            
            <?php if(get_atom_option("set_blurbs")=='on') : ?>
            <div id="blurbs" class="clearfix">    
                <?php 
                $count=array("first"=>"1","second"=>"2","third"=>"3",'fourth'=>'4');
                foreach($count as $key=>$val):
                    query_posts('showposts=1&post_type=page&page_id=' . get_atom_option("blurb_{$val}"));
                    if(have_posts()) : while(have_posts()): the_post(); ?>        
                    <div class="blurb">
                        <?php $icon = get_post_meta($post->ID,"Icon"); ?>
                        <h3 class="title"><a href="#"><?php the_title(); ?></a></h3>
                        <img class="blurb-thumb" src="<?php echo $icon[0]; ?>" />
                        <div class="blurb-description">
                        <?php the_excerpt(); ?>
                        </div>
                    </div>
                <?php endwhile; endif; wp_reset_query(); endforeach; ?>
            </div>
            <?php endif; ?>            
        </div>
        <div id="sidebar">    	
            <div id="latest-posts">
                <h3 class="title">Latest News</h3>
                <?php query_posts('showposts=3&post_type=post'); if(have_posts()) : while(have_posts()): the_post(); ?>
                
                <div class="entry clearfix">
                	<a href="<?php the_permalink() ?>">
                    <div class="entry-thumb">
                        
                        <?php 
						if(has_post_thumbnail()) :
							get_featured_image($post->ID,'medium','h=70&w=108&zc=1'); 
						else : ?>
                        	<img src="<?php echo THEME_URL ?>/images/latest-news.png" />
                        <?php endif; ?>
                        
                    </div>
                    <h3 class="entry-title"><?php the_title(); ?></h3>
                    <div class="entry-excerpt">
                        <?php echo atom_truncate(atom_get_the_content(),84); ?>
                    </div>
                    </a>
                </div>
                
                <?php endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>
        
		<?php else: ?>
        
        <div id="content" class="clearfix">
        	<?php if(get_atom_option("set_intro_page")=='on') : ?>
            <div id="intro-page" class="clearfix hentry">
                <?php query_posts('showposts=1&post_type=page&page_id=' . get_atom_option("intro_page")); 
                if(have_posts()) : while(have_posts()): the_post(); ?>
                <h2 class="title"><a><?php the_title(); ?></a></h2>
                <div id="post-<?php the_ID(); ?>" <?php post_class($add_class); ?>>
                    <?php the_excerpt(); ?>
                </div>
                <?php endwhile; endif; wp_reset_query(); ?>
            </div>
            <?php endif; ?>
            
            <?php if(get_atom_option("set_blurbs")=='on') : ?>
            <div id="blurbs" class="clearfix">    
                <?php 
                $count=array("first"=>"1","second"=>"2","third"=>"3",'fourth'=>'4');
                foreach($count as $key=>$val):
                    query_posts('showposts=1&post_type=page&page_id=' . get_atom_option("blurb_{$val}"));
                    if(have_posts()) : while(have_posts()): the_post(); ?>        
                    <div class="blurb hentry">
                        <?php $icon = get_post_meta($post->ID,"Icon"); ?>
                        <h3 class="title"><a href="#"><img class="blurb-thumb" src="<?php echo $icon[0]; ?>" /><?php the_title(); ?></a></h3>
                        
                        <div class="blurb-description">
                        <?php the_excerpt(); ?>
                        </div>
                    </div>
                <?php endwhile; endif; wp_reset_query(); endforeach; ?>
            </div>
            <?php endif; ?>            
        </div>
        <div id="sidebar" class="hentry">    	
            <div id="latest-posts">
                <h3 class="title"><a>Latest News</a></h3>
                <?php query_posts('showposts=3&post_type=post'); if(have_posts()) : while(have_posts()): the_post(); ?>
                
                <div class="entry clearfix">
                	<a href="<?php the_permalink() ?>">
                    <div class="entry-thumb">
                        
                        <?php 
						if(has_post_thumbnail()) :
							get_featured_image($post->ID,'medium','h=70&w=108&zc=1'); 
						else : ?>
                        	<img src="<?php echo THEME_URL ?>/images/latest-news.png" />
                        <?php endif; ?>
                        
                    </div>
                    <h3 class="entry-title"><?php the_title(); ?></h3>
                    <div class="entry-excerpt">
                        <?php echo atom_truncate(atom_get_the_content(),84); ?>
                    </div>
                    </a>
                </div>
                
                <?php endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>
        
        <?php endif; ?>
        
    </div>
<?php get_footer() ?>