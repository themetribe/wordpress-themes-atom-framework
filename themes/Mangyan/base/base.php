<?php

$temp = get_theme_data(get_bloginfo( 'stylesheet_url' ));

define('THEME_NAME', $temp['Name']);
define('THEME_URL',get_bloginfo('template_url'));
define('BASE_URL',THEME_URL.'/base');
define('THEME_DIR',get_template_directory());
define('BASE_DIR',THEME_DIR.'/base');
define( 'SITE_DIR', get_template_directory() );


include('lib/layout.php');
include('lib/panel.php');
include('base-functions.php');

include('lib/meta-boxes/mb_blog.php');

include('lib/widgets/ads125x125.php');
include('lib/widgets/contact_info.php');
include('lib/widgets/contact_us.php');
include('lib/widgets/recent_post.php');

include('lib/shortcodes/blog.php');
include('lib/shortcodes/box.php');
include('lib/shortcodes/button.php');
include('lib/shortcodes/columns.php');
include('lib/shortcodes/contactform.php');
include('lib/shortcodes/gallery.php');
include('lib/shortcodes/gchart.php');
include('lib/shortcodes/gmap.php');
include('lib/shortcodes/image.php');
include('lib/shortcodes/shortcode_script.php');
include('lib/shortcodes/sidebar.php');
include('lib/shortcodes/slider.php');
include('lib/shortcodes/typography.php');
include('lib/shortcodes/video.php');

register_sidebar( array(
	'name' => __( 'Sidebar', 'base' ),
	'id' => 'sidebar-widget-area',
	'description' => __( 'The sidebar widget area', 'base' ),
	'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>'
) );

//  Footer Wiggets
$arrs = array('first','second','third','fourth');
foreach($arrs as $arr){
	register_sidebar( array(
		'name' => __( ucwords($arr).' Footer', 'base' ),
		'id' => $arr.'-footer-widget-area',
		'description' => __( 'The '.$arr.' footer widget area', 'base' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>'
	) );
}

// Slider Post Type
register_post_type('slider', array(
	'label' => __('Sliders'),
	'singular_label' => __('Slider'),
	'public' => true,
	'show_ui' => true,
	'capability_type' => 'post',
	'hierarchical' => false,
	'menu_position' => 9,
	'rewrite' => true,
	'supports' => array('title','editor','thumbnail')
));

?>