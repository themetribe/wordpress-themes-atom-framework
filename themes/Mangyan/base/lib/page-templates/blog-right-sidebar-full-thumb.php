<?php get_header(); ?>
	<section id="main" class='row blog-right-sidebar full-thumb'>
		<div class='container'>
			<?php 
			if(!is_archive())
				query_posts( array( 'post_type' => 'post', 'paged' => get_query_var('paged') ) );
			?>

			<!-- Content -->
			<section id='content'>
				<?php if(have_posts()) : while(have_posts()): the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?>>					
						<div class='entry-thumb'>
							<a href='<?php the_permalink() ?>'><?php get_featured_image("post_id=".get_the_ID()."&size=large&h=200&w=630") ?></a>
						</div>
						<div class="entry-header">
							<h2 class="entry-title"><a href='<?php the_permalink() ?>'><?php the_title(); ?></a></h2>
							<div class="entry-meta">
								<div class='posted-on'>
									<span class="lbl">Posted on: </span> <?php post_meta_date() ?>
									<a href='<?php the_permalink() ?>#comment'><?php comments_number(); ?></a>
								</div>
							</div>
						</div>
						<div class="entry-content">
							<?php echo tt_truncate( tt_get_the_content(), 200,'&hellip; <a href="'.get_permalink().'">Continue Reading &raquo; </a>' ); ?>
						</div><!-- .entry-content -->
						
					</article><!-- #post -->
					<?php				
				endwhile; endif; ?>
				<div class="page-navi clearfix">
					<?php atom_pagination(); ?>
				</div>
			</section>			
			<!-- end Content -->	
			<?php get_sidebar(); ?>	
		</div>
	</section>	
<?php get_footer(); ?>