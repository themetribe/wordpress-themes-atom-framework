<?php

function front_styles(){
    wp_enqueue_script('jquery');
	wp_enqueue_script("superfish",BASE_URL."/js/superfish.js");
    wp_enqueue_script("supersubs",BASE_URL."/js/supersubs.js");
    wp_enqueue_script("hoverIntent",BASE_URL."/js/hoverIntent.js");
    wp_enqueue_script("cycle",BASE_URL."/js/jquery.cycle.all.js");
    wp_enqueue_script("base",BASE_URL."/js/base.js");
    wp_enqueue_script("bootstrap",BASE_URL."/js/bootstrap.js");
    //wp_enqueue_script("jquerynivoslider",BASE_URL."/slider/jquery.nivo.slider.js");

    wp_enqueue_style( 'shortcodes', BASE_URL.'/css/shortcodes.css');
    wp_enqueue_style("superfish",BASE_URL."/css/superfish.css");
    wp_enqueue_style("animate",BASE_URL."/css/animate.css");
    wp_enqueue_style("base",BASE_URL."/css/base.css");
} add_action('wp_enqueue_scripts', 'front_styles');

add_filter('body_class','browser_body_class');
function browser_body_class($classes) {
    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
    if($is_lynx) $classes[] = 'lynx';
    elseif($is_gecko) $classes[] = 'gecko';
    elseif($is_opera) $classes[] = 'opera';
    elseif($is_NS4) $classes[] = 'ns4';
    elseif($is_safari) $classes[] = 'safari';
    elseif($is_chrome) $classes[] = 'chrome';
    elseif($is_IE) $classes[] = 'ie';
    else $classes[] = 'unknown';
    if($is_iphone) $classes[] = 'iphone';
    return $classes;
}