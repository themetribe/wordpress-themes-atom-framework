<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php atom_title() ?></title>
<?php atom_description(); ?>
<?php atom_keywords(); ?>
<?php atom_canonical(); ?>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<?php wp_head(); ?>

<style><?php atom_color_scheme(); ?><?php atom_typography(); ?></style>
</head>

<body <?php body_class(); ?>>

<?php if(get_atom_option('option_previewer')=='on' && !try_mobile()): ?>
<div id="theme-settings-preview">
	<a id="control-toggle" class="close"></a>
    <div class="background-pattern">
    	<h6>Background Pattern</h6>
        <ul>
        	<li><a href="#"><img src="<?php echo THEME_URL ?>/images/bg.png" /></a></li>
            <li><a href="#"><img src="<?php echo THEME_URL ?>/images/bg2.png" /></a></li>
            <li><a href="#"><img src="<?php echo THEME_URL ?>/images/bg3.png" /></a></li>
            <li><a href="#"><img src="<?php echo THEME_URL ?>/images/bg4.png" /></a></li>
            <li><a href="#"><img src="<?php echo THEME_URL ?>/images/bg5.png" /></a></li>
            <li><a href="#"><img src="<?php echo THEME_URL ?>/images/bg6.png" /></a></li>
            <li><a href="#"><img src="<?php echo THEME_URL ?>/images/bg7.png" /></a></li>
            <li><a href="#"><img src="<?php echo THEME_URL ?>/images/bg8.png" /></a></li>
        </ul>
    </div>    
    <a href="http://themetribe.com" >More Options Inside</a>
    <a href="http://themetribe.com" class="atom_button">Buy This Theme</a>
</div>
<?php endif; ?>

<?php if(!try_mobile()) : ?>
<header id="header">
	<nav id="access1" role="navigation">
    	<div class="wrapper clearfix">
    	<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'sf-menu', 'depth'=>1 ) ); ?>
        </div>
    </nav>
    
    <div id="header-top" class="wrapper clearfix">
        <hgroup id="branding">
            <?php if(get_atom_option('set_custom_logo')!='on') : ?>
            <h1 id="site-title">
                <span><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
            </h1>
            <?php else: ?>
            <h1 id="site-logo">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_atom_option('logo',get_bloginfo('template_url')."/images/logo.png") ?>" /></a>
            </h1>
            <?php endif; ?>
            <h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
        </hgroup>
        <?php if(get_atom_option('set_468x60_banner',"off")=="on") : ?>
        <div id="ads_468X60">            
	    	<a href="<?php echo get_atom_option('a_468x60','http://themetribe.com') ?>"><img src="<?php echo get_atom_option('img_468x60',get_bloginfo('template_url')."/images/ads_648x60.gif") ?>" /></a>
        </div>
        <?php endif; ?>
    </div>
    <nav id="access2" role="navigation">
	    <div class="wrapper clearfix">
    	<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'sf-menu', 'walker' => new description_walker()) ); ?>
        </div>
    </nav>
</header>
<?php else: //if(get_atom_option('enable_responsive')!='on') ?>
<nav id="menu" role="navigation" class="clearfix">
	<h2>Menu Nav</h2>
	<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'sf-menu', 'depth'=>1 ) ); ?>
</nav>
<div id="responsive">



<header id="header">
	
    <div id="menu-button">
    	<a>Menu Button</a>
    </div>
    <div id="header-top" class="wrapper clearfix">
        <hgroup id="branding">
            <?php if(get_atom_option('set_custom_logo')!='on') : ?>
            <h1 id="site-title">
                <span><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></span>
            </h1>
            <?php else: ?>
            <h1 id="site-logo">
                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_atom_option('logo',get_bloginfo('template_url')."/images/logo.png") ?>" /></a>
            </h1>
            <?php endif; ?>
            <h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
        </hgroup>        
    </div>
    
</header>

<?php endif; //if(get_atom_option('enable_responsive')!='on')