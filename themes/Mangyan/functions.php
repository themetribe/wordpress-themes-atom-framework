<?php
include('lib/config.php');
include('lib/panel/panel_func.php');
include('lib/widgets/ads125x125.php');
#Load Shortcode
include('lib/shortcodes/slider.php');
include('lib/shortcodes/columns.php');
include('lib/shortcodes/image.php');
include('lib/shortcodes/box.php');
include('lib/shortcodes/button.php');
include('lib/shortcodes/sidebar.php');
include('lib/shortcodes/blog.php');
include('lib/shortcodes/typography.php');
include('lib/shortcodes/contactform.php');
include('lib/shortcodes/gallery.php');
include('lib/shortcodes/gmap.php');
include('lib/shortcodes/gchart.php');
include('lib/shortcodes/table.php');
include('lib/shortcodes/video.php');
include('lib/shortcodes/shortcode_script.php');
remove_filter ('the_content', 'wpautop');
#Load Widgets
include('lib/widgets/recent_post.php');
include('lib/widgets/contact_info.php');
include('lib/widgets/contact_us.php');
# Load Side Generator
include('lib/side_generator.php');
/*============================[ Constructor ]===*/
function base_setup(){	
	register_nav_menu( 'primary', __( 'Primary Menu', 'atom' ) );
	register_nav_menu( 'secondary', __( 'Secondary Menu', 'atom' ) );
	add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );	
	add_theme_support( 'custom-background' );
	add_post_type_support('page', 'excerpt');
} add_action( 'after_setup_theme', 'base_setup' );
function front_styles(){	
	wp_enqueue_script("jquery",THEME_URL.'/lib/js/jquery-1.7.2.min.js');	
	wp_enqueue_script( 'hoverIntent', THEME_URL.'/lib/js/hoverIntent.js');
	wp_enqueue_script( 'superfish', THEME_URL.'/lib/js/superfish.js');		
	wp_enqueue_script( 'supersub', THEME_URL.'/lib/js/supersubs.js');	
	wp_enqueue_script( 'easing', THEME_URL.'/lib/js/easing.js');
	wp_enqueue_script( 'cycle', THEME_URL.'/lib/js/cycle.js');
	wp_enqueue_script( 'custom', THEME_URL.'/lib/js/custom.js');
	wp_enqueue_script( 'nivo', THEME_URL.'/lib/js/jquery.nivo.slider.pack.js');
	wp_enqueue_script( 'lightbox', THEME_URL.'/lib/js/lightbox.js');
	
	wp_enqueue_style( 'reset', THEME_URL.'/css/reset.css', array(), '2.0');
	wp_enqueue_style( 'superfish', THEME_URL.'/css/superfish.css');
	wp_enqueue_style( 'nivo-slider', THEME_URL.'/css/nivo-slider.css',array(),'2.7.1');		
	wp_enqueue_style( 'superfish', THEME_URL.'/css/shortcodes.css');
	wp_enqueue_style( 'shortcodes', THEME_URL.'/css/shortcodes.css');
	wp_enqueue_style( 'ptemplate', THEME_URL.'/css/ptemplate.css');
	wp_enqueue_style( 'lightbox', THEME_URL.'/css/lightbox.css');
	
	if(try_mobile()){
		wp_enqueue_style( 'responsive', THEME_URL.'/css/responsive.css');
		wp_enqueue_script( 'responsive', THEME_URL.'/lib/js/responsive.js');
	}
	
} add_action('wp_enqueue_scripts', 'front_styles');
function atom_excerpt_length( $length ) {return 55;}
add_filter( 'excerpt_length', 'atom_excerpt_length', 999 );
function try_mobile(){
	global $is_iphone, $is_ipad, $is_ipod, $is_mobile, $is_android;
	if((get_atom_option('enable_responsive')=='on')){
		//if($is_iphone || $is_ipad || $is_ipod || $is_mobile || $is_android){
			return true;
		//}
	}
}
function mobile_head(){
	if(try_mobile()){
		?><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">  
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">  
<meta name="apple-mobile-web-app-capable" content="yes">  
<meta name="apple-mobile-web-app-status-bar-style" content="black"><?php
	}
} add_filter('wp_head','mobile_head');
// For Admin
function admin_scripts(){
	wp_enqueue_script('thickbox',null,array('jquery'));	
} add_action('admin_print_scripts', 'admin_scripts');
function admin_styles(){
	wp_enqueue_style('thickbox.css', '/'.WPINC.'/js/thickbox/thickbox.css', null, '1.0');
} add_action('admin_print_styles', 'admin_styles');
add_theme_support( 'automatic-feed-links' );
/* SIDEBAR */
register_sidebar( array(
	'name' => __( 'Sidebar', 'atom' ),
	'id' => 'sidebar-footer-widget-area',
	'description' => __( 'The sidebar widget area', 'atom' ),
	'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );
/*============================[ HEADER ]===*/
/* atom_title() - This is responsible for <title> text display for SEO purpose. 
				- This can be changed in Theme Panel > SEO
				- Can be: 
					1. Home Title
						a) Blog Name | Blog Description 
						b) Blog description | BlogName
						c) BlogName only
					2. Single Title
						a) Blog Name | Post Title 
						b) Post title | BlogName
						c) Post title only
					3. Index Title (categories,archives,search results)
						a) BlogName | Category name
						b) Category name | BlogName
						c) Category name only */					
function atom_title(){
	$temp = '';
	$sep = get_atom_option('homepage_title_separator','|');
	$site_description = get_bloginfo('description');
	$blog_name=get_bloginfo( 'name' );
	if(is_home() || is_front_page()){		
		if(get_atom_option('homepage_set_custom_title')=="on"){
			$temp=get_atom_option("homepage_custom_title");
		}
		else {
			$temp_title = get_atom_option('homepage_title_type','blogname_|_blog_description');		
			if($temp_title=='blogname_|_blog_description')
				$temp=$blog_name.' '.$sep.' '.$site_description;
			elseif($temp_title=='blog_description_|_blogname')
				$temp=$site_description.' '.$sep.' '.$blog_name;
			else
				$temp=$blog_name;
		}
	}
	elseif(is_single() || is_page()){		
		$temp_title = get_atom_option('single_title',1);		
		if($temp_title==0){
			$temp = $blog_name . wp_title( $sep, false, 'left' );
		}
		elseif($temp_title==1){
			$temp = wp_title( $sep, false, 'right' ) . $blog_name ;
		}
		else{
			$temp=$blog_name;
		}		
	}
	else{
		$temp_title = get_atom_option('index_title',1);		
		if($temp_title==0){
			$temp = $blog_name . wp_title( $sep, false, 'left' );
		}
		elseif($temp_title==1){
			$temp = wp_title( $sep, false, 'right' ) . $blog_name ;
		}
		else{
			# Spacing issue here is solved on the trim() func on the nearest code.
			$temp=wp_title( '',false,'' );
		}	
	}
	echo trim($temp); 
}
function atom_description(){
	if(is_home() || is_front_page()){
		$temp = get_atom_option('homepage_set_meta_description');
		if($temp=="on") {
			?><meta name="description" content="<?php echo get_atom_option('homepage_meta_description') ?>" /><?php echo "\n";
		}
	}
	elseif(is_single() || is_page()){		
	}
	else{
	}
}
function atom_keywords(){
	if(is_home() || is_front_page()){
		$temp = get_atom_option('homepage_set_meta_keywords');
		if($temp=="on") {
			?><meta name="keywords" content="<?php echo get_atom_option('homepage_meta_keywords') ?>" /><?php echo "\n";
		}
	}
	elseif(is_single() || is_page()){		
	}
	else{
	}
}
function atom_canonical(){
	if(is_home() || is_front_page()){
		$temp = get_atom_option('homepage_set_canonical');
		if($temp=="on") {
			?><link rel="canonical" href="<?php echo get_bloginfo('siteurl') ?>" /><?php echo "\n";
		}
	}
	elseif(is_single() || is_page()){		
	}
	else{
	}
}
function atom_favicon(){
	$temp = get_atom_option("favicon");
	if($temp!=''):
		?><link rel="shortcut icon" href="<?php echo $temp ?>" /><?php echo "\n"; 
		?><link rel="icon" href="<?php echo $temp ?>" /><?php    
	endif;
} add_action('wp_head','atom_favicon');
# MENU WALKER
class description_walker extends Walker_Nav_Menu {
	function start_el(&$output, $item, $depth, $args) {
		 global $wp_query;
		 $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';	
		 $class_names = $value = '';	
		 $classes = empty( $item->classes ) ? array() : (array) $item->classes;	
		 $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
		 $class_names = ' class="'. esc_attr( $class_names ) . '"';	
		 $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';	
		 $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		 $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		 $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		 $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
	
		 $prepend = '<strong>';
		 $append = '</strong>';
		 $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';
	
		 if($depth != 0) {
				   $description = $append = $prepend = "";
		 }
	
		$item_output = $args->before;
		$item_output .= '<a'. $attributes .'>';
		$item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
		$item_output .= $description.$args->link_after;
		$item_output .= '</a>';
		$item_output .= $args->after;
  
		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}
/*============================[ GENERAL ]===*/
function slug($str){return str_replace(' ','_',strtolower($str));}
function atom_truncate($content, $limit, $more='&hellip;'){
	if(strlen($content)<$limit){$more="";}
	return substr_replace($content, '', $limit)."".$more;
}
function atom_get_the_content() {	
	ob_start();
	the_excerpt();
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}
function breadcrumbs(){ ?>
	<div id="breadcrumbs">
      <a href="<?php bloginfo('url'); ?>"><?php _e('Home','atom') ?></a> <span class="raquo">&raquo;</span>      
      <?php if( is_tag() ) { ?>
          <?php _e('Posts Tagged ','atom') ?><span class="raquo">&quot;</span><?php single_tag_title(); echo('&quot;'); ?>
      <?php } elseif (is_day()) { ?>
          <?php _e('Posts made in','atom') ?> <?php the_time('F jS, Y'); ?>
      <?php } elseif (is_month()) { ?>
          <?php _e('Posts made in','atom') ?> <?php the_time('F, Y'); ?>
      <?php } elseif (is_year()) { ?>
          <?php _e('Posts made in','atom') ?> <?php the_time('Y'); ?>
      <?php } elseif (is_search()) { ?>
          <?php _e('Search results for','atom') ?> <?php the_search_query() ?>
      <?php } elseif (is_single()) { ?>
          <?php $category = get_the_category();
                $catlink = get_category_link( $category[0]->cat_ID );
                echo ('<a href="'.$catlink.'">'.$category[0]->cat_name.'</a> '.'<span class="raquo">&raquo;</span> '.get_the_title()); ?>
      <?php } elseif (is_category()) { ?>
          <?php single_cat_title(); ?>
      <?php } elseif (is_author()) { ?>
          <?php _e('Posts by ','atom'); echo ' ',$curauth->nickname; ?>
      <?php } elseif (is_page()) { ?>
          <?php wp_title(''); ?>
      <?php }; ?>
    </div> <!-- end #breadcrumbs -->
    <?php 
}
function atom_post_navi( $id = 'post-navi', $prev_pretext = '&larr;', $next_pretext = '&rarr;'){ ?>
	<div id="<?php echo $id; ?>" class="clearfix">
		<div class="nav-prev"><?php previous_post_link( '%link', '<span class="meta-nav">' . $prev_pretext . '</span> %title' ); ?></div>
		<div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">' . $next_pretext . '</span>' ); ?></div>
		<div class="clear"></div>
	</div><!-- #<?php echo $id; ?> -->
	<?php
}
function get_featured_image($post_id,$size='full',$ttatr='h=150&w=150&zc=1',$display=TRUE){
	$img = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),$size);
	if($display==TRUE){
		if($size!='full'){
			echo "<img src='".THEME_URL."/lib/timthumb.php?src=".$img[0]."&".$ttatr."' alt='".get_the_title()."' title='".get_the_title()."' >";	
		}
		else{
			the_post_thumbnail();
		}
	}
	else{
		echo THEME_URL."/lib/timthumb.php?src=".$img[0]."&".$ttatr;
	}
}
function post_meta_s1(){
	global $post;
	$comment = wp_count_comments($post->ID);
	?>
    <div class="post_meta_s1">
    	<a href="<?php the_permalink() ?>">
            <span class="j"><?php echo esc_attr( get_the_date('j') ) ?></span>
            <span class="M"><?php echo esc_attr( get_the_date('M') ) ?></span>
        </a>
        <span class="comment"><?php echo $comment->approved ?></span>
    </div>
    <?php	
}
function base_posted_by() {
	printf( __( '<span class="author vcard"><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>', 'atom' ),
		esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
		sprintf( esc_attr__( 'View all posts by %s', 'atom' ), get_the_author() ),
		esc_html( get_the_author() )
	);
}
function base_posted_in($lbl=TRUE, $tag=TRUE){ 
	$show_sep = false; 	
	/* translators: used between list items, there is a space after the comma */
	$categories_list = get_the_category_list( __( ', ', 'atom' ) );
	if ( $categories_list ): ?>
        <span class="cat-links">
            <?php 
			if($lbl){
				printf( __( '<span class="%1$s">Posted in</span> %2$s', 'atom' ), 'entry-utility-prep entry-utility-prep-cat-links', $categories_list );
			}
			else{
				printf( __( '%2$s', 'atom' ), 'entry-utility-prep entry-utility-prep-cat-links', $categories_list );
			}
            $show_sep = true; ?>
        </span>
    <?php endif; // End if categories ?>
    <?php
	if($tag) :
			/* translators: used between list items, there is a space after the comma */
			$tags_list = get_the_tag_list( '', __( ', ', 'atom' ) );
			if ( $tags_list ):
			if ( $show_sep ) : ?>
		<span class="sep"> | </span>
			<?php endif; // End if $show_sep ?>
		<span class="tag-links">
			<?php printf( __( '<span class="%1$s">Tagged</span> %2$s', 'atom' ), 'entry-utility-prep entry-utility-prep-tag-links', $tags_list );
			$show_sep = true; ?>
		</span>
		<?php endif; // End if $tags_list 
	endif; 
}
function atom_pagination($pages = '', $range = 2, $label = ''){
	global $wp_query;
	$big = 999999999; // need an unlikely integer
	
	echo paginate_links( array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages
	) );
}
# Comments
function atom_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'twentyeleven' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment clearfix">
			<footer class="comment-meta">
				<div class="comment-author vcard">
                	<span class="frame"></span>
					<?php
						$avatar_size = 83;
						if ( '0' != $comment->comment_parent )
							$avatar_size = 83;
						echo get_avatar( $comment, $avatar_size );
						/* translators: 1: comment author, 2: date and time */
						printf( __( '%1$s <span class="comment-date">%2$s</span> ', 'atom' ),
							sprintf( '<span class="fn">%s</span>', get_comment_author_link() ),
							sprintf( '<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
								esc_url( get_comment_link( $comment->comment_ID ) ),
								get_comment_time( 'c' ),
								/* translators: 1: date, 2: time */
								sprintf( __( '%1$s at %2$s', 'atom' ), get_comment_date(), get_comment_time() )
							)
						);
					?>
					<?php edit_comment_link( __( 'Edit | ', 'atom' ), '<span class="edit-link">', '</span>' ); ?>
                    <div class="comment-content"><?php comment_text(); ?></div>
                    <div class="reply">
                        <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'atom' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                    </div><!-- .reply -->
				</div><!-- .comment-author .vcard -->
				
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'atom' ); ?></em>
					<br />
				<?php endif; ?>
			</footer>
		</article><!-- #comment-## -->
	<?php
		break;
	endswitch;
}
# Resource: http://snipplr.com/view/4621/
function HexToRGB($hex) {
	$hex = ereg_replace("#", "", $hex);
	$color = array();
	if(strlen($hex) == 3) {
		$color['r'] = hexdec(substr($hex, 0, 1) . $r);
		$color['g'] = hexdec(substr($hex, 1, 1) . $g);
		$color['b'] = hexdec(substr($hex, 2, 1) . $b);
	}
	else if(strlen($hex) == 6) {
		$color['r'] = hexdec(substr($hex, 0, 2));
		$color['g'] = hexdec(substr($hex, 2, 2));
		$color['b'] = hexdec(substr($hex, 4, 2));
	}
	return $color;
}
function atom_color_scheme(){
	$temp = get_atom_option('color_scheme');
	if($temp!="") {
		$temp1 = HexToRGB($temp);		
		echo "#access1 {background:$temp;} \n";
		echo "#access2 {border-bottom-color:rgba({$temp1['r']}, {$temp1['g']}, {$temp1['b']},0.8);} \n";
		echo "#access2 li {background-color:rgba({$temp1['r']}, {$temp1['g']}, {$temp1['b']},0.05);} \n";		
		echo "#footer-colophon {background-color:rgba({$temp1['r']}, {$temp1['g']}, {$temp1['b']},0.6); box-shadow:0px -1px 0px rgba({$temp1['r']}, {$temp1['g']}, {$temp1['b']},0.90);} \n";		
	}	
}
function atom_typography(){	
	# TOP MENU 
	echo (get_atom_option('font_color_menu1')!='') ? "#access1 a {color:".get_atom_option('font_color_menu1')."; }\n" : '' ;
	echo (get_atom_option('hover_color_menu1')!='') ? "#access1 a:hover {color:".get_atom_option('hover_color_menu1')."; }\n" : '' ;
	echo (get_atom_option('font_size_menu1')!='') ? "#access1 a {font-size:".get_atom_option('font_size_menu1')."; }\n" : '' ;
	echo (get_atom_option('font_family_menu1')!='') ? "#access1 a {font-family:".get_atom_option('font_family_menu1')."; }\n" : '' ;	
	
	# ENTRY TITLE	
	echo (get_atom_option('font_size_entry_titles')!='') ? "#content .title a  {font-size:".get_atom_option('font_size_entry_titles')."; }\n" : '' ;
	echo (get_atom_option('font_color_entry_titles')!='') ? "#content .title a  {color:".get_atom_option('font_color_entry_titles')."; }\n" : '' ;	
	echo (get_atom_option('font_family_entry_titles')!='') ? "#content .title a  {font-family:".get_atom_option('font_family_entry_titles')."; }\n" : '' ;	
	
	# CONTENT TEXTS
	echo (get_atom_option('font_size_content_texts')!='') ? ".entry-excerpt {font-size:".get_atom_option('font_size_content_texts')."; }\n" : '' ;
	echo (get_atom_option('font_color_content_texts')!='') ? ".entry-excerpt {color:".get_atom_option('font_color_content_texts')."; }\n" : '' ;	
	echo (get_atom_option('font_family_content_texts')!='') ? ".entry-excerpt {font-family:".get_atom_option('font_family_content_texts')."; }\n" : '' ;
	
	# CONTENT TEXTS
	echo (get_atom_option('font_size_links')!='') ? "a, .entry-meta a, #sidebar a {font-size:".get_atom_option('font_size_links')."; }\n" : '' ;
	echo (get_atom_option('font_color_links')!='') ? "a, .entry-meta a, #sidebar a {color:".get_atom_option('font_color_links')."; }\n" : '' ;	
	echo (get_atom_option('font_family_links')!='') ? "a, .entry-meta a, #sidebar a {font-family:".get_atom_option('font_family_links')."; }\n" : '' ;	
	echo (get_atom_option('hover_color_links')!='') ? "a:hover, .entry-meta a:hover, #sidebar a:hover {color:".get_atom_option('hover_color_links')."; }\n" : '' ;
}
function atom_header_code(){
	if(get_atom_option("activate_header_code")=="on"){
		echo get_atom_option("header_code");
	}
}
function atom_footer_code(){
	if(get_atom_option("activate_footer_code")=="on"){
		echo get_atom_option("footer_code");
	}
}
add_action('wp_head','atom_header_code');
add_action('wp_footer','atom_footer_code');
?>