<footer id="footer-colophon" role="contentinfo">  
  <div id="site-generator">
		Designed by <a href="http://themetribe.com">Themetribe</a> | Powered by <a href="http://wordpress.org">Wordpress</a> 
   </div>      
  </div>
</footer><!-- #colophon -->
<?php if(try_mobile()): ?>
</div> <!-- #responsive -->
<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>