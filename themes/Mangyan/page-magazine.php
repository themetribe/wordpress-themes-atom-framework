<?php 
/** Template Name: Magazine*/ 
get_header();

if(!is_archive()){
	query_posts( array( 'post_type' => 'post', 'paged' => get_query_var('paged') ) );
}

$big_post=1;
$c=0;

?>

<div id="header-banner" class="clearfix wrapper">
	<div id="header-banner-img">
    	<?php if(has_post_thumbnail()) { ?>
		<img src="<?php get_featured_image($post->ID,'large','h=138&w=951&zc=1',FALSE); ?>" />
        <?php } ?>
    </div>
    <div id="breadcrumb">
        <span>You are here:</span> 
        <?php breadcrumbs() ?>
    </div>
    <h2 id="page-title" class="title"><?php the_title() ?></h2>
    <?php atom_post_navi('post-navi','&laquo;','&raquo;'); ?>
</div>
<div id="main" class="wrapper clearfix">
	<div id="content">
    	
		<?php 		
		if(have_posts()) : while(have_posts()): the_post(); $c++; 
			if($c<=$big_post) 
				{$add_class=" big"; $feat_size='h=210&w=614&zc=1'; $content_length=244; }
			else 
				{$add_class=""; $feat_size='h=210&w=284&zc=1';$content_length=50;}
			
			 ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class($add_class); ?>>
            	<h2 class="title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h2>
                <?php if(has_post_thumbnail()) : ?>
                    <div class="entry-thumb">
                        <span class="frame"></span>
                        <?php get_featured_image($post->ID,'large',$feat_size); ?>
                    </div>
                <?php endif; ?>
                <div class="entry-meta clearfix">
                    <?php post_meta_s1(); ?>
                    <div class="posted-by">
                        <span class="lbl">Posted by: </span> <?php base_posted_by(); ?>
                    </div>
                    <?php if($c<=$big_post) : ?>
                    <div class="posted-in">
                        <span class="lbl">Posted in: </span> <?php base_posted_in(FALSE,FALSE); ?>
                    </div>
                    <?php endif; ?>                        
                </div>
                <div class="entry-excerpt">
                    <?php echo atom_truncate(atom_get_the_content(),$content_length); ?>
                </div>
                <a class="atom_button readmore" href="<?php the_permalink() ?>">Read More <span>&rarr;</span></a>
            </div>
        <?php endwhile; endif; ?>
        <div class="clear"></div>
        <div class="wp-pagenavi"><?php atom_pagination(); ?></div>
    </div>
    <?php get_sidebar(); ?>
</div>
<?php get_footer() ?>
