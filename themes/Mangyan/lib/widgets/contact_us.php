<?php
class TF_contact_us extends WP_Widget{
	function TF_contact_us(){
		$widget_ops = array('classname' => 'tf-contact-us', 'description' => 'Displays an email form.' );
	    $this->WP_Widget('TF_contact_us', 'TF Contact Us', $widget_ops);
	}
	function form($instance){
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'email'=>get_bloginfo('admin_email') ) );
	    $title = $instance['title'];
		$email = $instance['email'];
		?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
        <p><label for="<?php echo $this->get_field_id('email'); ?>">Email: <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo attribute_escape($email); ?>" /></label></p>
        <?php
	}
	function update($new_instance, $old_instance){		
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['email'] = $new_instance['email'];
		return $instance;
	}
	function widget($args, $instance){
		global $post;
		extract($args, EXTR_SKIP);
		echo $before_widget;
		
		$title = empty($instance['title']) ? 'Email Us' : apply_filters('widget_title', $instance['title']);
		$email = empty($instance['email']) ? ' ' : apply_filters('widget_title', $instance['email']);
		
		if (!empty($title))
		  echo $before_title . $title . $after_title;;
	 
		// WIDGET CODE GOES HERE
		
		if( isset($_POST['atom_contactform']) ){
			global $wpdb;
			
			extract($_POST);
			
			$name = $wpdb->escape( $contact_name_{$rand} );
			$email = $wpdb->escape( $contact_email_{$rand} );
			$message = $wpdb->escape( $contact_message_{$rand} . '\r\n\r\n' . $contact_name_{$rand});
			$admin_email = $atts['emailto'];
			
			mail($admin_email, "A message from $name", $message, "Content-Type: text/html; charset=iso-8859-1 \r\nFrom: {$email}");		
		}
		$rand = rand(100,999);
		?>
        
        <ul class="tf_contact_us">
        	<li>
        	<form action="<?php the_permalink(); ?>" method="post" class="atom_contactform">
                <input type="hidden" name="emailto" value="<?php echo html_entity_decode($email) ?>" />
		    	<input type="hidden" name="rand" value="<?php echo $rand ?>" />
                <p><input type="text" required name="contact_name_<?php echo $rand ?>" id="contact_name_<?php echo $rand ?>" /> <label for="contact_name_<?php echo $rand ?>">Name <span class="required">*</span></label></p>
                <p><input type="text" required name="contact_email_<?php echo $rand ?>" id="contact_email_<?php echo $rand ?>" /> <label for="contact_email_<?php echo $rand ?>">Email <span class="required">*</span></label></p>
                <p><textarea required cols="30" rows="5" name="contact_message_<?php echo $rand ?>" id="contact_message_<?php echo $rand ?>"></textarea></p>
                <p><input type="submit" value="Send Message" class="atom_button grey small" /></p>
            </form>
            </li>
        </ul>
        <?php
	 
		echo $after_widget;
	}
}
add_action( 'widgets_init', create_function('', 'return register_widget("TF_contact_us");') );
?>