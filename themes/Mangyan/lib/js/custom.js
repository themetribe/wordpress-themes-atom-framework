/* ===== IE Compatible for HTML5 ===================================================*/
document.createElement('header');
document.createElement('footer');
document.createElement('section');
document.createElement('aside');
document.createElement('nav');
document.createElement('article');

jQuery.noConflict();
jQuery(document).ready(function(){ 
	jQuery("ul.sf-menu").supersubs({ 
		minWidth:    13,                                // minimum width of sub-menus in em units 
		maxWidth:    27,                                // maximum width of sub-menus in em units 
		extraWidth:  1                                  // extra width can ensure lines don't sometimes turn over 
														// due to slight rounding differences and font-family 
	}).superfish({ 
		delay:       500,                               // delay on mouseout 
		animation:   {opacity:'show', height:'show'},    // fade-in and slide-down animation 
		speed:       'medium',                            // faster animation speed 
		autoArrows:  false,                             // disable generation of arrow mark-up 
		dropShadows: false                              // disable drop shadows 
	})
	jQuery(".comment-reply-link").addClass("atom_button");
	
});

jQuery("#control-toggle.close").live('click',function(){
	jQuery(this).attr('class','open').parent().animate({
		left:"-165px",
		opacity:0.8
	});
});
jQuery("#control-toggle.open").live('click',function(){
	jQuery(this).attr('class','close').parent().animate({
		left:"0",
		opacity:1
	});
});
jQuery(".background-pattern a").live('click',function(){
	custom_bg = jQuery(this).find("img").attr('src');
	jQuery('body').attr('style',"background:url("+custom_bg+");");
});

jQuery(window).load(function() {
	jQuery('#slider').nivoSlider({
		 effect:'fold,boxRandom,boxRain,boxRainReverse,boxRainGrow,boxRainGrowReverse,sliceDownLeft,sliceUpDownLeft,sliceUpDown',
		 directionNav:false,
		 captionOpacity:0.5
	});
	jQuery(".page-template-page-magazine-php #content .hentry:odd").css({marginRight:'15px', float:'left'});
	
	jQuery('.page-template-page-portfolio-php .entry-thumb a').hover(function() {
		jQuery(this).find('span').stop().fadeIn();
	},function(){
		jQuery(this).find('span').stop().fadeOut();
	});
});

