jQuery(document).ready(function(){
	
	// FEATURED
	jQuery('#featured').hover(function() {		
		jQuery(this).find('a').fadeIn(); 
	},
	function(){
		jQuery(this).find('a').fadeOut(); 
	});
	
	
	// MENU TAB BUTTON
	jQuery("#menu-button a").live('click',function(){
		if(jQuery("#menu").hasClass('active')){
			jQuery("#menu").removeClass('active');
			jQuery("#responsive").animate({left:0});			
		}
		else{
			jQuery("#menu").addClass('active');
			jQuery("#responsive").animate({left:225});
		}
	});	
	jQuery("#featured img").css({width:"100%", height: '353px'});
	// MENU ITEM AJAX LOAD
	jQuery("#menu a").live("click", function(e){  
    	e.preventDefault();  
		var featured_container = jQuery("#featured-container");
		var main = jQuery("#main");
		var linkurl   = jQuery(this).attr("href");
		var linkhtmlurl = linkurl.substring(1, linkurl.length);
		var imgloader  = '<center id="imgloader" style="margin-top: 30px;"><img src="wp-content/themes/Mangyan/images/loading.gif" alt="loading..." /></center>'; 
		featured_container.remove();
		main.html(imgloader);
		setTimeout(function() {			
			main.load(linkurl+' #content', function() { jQuery('#imgloader').remove(); }) 
			
		}, 1200);
	});
});