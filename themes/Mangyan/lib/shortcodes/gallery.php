<?php
function jsgallery(){
	?>
    <script>
	jQuery('.gallery a').attr('rel','lightbox').hover(function(){
		jQuery(this).stop().animate({opacity:0.7});
	},function(){
		jQuery(this).stop().animate({opacity:1});
	});
	</script>
    <?php
	
}
function atom_gallery( $output, $attr) {
	wp_enqueue_script( 'prototype', SITE_URL.'/js/prototype.js');
	wp_enqueue_script( 'scriptaculous', SITE_URL.'/js/scriptaculous.js?load=effects,builder');
	wp_enqueue_script( 'lightbox', SITE_URL.'/js/lightbox.js');
	
    wp_enqueue_style( 'lightbox', SITE_URL.'/css/lightbox.css' );
	add_action('wp_footer','jsgallery');
	return $output;
}
add_filter( 'post_gallery', 'atom_gallery', 10, 2 );
?>