<?php
function atom_video($atts, $content = NULL){
	extract($atts);
	$width = ($width) ? $width : 640;
	$height = ($height) ? $height : 360;
	$play = ($play) ? $play : "true";
	ob_start();
	if($type=='flash'):
		?>
		<object width="<?php echo $width ?>" height="<?php echo $height ?>">
			<param name="movie" value="<?php echo $src ?>"></param>
			<param name="allowFullScreen" value="true"></param>
			<param name="allowscriptaccess" value="always"></param>
			<param name="play" value="<?php echo $play ?>">
			<param name="wmode" value="transparent">
			<embed src="<?php echo $src ?>" type="application/x-shockwave-flash" width="<?php echo $width ?>" height="<?php echo $height ?>" wmode="transparent" allowscriptaccess="always" allowfullscreen="true"></embed>
		</object>
		<?php
	elseif($type=='youtube'):
		?>
		<iframe width="<?php echo $width ?>" height="<?php echo $height ?>" src="http://www.youtube.com/embed/<?php echo $id ?>?rel=0" frameborder="0" allowfullscreen></iframe>
		<?php
	elseif($type=='vimeo'):
		?>
        <iframe 
        	src="http://player.vimeo.com/video/<?php echo $id ?>?title=0&amp;byline=0&amp;portrait=0" 
            width="<?php echo $width ?>" height="<?php echo $height ?>" 
            frameborder="0" 
            webkitAllowFullScreen mozallowfullscreen allowFullScreen>
        </iframe>
		<?php
	elseif($type=='dailymotion'):
		?>
        <iframe frameborder="0" 
        	width="<?php echo $width ?>" height="<?php echo $height ?>" 
            src="http://www.dailymotion.com/embed/video/<?php echo $id ?>">
        </iframe>
        <?php
	endif; 
	$s = ob_get_contents();
	ob_end_clean();
	return $s;
}
add_shortcode('video','atom_video');
?>