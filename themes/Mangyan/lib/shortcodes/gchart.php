<?php

function atom_gchart($atts=NULL, $content = NULL){
	
	extract($atts);	
	$rand = rand(100,999);
	
	$labels = explode('|',$atts['labels']);	
	$data = explode('|',$data);
	foreach($data as $key=>$val){
		$data[$key] = explode(',',$val);
	}
	$time = explode(',',$atts['time']);	
	$str_col = $str_r='';
	
	# For Pie
	foreach($labels as $key=>$val){
		$str_r.="['".$labels[$key]."',".$data[0][$key]."],\n";
	}
	
	# For Column
	foreach($time as $key=>$val){
		$str_col.="['".$val."',".$data[0][$key].",".$data[1][$key]."],\n";
	}
	
	$str_r=substr($str_r ,0,-1);
	
	$height = ($height) ? $height : "500px";
	$width = ($width) ? $width : "900px";
	
	ob_start();
	?>
    
    <div id="chart_div_<?php echo $rand ?>" style="width:<?php $width ?>;height:<?php echo $height ?>"></div>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
		google.load("visualization", "1", {packages:["corechart"]});
		google.setOnLoadCallback(drawChart);
		<?php if($atts['type']=='column') : ?>
			function drawChart() {
				var data = new google.visualization.DataTable();
				data.addColumn('string', 'Year');
				data.addColumn('number', '<?php echo $labels[0]; ?>');
				data.addColumn('number', '<?php echo $labels[1]; ?>');
				data.addRows([
					<?php echo $str_col ?>
				]);
			
				var options = {
				  title: '<?php echo $atts['title'] ?>',
				  hAxis: {title: 'Year', titleTextStyle: {color: 'red'}}
				};
			
				var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_<?php echo $rand ?>'));
				chart.draw(data, options);
			}	
	  	<?php else : ?>
			function drawChart() {
				var data = new google.visualization.DataTable();
				data.addColumn('string', 'col1');
				data.addColumn('number', 'col2');
				data.addRows([
				  <?php echo $str_r ?>
				]);
		
				var options = {
				  title: '<?php echo $atts['title'] ?>'
				};
				var chart = new google.visualization.PieChart(document.getElementById('chart_div_<?php echo $rand ?>'));
				chart.draw(data, options);
			}
	  <?php endif; ?>	
    </script>
    <?php
	$s = ob_get_contents();
	ob_end_clean();
	add_action('wp_footer','jsgchart');
	return $s;
}
add_shortcode('gchart','atom_gchart');

function jsgchart(){
	?><?php
}

?>