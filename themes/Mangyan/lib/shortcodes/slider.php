<?php

function atom_slider($atts, $content = NULL)
{	
	global $post;
	extract($atts);
	
	/* Default Atts value */
	$width = ($width=='') ? 960 : $width;
	$height = ($height=='') ? 475 : $height; 
	ob_start();
	if($type=='nivo') : ?>
    
        <div class="nivoContainer">
            <div class="nivo-frame" style="display:block; width:<?php echo ($width-6) ?>px; height:<?php echo ($height-6) ?>px;"></div>
            <div class="nivo">              
                <?php                    
                query_posts(array('category'=>$category));
                
                if(have_posts()) : while(have_posts()): the_post(); 
                    get_featured_image($post->ID,'large',"h=$height&w=$width&zc=1");   
                endwhile; endif; wp_reset_query();
                
                if(!in_array('pagination',$atts)) {
                    add_action('wp_footer', 'jsnivo_nonav');
                }
                else{
                    add_action('wp_footer', 'jsnivo');
                }
                
                wp_enqueue_script( 'nivo', SITE_URL.'/js/nivo.js');
                wp_enqueue_style( 'nivo_style', SITE_URL.'/css/nivo.css' );
                ?>	
            </div>
            <?php if(in_array('shadow',$atts)) {
                ?><div class="nivo-shadow"></div><?php
            } ?>        
        </div>
        
    <?php elseif($type=='cycle') : ?>
    
    	<div class="CycleContainer">
        	<div class="cycle">
            	<?php                    
                query_posts(array('category'=>$category));
                
                if(have_posts()) : while(have_posts()): the_post(); 
                    get_featured_image($post->ID,'large',"h=$height&w=$width&zc=1");   
                endwhile; endif; wp_reset_query();
				
				add_action('wp_footer', 'jscycle');
                ?>
            </div>
            
            <div class="cycle-nav"></div>
        </div>
        
    <?php else : ?>
    	
	<?php endif; 
	$s = ob_get_contents();
	ob_end_clean();
	return $s;
}
add_shortcode('slider','atom_slider');

/* NIVO functions ================================================*/
function nivo($nopage=false){ ?>
    <script>
		jQuery(window).load(function() {			
			jQuery('.nivo').nivoSlider({
				 effect:'fold,boxRandom,boxRain,boxRainReverse,boxRainGrow,boxRainGrowReverse,sliceDownLeft,sliceUpDownLeft,sliceUpDown',
				 <?php
				 if($nopage){
					 echo 'controlNav:false,';
				 }
				 ?>
				 directionNav:false
				 
			});
			jQuery(".nivo-caption").hide();
			jQuery('.nivoContainer').hover(function(){
				jQuery('.nivo-caption').slideDown();
				}, function(){
				jQuery('.nivo-caption').slideUp();
			});
		});
	</script>
    <?php
}
function jsnivo(){ nivo(); }
function jsnivo_nonav(){ nivo(true); }

/* CYCLE functions ===============================================*/

function jscycle(){ 
	?>
	<script>
		jQuery(window).load(function() {
			jQuery('.cycle').cycle({
				fx:'scrollRight',
				pager:  '.cycle-nav'
			});
		});
	</script>
	<?php
}
?>