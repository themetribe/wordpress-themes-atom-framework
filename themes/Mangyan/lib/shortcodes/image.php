<?php
function atom_image($atts, $content = NULL){
	extract($atts);
	$width = ($width) ? $width : "100%";
	$height = ($height) ? $height : "100%";
	
	wp_enqueue_script( 'prototype', SITE_URL.'/js/prototype.js');
	wp_enqueue_script( 'scriptaculous', SITE_URL.'/js/scriptaculous.js?load=effects,builder');
	wp_enqueue_script( 'lightbox', SITE_URL.'/js/lightbox.js');
	
    wp_enqueue_style( 'lightbox', SITE_URL.'/css/lightbox.css' );
	if($width=='100%'){
		$s = "<img src='{$source}'
		alt='{$alt}'
		title='{$title}'
		width='{$width}'
		height='{$height}' />";
	}
	else{
		$s=get_featured_image('','large',"h=$height&w=$width&zc=1",$source,false);
		$s = "<img src='{$s}'
		alt='{$alt}'
		title='{$title}' />";
	}
	if(in_array('lightbox',$atts)){
		if(array_key_exists('url',$atts)){
			$s="<a class='imglink' href='{$url}' rel='lightbox'><span class='overlay_lightbox' style='width:$width;height:$height;'></span>{$s}</a>";
		}
		else{
			$s="<a class='imglink' href='{$source}' rel='lightbox'><span class='overlay_lightbox' style='width:$width;height:$height;'></span>{$s}</a>";
		}
	}	
	if(in_array('link',$atts)){
		$s="<a class='imglink' href='{$url}'><span class='overlay_link' style='width:$width;height:$height;'></span>{$s}</a>";
	}
	add_action('wp_footer', 'jsimg');
	return $s;	
}
add_shortcode('image','atom_image');
function jsimg(){
	?>
    <script>
		jQuery(".imglink").hover(function(){
			jQuery(this).find('.overlay_lightbox, .overlay_link').fadeToggle('fast');
		});
	</script>
    <?php
}
?>