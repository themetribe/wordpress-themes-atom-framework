<?php
function atom_box($atts=NULL, $content = NULL){
	$s='';
	if($atts==NULL){
		$type='default';
	}
	else{
		extract($atts);
		if(array_key_exists('align',$atts)) $align="align{$align}";
	}
	
	switch($type){
		case 'note':
		case 'error':
			$s="<div class='atom_box {$type} {$align}' style='width:{$width}px;'>
			<div class='title'>{$title}</div>
			<div class='inner clearfix'>".do_shortcode($content)."</div>
			</div>";
			break;
		default:
			$s="<div class='atom_box {$type} {$align}' style='width:{$width}px;'><div class='inner clearfix'>".do_shortcode($content)."</div></div>";
			break;
	}
	return $s;
}
add_shortcode('box','atom_box');
?>