<?php
function atom_blog($atts, $content = NULL){
	global $post;
	$arg='paged='.get_query_var( 'paged' ).'&';
	extract($atts);
	ob_start();
	
	if($count){
		$arg.="posts_per_page=$count&";
	}
	if($category){
		$arg.="category_name=$category&";
	}
	$arg = substr($arg, 0, strlen($arg)-1); 
	
	query_posts($arg);
	?>
    <div class="blog clearfix">
    <?php
	if(have_posts()) : while(have_posts()): the_post(); ?>
		<div class="clearfix entry">        	
            <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>            
            <?php if(in_array('nometa',$atts)==false) : ?>
            	<div class="meta"><?php post_meta() ?></div>
            <?php endif; ?>
            <?php if(has_post_thumbnail() && in_array('noimage',$atts)==false): ?>
            	<div class="thumb"><a href="<?php the_permalink(); ?>"><?php get_featured_image($post->ID,'large','h=180&w=294&zc=1') ?></a></div>            
            <?php endif; ?>
            <div class="excerpt">
            	<?php echo patico_truncate(patico_get_the_content(),500) ?>
            </div>
            <a class="atom_button grey small" href="<?php the_permalink() ?>">Read More <span>&raquo;</span></a>
        </div>
		<?php endwhile; endif; ?>
        <?php if(in_array('nopaging',$atts)==false) : ?>
            <div class="page-navi clearfix">
                <?php atom_pagination(); ?>
            </div>
        <?php endif; ?>
        <?php wp_reset_query(); ?>
    </div>
    <?php
	$s = ob_get_contents();
	ob_end_clean();
	return $s;
}
add_shortcode('blog','atom_blog');
?>