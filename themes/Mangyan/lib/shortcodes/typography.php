<?php

/* ==== DROPCAPS ============ */
function atom_dropcap($atts=NULL, $content = NULL){
	$_class='';
	if($atts){
		if(in_array('background',$atts)) $_class.=' bg';
		if(in_array('shadow',$atts)) $_class.=' shadow';
	}
	return "<span class='dropcap{$_class}'>$content</span>";
}
add_shortcode('dropcap','atom_dropcap');

/* ==== BLOCKQUOTES ============ */
function atom_blockquote($atts=NULL, $content = NULL){
	$_cite = $_class='';
	if(array_key_exists('align',$atts)) $_class.=" align".$atts['align'];
	if(array_key_exists('cite',$atts)) $_cite.=" <span class='cite'>".$atts['cite']."</span>";
	return "<span class='blockquote{$_class}'>$content {$_cite}</span>";	
}
add_shortcode('blockquote','atom_blockquote');

/* ==== HIGHLIGHT ============ */
function atom_highlight($atts=NULL, $content = NULL){
	$_style='';
	if($atts){
		$_style="style='";
		if(array_key_exists('bgcolor',$atts)) { $_style.="background:".$atts['bgcolor'].";"; }
		if(array_key_exists('txtcolor',$atts)) { $_style.="color:".$atts['txtcolor'].";"; }
		$_style.="'";
	}
	return "<span class='highlight'{$_style}>{$content}</span>";
}
add_shortcode('highlight','atom_highlight');

/* ==== TOGGLE ============ */
function atom_toggle($atts=NULL, $content = NULL){
	extract($atts);
	ob_start();
	?>
    <div class="toggle">
    	<h4 class="title"><span class="sign">+</span><span class="sign" style="display:none;">-</span><?php echo $atts['title'] ?></h4>
        <div class="content" style="display:none;">
        	<?php 
			if($type=='code'){
				remove_filter('the_content','wpautop');
				//echo "<code>".nl2br(htmlspecialchars(trim($content)))."</code>"; 
				echo $content; 
				
			}
			else{
				echo do_shortcode($content);
			}
			?>
        </div>
    </div>
    <?php
	$s = ob_get_contents();
	ob_end_clean();
	add_action('wp_footer', 'jstoggle');
	return $s;
}
add_shortcode('toggle','atom_toggle');
function jstoggle(){ 
	?>
	<script>
		jQuery(window).load(function() {
			jQuery(".toggle .title").live('click',function(){				
				jQuery(this).parent().find(".content, .title .sign").toggle('slow');
				
			});
		});
	</script>
	<?php
}
?>