<?php
function atom_contactform($atts=NULL, $content = NULL){
	
	if( isset($_POST['atom_contactform']) ){
		global $wpdb;
		
		extract($_POST);
		
		$name = $wpdb->escape( $contact_name_{$rand} );
		$email = $wpdb->escape( $contact_email_{$rand} );
		$message = $wpdb->escape( $contact_message_{$rand} . '\r\n\r\n' . $contact_name_{$rand});
		$admin_email = $atts['emailto'];
		
		mail($admin_email, "A message from $name", $message, "Content-Type: text/html; charset=iso-8859-1 \r\nFrom: {$email}");		
	}
	
	ob_start();
	$rand = rand(100,999);
	?>
    <form action="<?php the_permalink(); ?>" method="post" class="atom_contactform">
    	<input type="hidden" name="emailto" value="<?php echo $atts['emailto'] ?>" />
    	<input type="hidden" name="rand" value="<?php echo $rand ?>" />
    	<p><input type="text" required name="contact_name_<?php echo $rand ?>" id="contact_name_<?php echo $rand ?>" /> <label for="contact_name_<?php echo $rand ?>">Name <span class="required">*</span></label></p>
        <p><input type="text" required name="contact_email_<?php echo $rand ?>" id="contact_email_<?php echo $rand ?>" /> <label for="contact_email_<?php echo $rand ?>">Email <span class="required">*</span></label></p>
        <p><textarea required cols="30" rows="5" name="contact_message_<?php echo $rand ?>" id="contact_message_<?php echo $rand ?>"></textarea></p>
        <p><input type="submit" class="atom_button white" /></p>
    </form>
    <?php
	$s = ob_get_contents();
	ob_end_clean();
	return $s;
}
add_shortcode('contactform','atom_contactform');
?>