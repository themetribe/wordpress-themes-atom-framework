<?php

register_post_type('custom_sidebar_area', array(
	'label' => __('Sidebar Generator'),
	'singular_label' => __('Sidebar Generator'),
	'public' => true,
	'show_ui' => true,
	'capability_type' => 'post',
	'hierarchical' => false,
	'menu_position' => 80,
	'rewrite' => true,
	'supports' => array('title')
));

$swag = new WP_Query();
$swag->query('post_type=custom_sidebar_area');

while ($swag->have_posts()) : $swag->the_post();	
	register_sidebar( array(
		'name' => __( get_the_title(), 'atom' ),
		'id' => "sidebar-{$post->post_name}",
		'description' => __( 'A custom sidebar widget area created from Sidebar Generator', 'atom' ),
		'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</li>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
endwhile;
?>