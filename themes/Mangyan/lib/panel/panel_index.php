<?php 
if(function_exists('add_panel_option')){
	$arr2 = add_panel_option();	
	if(function_exists('remove_panel_option')){		
		$r_arr = remove_panel_option();
		$arr = panel_skeleton($arr2,$r_arr);
	}
	else{
		$arr = panel_skeleton($arr2);
	}
}
elseif(function_exists('remove_panel_option')){
	$r_arr = remove_panel_option();
	$arr = panel_skeleton(array(),$r_arr);
}
else{
	$arr = panel_skeleton();
}
	
?>
<link rel="stylesheet" type="text/css" href="<?php echo THEME_URL; ?>/lib/panel/panel_style.css" />
	<div id="result"><?php if(isset($_POST['submit'])){ save_settings(); } ?><?php if(isset($_POST['to_default'])) { default_settings(); } ?></div>
<div class="wrap" id="theme-settings" style="width:983px;">	
    <form action="<?php echo $_SERVER['PHP_REQUEST']; ?>" method="post">
    
    <?php /*For Sidemenu, the foreach will detect the first array index of the $arr, 
			- if it's [0] then it has no sub menu
			- if it's not [0] then it has and must have a sub menu 
			
			So, no submenu should be encapsulated under [0] index of the array. */ ?>
            
    <div id="side-menu">
    	<div id="logo_img"><img src="<?php echo THEME_URL ?>/lib/panel/images/admin-logo.png" /></div>	
    	<ul>
        <?php 
		foreach($arr as $key=>$val) { 			
            echo "<li class='".slug($key)."' rel='".slug($key)."'><span class='icon'></span><a href='#'>$key</a>";			
			if(!array_key_exists(0,$val)){
				?>
                <div class="sub-menu-div">
                	<ul>
                    	<?php foreach($val as $k2=>$v2){
                    	echo "<li rel='".slug($k2)."'><a href='#'>$k2</a></li>";
						} ?>
                    </ul>
                </div>
                <?php
			}
			echo "</li>";
        } ?>
        </ul>
    </div>
    
    
    <div id="settings">
    	<?php foreach($arr as $key=>$val) : ?> 
        <div class="panel" id="panel_<?php echo slug($key); ?>">
        
        	<?php
			foreach($val as $k=>$v):   ?>
				<div class="option-group <?php echo slug($k) ?>" >
                	<?php 
					foreach($v as $k2=>$v2){ 
						if($k2=="sep") : ?>
                        	<hr />
                        <?php else: ?>
                            <div class="option <?php echo slug($k2) ?>">
                                <div class="option-header">
                                    <strong><?php echo (array_key_exists('label',$v2)) ? $v2['label'] : $k2 ?></strong>
                                    <a href="#" class="quest"><img src="<?php echo THEME_URL ?>/lib/panel/images/question.png" /></a>
                                    <span class="desc"><?php echo $v2['desc'] ?></span>
                                </div>
                                <div class="option-content">
                                    <?php input_type($k2, $v2); ?>
                                </div>
                            </div>
                    	<?php endif; 
					}
					?>
                </div>
                <?php
			endforeach;
			?>
        </div>
        <?php endforeach; ?> 
        <div class="controller">
        	<a href="#" class="atom_button" id="frm_default">Default<span class='shadow'></span></a>
            <a href="#" class="atom_button" id="frm_save">Save<span class='shadow'></span></a>
        </div>
    </div>
    </form>
    <div style="clear:both;"></div>
</div>

<script type="text/javascript">	
	
	/* When Hint is hover*/
	jQuery(".quest").hover(function(){jQuery(this).parent().find('.desc').toggle();});
	
	/* Show first option-group of first panel*/
	jQuery('#settings .panel:first .option-group').first().show();
	
	/* 
		-Make first menu item as current, 
	    - (and if it has a sub), 
		 	--make the first item of sub menu as current as well. 
	    -Active #side-menu li should have an indicator span
	*/	   
	jQuery("#side-menu li").first().addClass('current')
		.append("<span class='current-indi'></span>")
		.parent().find(".sub-menu-div li").first().addClass('current');
	
	/* When Item is clicked */
	jQuery("#side-menu a").live('click',function(){	
		
		jQuery("#side-menu li").removeClass('current')									// Make other not active 
			.find('.current-indi').remove(); 											// Remove the current-indi of the non actives
		
		jQuery(".option-group").hide(); 												// Hide other option-group 
		rel = jQuery(this).parent().attr('rel');										// show the option-group
		
		if(jQuery(this).parent().parent().parent().attr('class')=="sub-menu-div"){		// if the click is sub menu,
			jQuery(this).parent().parent().parent().parent().addClass('current'); 		// put back the active on it's parent
			jQuery(".option-group."+rel).fadeIn();
		}
		else{																			// if the click is main menu item		
			jQuery("#panel_"+rel+" ").find(".option-group:first").fadeIn();											// show first panel-option
		}
		
		jQuery(this).parent().addClass('current') 										// Make the clicked Item active
			.append("<span class='current-indi'></span>") 								// And add the Indicator
			.find(".sub-menu-div li").first().addClass('current');  			// If there's a sub-menu, make the first item active
	});
	
	/* When btn_on_off is clicked */
	jQuery(".btn_on_off").live('click',function(){
		// Toggle Text to Enable|Disable and add class active for style
		// Also, Bind the checkbox check|uncheck
		lbl = jQuery(this).find('strong').text();
		if(lbl=="Disabled"){
			jQuery(this).addClass('active').find('strong').text("Enabled");
			jQuery(this).parent().find('input[type="checkbox"]').attr('checked','checked');
		}
		else{
			jQuery(this).removeClass('active').find('strong').text("Disabled");
			jQuery(this).parent().find('input[type="checkbox"]').removeAttr('checked');
		}
	});
	
	// Link Submit link to submit the form
	jQuery('#frm_save').click(function(){jQuery('form').submit()});
	jQuery('form').submit(function(event){
		event.preventDefault();
		jQuery("#result").html("<div>Saving...</div>");
		var data=jQuery(this).serialize()+"&submit=true";
		jQuery.post(window.location.href,data,function(reply){
			if(jQuery.trim(jQuery(reply).find('#result').html())=="Success"){								
				jQuery("#result").html("<div>Configuration Saved!</div>").delay(2000).find("div").fadeOut('slow');
			}
			else{
				alert(jQuery.trim(jQuery(reply).find('#result').html()));
			}
        });
	});
	
	// Settings Default
	jQuery('#frm_default').click(function(){
		jQuery("#result").html("<div>Are you sure about this? <p><a id='btn_yes' class='atom_button'>Yes</a> <a id='btn_no' class='atom_button'>No</a></p></div>");
	});	
	jQuery('#btn_no').live('click',function(){
		jQuery('#result div').fadeOut('medium',function(){jQuery(this).remove()});
	});	
	jQuery("#btn_yes").live('click',function(){		
		jQuery("#result").html("<div>Restoring to default...</div>");
		var data="to_default=true";
		jQuery.post(window.location.href,data,function(reply){
			if(jQuery.trim(jQuery(reply).find('#result').html())=="Success"){								
				jQuery("#result").html("<div>Default Configuration Saved!</div>").delay(2000).find("div").fadeOut('slow');
				location.reload();
			}
			else{
				alert(jQuery.trim(jQuery(reply).find('#result').html()));
			}
        });
	});
	
	// TB_SHOW for Upload Media
	
	var uploadfield='';
	jQuery(".btn_upload_image").click(function(){
		uploadfield = jQuery(this).parent().find('.txt_upload_image');
		tb_show('', 'media-upload.php?width=640&amp;height=500&amp;type=image&amp;TB_iframe=true');
		return false;
	});
	// Reset Button
	jQuery('.btn_upload_image_reset').click(function() {
		jQuery(this).parent().find('.txt_upload_image').val('');
	});
	// TB Media Insert Into field binding to text field
	window.original_send_to_editor = window.send_to_editor;
	window.send_to_editor = function(html) {
		if(uploadfield){
			imgurl = jQuery('img',html).attr('src');
			uploadfield.val(imgurl);
			tb_remove();
		}
		else{
			window.original_send_to_editor(html);
		}
	}
	
	
	jQuery(document).ready(function($) {		
	
		// COLOR SCHEME
						
		$(".fabox").each(function(){
			
			// Hide Color Picker
			$(this).hide();
			
			fabox = $(this);
			color_input = $(this).prev();
			
			
			$("#"+color_input.attr('id')).live('click',function(){				
				input = $(this);
				this_fabox = $(this).next();				
				
				// Show Corresponding Color Picker When Clicked
				$(this_fabox).fadeIn();								
				
				$( "#"+$(this_fabox).attr('id') ).farbtastic( function(_color){
					$( "#"+$(input).attr('id') ).val(_color).css({backgroundColor:_color});
				});
			});
			
		});
		
		$(document).mousedown(function() {
			$('.fabox').each(function() {
				var display = $(this).css('display');
				if ( display == 'block' )
					$(this).fadeOut();
			});
		});
		
	});
</script>