<?php



function theme_panel(){

	$page = add_theme_page(THEME_NAME.' Settings', THEME_NAME.' Settings', 'manage_options', strtolower(THEME_NAME).'_settings', 'theme_panel_init');	

	add_action( 'admin_print_styles-' . $page, 'my_admin_scripts' );

}

function my_admin_scripts(){

	wp_enqueue_style( 'farbtastic' );

    wp_enqueue_script( 'farbtastic' );

}

function theme_panel_init(){

	if (!current_user_can('manage_options'))

		wp_die( __('You do not have sufficient permissions to access this page.') );

	include("panel_index.php");

}

add_action('admin_menu', 'theme_panel');



/********** Panel Skeleton */

function panel_skeleton($arr2=array(),$rem_arr=''){

	$pages = get_pages(); $allpage = array(); foreach ($pages as $page) { $allpage[$page->ID] = $page->post_title; }

	$categories = get_categories('hide_empty=0'); $allcategory = array(); foreach ($categories as $category) {$allcategory[$category->cat_ID] = $category->cat_name;}

	$allfonts=array(

					""=>"",

					'Share, cursive'										=>	"'Share', cursive",

					'Verdana, Geneva, sans-serif'							=>	'Verdana, Geneva, sans-serif',

					'Georgia, Times New Roman, Times, serif'				=>	'Georgia, "Times New Roman", Times, serif',

					'Courier New, Courier, monospace'						=>	'"Courier New", Courier, monospace',

					'Arial, Helvetica, sans-serif'							=> 	'Arial, Helvetica, sans-serif',

					'Tahoma, Geneva, sans-serif'							=>	'Tahoma, Geneva, sans-serif',

					'Trebuchet MS, Arial, Helvetica, sans-serif'			=>	'"Trebuchet MS", Arial, Helvetica, sans-serif',

					'Arial Black, Gadget, sans-serif'						=>	'"Arial Black", Gadget, sans-serif',

					'Times New Roman, Times, serif'							=>	'"Times New Roman", Times, serif',

					'Palatino Linotype, Book Antiqua, Palatino, serif'		=>	'"Palatino Linotype", "Book Antiqua", Palatino, serif',

					'Lucida Sans Unicode, Lucida Grande, sans-serif'		=>	'"Lucida Sans Unicode", "Lucida Grande", sans-serif',

					'MS Serif, New York, serif'								=>	'"MS Serif", "New York", serif',

					'Lucida Console, Monaco, monospace'						=>	'"Lucida Console", Monaco, monospace',

					'Comic Sans MS, cursive'								=>	'"Comic Sans MS", cursive'

					);

	$arr = array(

		'General Settings'=>array(

			'General'=>array(

				'set_custom_logo'=>array(

					'label'=>'Enable Custom Logo',

					'type'=>'on_off',

					'desc'=>'It allows you to enable/disable the logo that you want to upload.'

				),

				'Logo'=>array(

					'type'=>'image_upload',

					'desc'=>'Paste your logo image url or upload your desired custom logo image by pressing the "upload button"..'

				),

				'Favicon'=>array(

					'type'=>'image_upload',

					'desc'=>'paste your desired website icon/bookmark icon image url or upload it by pressing the "upload button".'

				),

				'Color Scheme'=>array(

					'type'=>'color',					

					'desc'=>'It allows you to choose and change the color of your website.'

				),

				'Option Previewer'=>array(

					'type'=>'on_off',					

					'desc'=>'It is used for themes demo and allowed the viewers to see the option themes.'

				),

				'Enable Responsive'=>array(				

					'type'=>'on_off',

					'desc'=>'It allows you to put and display the name of the author.'

				)

			),

			'Homepage'=>array(

				'Site Title Type'=>array(				

					'type'=>'select',

					'opt'=>array('Custom Logo','Title Text'),				

					'desc'=>'Choose between Site Title or choose custom logo <br />and upload your site logo as your site branding.'

				),

				'Set Intro Page'=>array(

					'type'=>'on_off',

					'desc'=>'Disables/ Enables to set and write your desired introductory page.'					

				),

				'Intro Page'=>array(

					'type'=>'select_index',

					'opt'=>$allpage,

					'desc'=>'write your Title page Introduction to have a great impression to your site.'

				),

				'set_blurbs'=>array(

					'label'=>'Enable Services/Blurbs',

					'type'=>'on_off',

					'desc'=>'It allows you to enable a brief publicity notice and promotion of your services.'

				),

				'Blurb 1'=>array(

					'type'=>'select_index',

					'opt'=>$allpage,

					'desc'=>'choose the title you want to appear in the blurb menu 1.'

				),

				'Blurb 2'=>array(

					'type'=>'select_index',

					'opt'=>$allpage,

					'desc'=>'choose the title you want to appear in the blurb menu 2.'

				),

				'Blurb 3'=>array(

					'type'=>'select_index',

					'opt'=>$allpage,

					'desc'=>'choose the title you want to appear in the blurb menu 3.'

				),

				'Blurb 4'=>array(

					'type'=>'select_index',

					'opt'=>$allpage,

					'desc'=>'choose the title you want to appear in the blurb menu 4.'

				)

			),

			'Featured Slider'=>array(

				'Display Featured Slider'=>array(

					'type'=>'on_off',

					'desc'=>'It allows enable/disable the featured you want to display in the slider.'

				),

				'Featured Category'=>array(

					'type'=>'select_index',

					'desc'=>'choose the category you want to feature in the slider.',

					'opt'=>$allcategory

				)				

			)

		),

		'Ad Management'=>array(

			0 => array(

				'set_468x60_banner' => array(

					'label'=>'Set 468x60 banner',

					'type'=>'on_off',

					'desc'=>'You can disable/enable a banner for advertising'

				),

				'img_468x60' => array(

					'label'=>'Input 468x60 advertisement banner image',

					'type'=>'image_upload',

					'desc'=>'Put an advertising banner image and then drop the image in the box.'

				),

				'a_468x60' => array(

					'label'=>'Input 468x60 advertisement destination url',

					'type'=>'textarea',

					'desc'=>'Put the url of your desired image.'

				)

			)

		),

		'Typography'=>array(

			'Top Menu'=>array(

				'font_size_menu1'=>array(

					'label'=>'Font Size',

					'type'=>'text',

					'desc'=>'- Enter here the desired size of a font and add the unit "px"

                   (e.g.: 12px)'

				),

				'font_color_menu1'=>array(

					'label'=>'Text Color',

					'type'=>'color',

					'desc'=>'Enter the color of the text. You can use a HEX value - like "#ff0000", an RGB value like "rgb (255,0,0)", a color name - like "red" '

				),

				'font_family_menu1'=>array(

					'label'=>'Font Family',

					'type'=>'select_index',

					'opt'=>$allfonts,

					'desc'=>'Enter the name of a font, like "verdana", "tahoma", "arial", etc.',

				),

				'hover_color_menu1'=>array(

					'label'=>'Hover Color',

					'type'=>'color',

					'desc'=>'Enter the desired color for hover (Tip:  hover selector is used to select text when you mouse over them.) '

				)

			),

			'Entry Titles'=>array(

				'font_size_entry_titles'=>array(

					'label'=>'Font Size',

					'type'=>'text',

					'desc'=>'Enter here the desired size of a font and add the unit "px"

                   (e.g.: 12px)

'

				),

				'font_color_entry_titles'=>array(

					'label'=>'Text Color',

					'type'=>'color',

					'desc'=>'Enter the color of the text. You can use a HEX value - like "#ff0000", an RGB value like "rgb (255,0,0)", a color name - like "red"'

				),

				'font_family_entry_titles'=>array(

					'label'=>'Font Family',

					'type'=>'select_index',

					'opt'=>$allfonts,

					'desc'=>'Enter the name of a font, like "verdana", "tahoma", "arial", etc.',

				)

			),

			'Content Texts'=>array(

				'font_size_content_texts'=>array(

					'label'=>'Font Size',

					'type'=>'text',

					'desc'=>'Enter here the desired size of a font and add the unit "px"

                   (e.g.: 12px)

'

				),

				'font_color_content_texts'=>array(

					'label'=>'Text Color',

					'type'=>'color',

					'desc'=>'Enter the color of the text. You can use a HEX value - like "#ff0000", an RGB value like "rgb (255,0,0)", a color name - like "red"'

				),

				'font_family_content_texts'=>array(

					'label'=>'Font Family',

					'type'=>'select_index',

					'opt'=>$allfonts,

					'desc'=>'Enter the name of a font, like "verdana", "tahoma", "arial", etc.',

				)

			),

			'Links'=>array(

				'font_size_links'=>array(

					'label'=>'Font Size',

					'type'=>'text',

					'desc'=>'Enter here the desired size of a font and add the unit "px"

                   (e.g.: 12px)

'

				),

				'font_color_links'=>array(

					'label'=>'Text Color',

					'type'=>'color',

					'desc'=>'Enter the color of the text. You can use a HEX value - like "#ff0000", an RGB value like "rgb (255,0,0)", a color name - like "red" '

				),

				'font_family_links'=>array(

					'label'=>'Font Family',

					'type'=>'select_index',

					'opt'=>$allfonts,

					'desc'=>'Enter the name of a font, like "verdana", "tahoma", "arial", etc.',

				),

				'hover_color_links'=>array(

					'label'=>'Hover Color',

					'type'=>'color',

					'desc'=>'Enter the desired color for hover (Tip:  hover selector is used to select text when you mouse over them.)  '

				)

			)

		),

		'SEO'=>array(

			'Homepage SEO'=>array(

				'homepage_set_custom_title'=>array(

					'label'=>'Set Custom Title',

					'type'=>'on_off',

					'desc'=>'Under Construction'

				),

				'homepage_set_meta_description'=>array(

					'label'=>'Set Meta Description',

					'type'=>'on_off',

					'desc'=>'Under Construction'

				),

				'homepage_set_meta_keywords'=>array(

					'label'=>'Set Meta Keywords',

					'type'=>'on_off',

					'desc'=>'Under Construction'

				),

				'homepage_set_canonical'=>array(

					'label'=>'Set canonical URL&rsquo;s',

					'type'=>'on_off',

					'desc'=>'Under Construction'

				),

				'sep'=>array(),

				'homepage_custom_title'=>array(

					'label'=>'Homepage custom title (Must be enabled)',

					'type'=>'text',

					'desc'=>'Under Construction'

				),

				'homepage_meta_description'=>array(

					'label'=>'Homepage meta description (Must be enabled)',

					'type'=>'textarea',

					'desc'=>'Under Construction'

				),

				'homepage_meta_keywords'=>array(

					'label'=>'Homepage meta keywords (Must be enabled)',

					'type'=>'text',

					'desc'=>'Under Construction'

				),

				'homepage_title_type'=>array(

					'label'=>'If custom titles are disabled, choose autogeneration method',

					'type'=>'select',

					'opt'=>array("BlogName | Blog description","Blog description | BlogName","BlogName only"),

					'desc'=>'Under Construction'

				),

				'homepage_title_separator'=>array(

					'label'=>'Define a character to separate BlogName and Post title',

					'type'=>'text',

					'desc'=>'Under Construction'

				)

				

			),

			'Single Post Page SEO'=>array(),

			'Index Page SEO'=>array()

		),

		'Integration'=>array(

			0=>array(

				'Header Code'=>array(

					'type'=>'textarea',

					'desc'=>'Anything you put here will be added to every page of this theme. This is usefull for inserting scripts or styles at the header section.'

				),

				'Activate Header Code'=>array(

					'type'=>'on_off',

					'desc'=>'Under Construction.'

				),

				'Footer Code'=>array(

					'type'=>'textarea',

					'desc'=>'Anything you put here will be added to every page of this theme. This is usefull for inserting scripts or styles at the footer section.'

				),

				'Activate Footer Code'=>array(

					'type'=>'on_off',

					'desc'=>'Under Construction.'

				),

			)

		),

		'Documentation'=>array(

			

		)

	);

	foreach($arr2 as $key=>$val){

		if(array_key_exists($key,$arr)) {

			$arr[$key]+=$arr2[$key];

		}

	}

	if($rem_arr!=''){

		foreach($rem_arr as $key=>$val){

			foreach($val as $k=>$v){

				if(array_key_exists($k,$arr)){				

					unset($arr[$k][$v]);

				}

			}			

		}

	}

	return $arr;

}

function header_code(){

	if(get_atom_option("activate_header_code")=="yes"){

		echo get_atom_option("header_code");

	}

}

function footer_code(){

	if(get_atom_option("activate_footer_code")=="yes"){

		echo get_atom_option("footer_code");

	}

}

add_action('wp_head','header_code');

add_action('wp_footer','footer_code');



function atom_string($str){

	return str_replace('\"','',urldecode($str));

}

function input_type($k, $v){

	$value = get_settings_value();

	switch($v['type']){

		case 'text': ?>

        	<input type="text" name="<?php echo slug($k) ?>" value="<?php echo atom_string($value[slug($k)]) ?>" />

			<?php break;

		case 'textarea': ?>

        	<textarea name="<?php echo slug($k) ?>" rows="5" cols="60"><?php echo atom_string($value[slug($k)]) ?></textarea>

        	<?php break;

		case 'file': ?>

        	<input type="file" name="<?php echo slug($k) ?>" value="<?php echo atom_string($value[slug($k)]) ?>" />

            <em><strong><?php echo atom_string($value[slug($k)]) ?></strong></em>

	        <?php break;

		case 'select': ?>

        	<select name="<?php echo slug($k) ?>" >

                <?php

				foreach($v['opt'] as $key=>$val){

				echo "<option value='".slug($val)."' ";

				if($value[slug($k)]==slug($val)) echo "selected ";

				echo ">$val</option>";

				}

				?>

            </select>

        	<?php break; 

		case 'select_index': ?>

			<select name="<?php echo slug($k) ?>" >

                <?php

				foreach($v['opt'] as $key=>$val){

				echo "<option value='".$key."' ";

				if($value[slug($k)]==$key) echo "selected ";

				echo ">$val</option>";

				}

				?>

            </select>

			<?php break;

		case 'on_off': 

			if(atom_string($value[slug($k)])=="on"){

				?> 

                <input type="checkbox" name="<?php echo slug($k) ?>" checked="checked" /> 

				<a href="#" class="btn_on_off atom_button active">This is <strong>Enabled</strong></a>

				<?php

			}

			else{

				?> 

                <input type="checkbox" name="<?php echo slug($k) ?>" /> 

				<a href="#" class="btn_on_off atom_button">This is <strong>Disabled</strong></a>

				<?php

			}

			break;

		case 'image_upload':

			?>

            <input class="txt_upload_image" type="text" name="<?php echo slug($k) ?>" value="<?php echo atom_string($value[slug($k)]) ?>" />

            <a href="#" class="atom_button btn_image_upload btn_upload_image_reset">Reset</a>

            <a href="#" class="atom_button btn_image_upload btn_upload_image">Upload Image</a>            

            <?php

			break;

		case 'color':

			?>

            <div class="color-picker-holder" style="position: relative;">

                <input type="text" name="<?php echo slug($k) ?>" class="color-picker" id="<?php echo slug($k) ?>" value="<?php echo atom_string($value[slug($k)]) ?>" style="background-color:<?php echo atom_string($value[slug($k)]) ?>" />

                <div style="position: absolute;" class="fabox" id="cp-<?php echo slug($k) ?>"></div>

            </div>

            <?php

			break;

		case 'hr':

			echo "<hr />";

			break;

		case 'docs': 

			echo $v['content'];

			break;

	}

}

function get_settings_value(){

	return get_option(strtolower(THEME_NAME).'_settings');

}

function save_settings(){

	unset($_POST['submit']);

	foreach($_POST as $key=>$val){

		$_POST[$key] = urlencode($val);

	}

	update_option(strtolower(THEME_NAME).'_settings',$_POST);

	echo "Success";

}

function get_atom_option($option,$default=''){
	$arr=get_option(strtolower(THEME_NAME)."_settings");
	if(!isset($arr[$option]) || $arr[$option]==''){
		return $default;
	}
	else{
		return atom_string($arr[$option]);		
	}
}

function default_settings(){

	delete_option(strtolower(THEME_NAME).'_settings');

	echo "Success";

}

?>