<?php 
/** Template Name: Portfolio*/ 
get_header() ?>

<div id="main" class="wrapper clearfix">
	<div id="content" class="portfolio">    	
		<?php		
		query_posts('post_type=attachment&post_mime_type=image&post_status=inherit&posts_per_page=3&paged='.get_query_var('paged'));
		if(have_posts()) : while(have_posts()): the_post();
			$feat_size='h=200&w=300&zc=1';
			?>
            <div class="entry-thumb">
                <a href="<?php echo $post->guid ?>" rel='lightbox'>
                    <span class="frame"></span>                
                    <?php get_featured_image($post->ID,'large',$feat_size); ?>
                </a>
            </div>
        <?php endwhile; endif; ?>    
        <div class="clear"></div>
        <div class="wp-pagenavi"><?php atom_pagination(); ?></div>    
    </div>
</div>
<?php get_footer() ?>