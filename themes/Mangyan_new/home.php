<?php 
/**
 * Template Name: Homepage
 *
 * @package WordPress
 * @subpackage 
 * @since 
 */
get_header();  ?>
	
	<div class="container">
		<div class="hero-unit" id="featured">
			<?php get_featured_items() ?>
	     </div>
	</div>

	<section id="main">
		<div class='container maincontent' >
			<div class="container">
				<div class="row-fluid">
						<div class="span12">

							<div class="span8">
								<div class="row-fluid">
									<div class="span6">
										<h3 class="title"><a href="">Sample Page</a></h3>
										<div class="span10"><img src="<?= THEME_URL?>/images/blurb3.png"> This is an example page. It’s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this: Hi there! I’m a bike messenger [...]
										</div>
									</div>
									<div class="span6">
										<h3 class="title"><a href="">Sample Page</a></h3>
										<div class="span10">
											<img src="<?= THEME_URL?>/images/blurb3.png"> This is an example page. It’s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this: Hi there! I’m a bike messenger [...]
										</div>
									</div>
								</div>
								<div class="row-fluid">
									<div class="span6">
										<h3 class="title"><a href="">Sample Page</a></h3>
										<div class="span10">
											<img src="<?= THEME_URL?>/images/blurb3.png"> This is an example page. It’s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this: Hi there! I’m a bike messenger [...]
										</div>
									</div>
									<div class="span6">
										<h3 class="title"><a href="">Sample Page</a></h3>
										<div class="span10"><img src="<?= THEME_URL?>/images/blurb3.png"> This is an example page. It’s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this: Hi there! I’m a bike messenger [...]
										</div>
									</div>
								</div>
							</div>

							<div class="span4 pull-right" id="latest-posts">
								
								<h3 class="title">Latest News</h3>
				                <?php query_posts('showposts=3&post_type=post'); if(have_posts()) : while(have_posts()): the_post(); ?>
				                
				                <div class="entry clearfix">
				                	
				                    <div class="entry-thumb">
				                        <a href="<?php the_permalink() ?>">
				                        <?php 
										if(has_post_thumbnail()) :
											get_featured_image($post->ID,'medium','h=70&w=108&zc=1'); 
										else : ?>
				                        	<img src="<?php echo THEME_URL ?>/images/latest-news.png" />
				                        <?php endif; ?>
				                        </a>
				                        
				                    </div>
				                    <h3 class="entry-title">
				                    	<a href="<?php the_permalink() ?>">
				                    	<?php the_title(); ?>
				                    	</a>
				                    </h3>
				                    <div class="entry-excerpt">
				                        <?php echo atom_truncate(atom_get_the_content(),84); ?>
				                    </div>
				                    
				                </div>
				                
				                <?php endwhile; endif; wp_reset_query(); ?>

						</div>

						</div>	

						
				 </div>
			</div>
		</div>

	</section>

<?php get_footer(); ?>


