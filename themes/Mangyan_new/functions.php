<?php

/* ----------------------------------------------- [ #1 : Don't remove this ] ------------------------*/

require('base/base.php');
include('lib/responsive.php');

register_nav_menu( 'primary', __( 'Primary Menu', 'atom' ) );
register_nav_menu( 'primary', __( 'Primary Menu', 'atom' ) );
register_nav_menu( 'footer', __( 'Footer Menu', 'atom' ) );

add_theme_support( 'post-thumbnails');
add_theme_support( 'automatic-feed-links' );

    register_sidebar();

/* ----------------------------------------------- [/ #1 ] ------------------------*/

function get_featured_items()
{	
	$htmlContent = "";
	query_posts('category_name=Featured Items&orderby=title&order=asc&show_post=1&posts_per_page=4');
	while (have_posts()) : the_post(); 
		$htmlContent .= '<h1>'.the_title().'</h1> <p>'.the_excerpt().'</p><p><a class="btn btn-primary btn-large" href="#">Learn more »</a></p>';
	endwhile;

	print $htmlContent;
}



?>

