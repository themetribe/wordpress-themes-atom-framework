<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Under Construction</title>
	<meta charset="utf-8">
	<meta name="author" content="Themetribe">
	
	<link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/base/css/bootstrap.css" />
	<?php wp_head(); ?>
	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />	
	
</head>
<body <?php body_class(); ?>>

		<div class="topheader">
			<div class="container">
				<div class="navbar navbar-inverse">
					<div class="navbar-inner">
				        <div class="container">
				          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				            <span class="icon-bar"></span>
				          </button>
				          <div class="nav-collapse collapse pull-right" id="menu-top">
				            	<?php wp_nav_menu( array('theme_location' => 'primary', 'menu_class' => 'sf-menu', 'depth'=>1, 'menu'=>'Top-Menu') ); ?>
	    				 </div>
				        </div>
				     </div>
				 </div>
			</div>
		</div>

		<div class="container">
				<!-- SITE LOGO and Description -->
			<section id='branding'>
				<div class='row'>
					<div class='span12'>
						<?php site_title_logo(); ?>
					</div>					
				</div>
				<div class='row'>
					<div class='span12'>
						<?php site_description(); ?>
					</div>
				</div>
			</section>
			<!-- end Site Logo and Description -->
		</div>
		<div class="linecontainer">
			<div class="container">
				<div class='row'>
					<nav id="menu-header" class='span12'>
    				<?php wp_nav_menu( array( 'theme_location' => 'secondary', 'menu_class' => 'sf-menu','menu'=>'menu-header') ); ?>
					</nav>	
				</div>
			</div>
		</div>
		<br/>
		<div class="container">
			<div class="hero-unit" id="featured">
				<?php //get_featured_items() ?>
		     </div>
		</div>
	
