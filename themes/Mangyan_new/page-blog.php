<?php 

/** Template Name: Blog  */ 
// if you prefer to change the html structure for the blog page, please edit this file instead and remove the include.

$tt_blog_type = get_post_meta($post->ID,'tt_blog_type',true);
switch($tt_blog_type){
	case "full-thumbnail-left-sidebar":
		include(BASE_DIR.'/lib/page-templates/blog-left-sidebar-full-thumb.php');		
		break;
	case "full-thumbnail-right-sidebar":
		include(BASE_DIR.'/lib/page-templates/blog-right-sidebar-full-thumb.php');		
		break;	
	case "medium-thumbnail-left-sidebar":
		include(BASE_DIR.'/lib/page-templates/blog-left-sidebar-med-thumb.php');		
		break;
	case "medium-thumbnail-right-sidebar":
		include(BASE_DIR.'/lib/page-templates/blog-right-sidebar-med-thumb.php');		
		break;
}

?>