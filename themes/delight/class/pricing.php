<?php
    class CPricing
    {        
        public $featureList;
        public $currency;
        public $priceStandard;
        public $pricePremium;
        public $priceDeveloper;
        
        function __construct()
        {
            $this->currency = "PHP";
            $this->priceStandard = "0.01";
            $this->pricePremium = "0.01";
            $this->priceDeveloper = "0.01";
        }
        
        function setFeature($type)
        {
            switch($type)
            {                
                case 0: // standard
                    $this->featureList = array("Single Theme", "Lifetime Ownership");
                    break;
                case 1: // premium
                    $this->featureList = array("All Themes", "Member for a year (extensible)");
                    break;
                case 2: // developer
                    $this->featureList = array("All Themes", "Member for a year (extensible)");
                    break;
            }
            
            $this->featureList[] = "Unlimited Domain Use";
            $this->featureList[] = "Unlimited Support";
            $this->featureList[] = "Include Future Theme Updates";
        }
    }
?>
