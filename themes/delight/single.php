<?php get_header(); ?>
<?php global $themePostOptions, $savedFiles; extract($savedFiles); extract($themePostOptions); ?>

<div id="main" class="wrapper clearfix">
	<?php if($dl_breadCrumbs=='enable') breadcrumbs(); ?>
	<div id="content">
    <?php
	if(have_posts()) : while(have_posts()): the_post(); ?>
		<div class="clearfix entry">        	
            <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>  
           	<div class="meta"><?php post_meta() ?></div>
            <?php if(has_post_thumbnail()): ?>
            	<div class="thumb"><a href="<?php the_permalink(); ?>"><?php get_featured_image($post->ID,'large','h=180&w=294&zc=1') ?></a></div>            
            <?php endif; ?>
            <div class="excerpt">
            	<?php the_content() ?>
            </div>
        </div>
    
        <?php if($dl_468x60_banner=="enable") : ?>
        	<div id="ads_468x60">
            	<?php if(trim($dl_468x60_imgsrc)!=''): ?>
	                <a href="<?php echo $dl_468x60_url ?>"><img src="<?php echo $dl_468x60_imgsrc; ?>" /></a>
                <?php else: ?>
					<img src="<?php echo bloginfo('template_url') ?>/images/468x60.jpg" />            
                <?php endif; ?>
            </div>
        <?php endif; ?>
        
        <div id="author-info" class="clearfix">
			<?php
            $email = get_the_author_meta('user_email'); 
            $nice_name = get_the_author_meta('display_name'); 			
            $description = get_the_author_meta('description');
            ?>
            <h3 class="title">The Author</h3>
            <div class="thumb">
                <span class="frame"></span>
                <?php echo get_avatar($email,70) ?>
            </div>
            <p id="author_name"><?php echo $nice_name ?></p>
            <p id="author_desc"><?php echo $description ?></p>
        </div>
		
        <?php comments_template( '', true ); ?>
                    
		<?php endwhile; endif; ?>        
        <?php wp_reset_query(); ?>
    </div>
    <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>