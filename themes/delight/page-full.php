<?php /** Template Name: Full Width Page  */ 
get_header(); ?>
<section id="main">	
	<section id="content" class="clearfix wrapper fullwidth">
    	<?php 
		if(have_posts()) : while(have_posts()): the_post(); 
			the_content(); 
		endwhile; endif; 
		?>
	</section>
</section>
<?php get_footer() ?>