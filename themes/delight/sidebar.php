<section id="sidebar" class="sidebar right" role="complementary">
	<ul>
	<?php if ( ! dynamic_sidebar( 'default-sidebar' ) ) : ?>
        <aside id="archives" class="widget-container">
            <h3 class="widget-title"><?php _e( 'Archives', 'twentyeleven' ); ?></h3>
            <ul>
                <?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
            </ul>
        </aside>

        <aside id="meta" class="widget-container">
            <h3 class="widget-title"><?php _e( 'Meta', 'twentyeleven' ); ?></h3>
            <ul>
                <?php wp_register(); ?>
                <li><?php wp_loginout(); ?></li>
                <?php wp_meta(); ?>
            </ul>
        </aside>

    <?php endif; // end sidebar widget area ?>
    </ul>
</section>