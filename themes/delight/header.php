<?php global $themePostOptions, $savedFiles; extract($savedFiles); extract($themePostOptions); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php if($dl_canonicalUrl=='enable') : ?>
	<link rel="canonical" href="<?php echo get_permalink() ?>" />
<?php endif; 
	wp_head(); ?>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<?php if(trim($dl_customHomepageMetaDesc)!='' && (is_home() || is_front_page())) : ?>
	<META NAME="Description" CONTENT="<?php echo trim($dl_customHomepageMetaDesc) ?>">
<?php elseif(trim($dl_metaDesc)!='') : ?>
	<META NAME="Description" CONTENT="<?php echo trim($dl_metaDesc) ?>">
<?php endif; ?>
<?php if(trim($dl_customHomepageMetaKeywords)!='' && (is_home() || is_front_page())) : ?>
	<META NAME="Keywords" CONTENT="<?php echo trim($dl_customHomepageMetaKeywords) ?>">
<?php elseif(trim($dl_metaKeyword)!='') : ?>
	<META NAME="Keywords" CONTENT="<?php echo trim($dl_metaKeyword) ?>">
<?php endif; ?>
<?php if(trim($dl_metaAuthor)!='') : ?>
	<META NAME="Author" CONTENT="<?php echo trim($dl_metaAuthor) ?>">
<?php endif; ?>
<?php if($dl_favIcon!=array()) : ?>
	<link rel="shortcut icon" type="<?php echo $dl_favIcon['type'] ?>" href="<?php echo $dl_favIcon['url'] ?>">
<?php endif; ?>
<?php if($dl_customCss!=array()) : ?>
	<link rel="stylesheet" type="text/css" href="<?php echo $dl_customCss['url'] ?>" />
<?php endif; ?>
<title>
	<?php 
	if(trim($dl_customHomepageTitle)!='' && (is_home() || is_front_page()))
		echo $dl_customHomepageTitle;
	else
		labtitles(); 
	?>
</title>
</head>

<body <?php body_class(); ?>>

<header id="header">
	<div class="wrapper clearfix">
        <hrgroup id="branding">
        	<?php if($dl_siteDescEnabled=='enable') : ?>
	        	<h2 id="site-description"><?php bloginfo( 'description' ); ?></h2>
            <?php endif; ?>
            <?php if($dl_siteTitleType=='titleText') : ?>          
            	<h1 id="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>            
            <?php else: ?>
            	<?php if($dl_logoImage['url']!='') : ?>
            		<h1 id="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
                    	<img src="<?php echo $dl_logoImage['url'] ?>" />
                    </a></h1>            
                <?php else: ?>
                	<h1 id="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">Delight</a></h1>
                <?php endif; ?>
            <?php endif; ?>
            
        </hrgroup>
        <nav id="access" role="navigation">
        	<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'sf-menu', 'container'=>'div') ); ?>
        </nav>
    </div>
</header>