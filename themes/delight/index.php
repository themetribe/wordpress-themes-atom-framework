<?php get_header() ?>
<?php if(is_category()) : ?>
<div id="main" class="wrapper clearfix">
	<div id="content">
    <?php
	if(have_posts()) : while(have_posts()): the_post(); ?>
		<div class="clearfix entry">        	
            <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>            
            <div class="meta"><?php post_meta() ?></div>
            <?php if(has_post_thumbnail()): ?>
            	<div class="thumb"><a href="<?php the_permalink(); ?>"><?php get_featured_image($post->ID,'large','h=180&w=294&zc=1') ?></a></div>            
            <?php endif; ?>
            <div class="excerpt">
            	<?php echo patico_truncate(patico_get_the_content(),500) ?>
            </div>
            <a class="atom_button grey small" href="<?php the_permalink() ?>">Read More <span>&raquo;</span></a>
        </div>
        <?php endwhile; endif; ?>  
        <div class="page-navi clearfix">
			<?php atom_pagination(); ?>
        </div>		      
        <?php wp_reset_query(); ?>
    </div>
    <?php get_sidebar(); ?>
</div>
<?php else: ?>
<section id="main">	
	<section id="content" class="clearfix wrapper fullwidth">
    	<?php 
		if(have_posts()) : while(have_posts()): the_post(); 
			the_content(); 
		endwhile; endif; 
		?>
	</section>
</section>
<?php endif; ?>

<?php get_footer() ?>
