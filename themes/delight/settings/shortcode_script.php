<?php 

function shortcode() { ?>
<script>
function loadfields(tag){	
	<?php
	$pages = get_pages(); $allpage = ""; foreach ($pages as $page) { $allpage .= $page->post_title."|"; } $allpage = substr($allpage,0,-1);
	$categories = get_categories('hide_empty=0');$allcategory = "";  foreach ($categories as $category) { $allcategory .= $category->cat_name."|";} $allcategory = substr($allcategory,0,-1);
	?>
	var tf_settings = new Array();
	var str = '';
	
	/* SHORTCODE DEFAULT SETTINGS =============
	   1. Blog
	*/
	tf_settings['blog']={
		count:{
			type: 'text',
			name: 'Count',
			description: 'Input how many post you want in your blog shortcode.'
		},
		category:{
			type: 'select',
			opt : '<?php echo $allcategory ?>',
			name : 'Category',
			description: 'Choose which category to populate in your blog shortcode.'
		},
		nopaging:{
			type: 'check',
			name: "No Paging",
			description: 'Check this to disable post pagination.'
		},
		nometa:{
			type: 'check',
			name: "No Meta Description",
			description: 'Check this to disable meta discription.'
		},
		noimage:{
			type: 'check',
			name: "No Image",
			description: 'Check this to disable post image thumbnail.'
		}
	};
	
	/* 2. Box */
	tf_settings['box']={
		type:{
			type: 'select',
			opt : 'Error|Note|Notice|Success',
			name : 'Type',
			description: 'Choose what type of box you want.'
		},
		title:{
			type: 'text',
			name: 'Title',
			description: 'This option is only available for <strong>Error</strong> and <strong>Note</strong> type box .'
		},
		width:{
			type: 'text',
			name: 'Width',
			description: 'You can specify the width of your box. This is best used together with <strong>Align attribute</strong>.'
		},
		align:{
			type: 'select',
			opt : '|left|right',
			name: 'Align',
			description: 'Choose where you want to position your box. This is best used together with <strong>Width attribute</strong> inside a paragraph.'
		},
		content:{
			type: 'text',
			name: 'Content',
			description: 'Here is where you are going to input the text inside the box.'
		}		
	};
	
	/* 3. Button */
	tf_settings['button']={
		size:{
			type: 'select',
			opt : 'Small|Medium|Large',
			name : 'Size',
			description: 'Choose what size of button you want.'
		},
		color:{
			type: 'select',
			opt : 'black|grey|white|blue|green|red|yellow|orange|pink|rosy|magenta',
			name : 'Color',
			description: 'Choose what color you like.'
		},
		bgcolor:{
			type: 'text',
			name : 'Background Color',
			description: 'Specify the hex value of the backgroundcolor you like. <pre>Hex Color Example: #769600</pre>'
		},
		textcolor:{
			type: 'text',
			name : 'Text Color',
			description: 'Specify the hex value of the text color you like. '
		},
		id:{
			type: 'text',
			name : 'ID',
			description: 'Specify the ID of the button. Ideal for creating custom styles and scripts.'
		},
		class:{
			type: 'text',
			name : 'Class',
			description: 'Specify the Class of the button. Ideal for creating custom styles and scripts.'
		},
		url:{
			type: 'text',
			name : 'Link (URL)',
			description: 'Specify the Link of the button when clicked.'
		},
		content:{
			type: 'text',
			name : 'Content',
			description: 'Specify the text/label of the button.'
		}
	};

	/* 4. Columns is not included in the Tiny MCE 
	   5. Contact Form is not included in the Tiny MCE
	   6. Gallery is Wordpress shortcode so no need for Tiny MCE
	*/
	
	/* 7. Google Chart */
	tf_settings['gchart']={
		type:{
			type: 'select',
			opt : 'Pie|Column',
			name : 'Type',
			description: 'Choose what type of chart you like.'
		},
		title:{
			type: 'text',
			name : 'Title',
			description: 'Specify the chart title.'
		},
		data:{
			type: 'text',
			name : 'Data',
			description: 'Specify the chart data. Use comma to separate data values. For <strong>type column</strong>, use `|` character to separate data of second label.'			
		},
		labels:{
			type: 'text',
			name : 'Labels',
			description: 'Specify the chart labels.'			
		},
		time:{
			type: 'text',
			name : 'Time',
			description: 'Specify the chart time. For <strong>type column</strong> use only.'			
		}
	};
	
	/* 8. Google Map */
	tf_settings['gmap']={
		address:{
			type: 'text',
			name : 'Address',
			description: 'Specify the address. If your address is not recognized by google, you may use the <strong>latitude</strong> and <strong>longitude</strong> attribute instead.'
		},
		latitude:{
			type: 'text',
			name : 'Latitude',
			description: 'Specify the map`s lattitude.'			
		},
		longitude:{
			type: 'text',
			name : 'Longitude',
			description: 'Specify the map`s longitude.'			
		}
	};
	
	/* 9. Image */
	tf_settings['image']={
		source:{
			type: 'text',
			name : 'Source',
			description: 'Specify the image url source.'
		},
		url:{
			type: 'text',
			name : 'URL (Source 2)',
			description: 'When used with lightbox, this serves as the image popup for lightbox. When used with links, this serves as the redirection link.'
		},
		title:{
			type: 'text',
			name : 'Title',
			description: 'Specify the title attribute of the image.'
		},
		alt:{
			type: 'text',
			name : 'Alternative Text',
			description: 'Specify the alt attribute of the image.'
		},
		width:{
			type: 'text',
			name : 'Width',
			description: 'Specify the width attribute of the image.'
		},
		height:{
			type: 'text',
			name : 'Height',
			description: 'Specify the height attribute of the image.'
		},
		_link:{
			type: 'option',
			name: "Link",
			description: 'Wraps the image with a link. Should be used with <strong>URL (Source 2)</strong> attribute.'
		},
		lightbox:{
			type: 'option',
			name: "Lightbox",
			description: 'Popup a jQuery Image Box when clicked. Should be used with <strong>URL (Source 2)</strong> attribute.'
		}
	};
	
	/* 10. Sidebar */
	tf_settings['sidebar']={
		type:{
			type: 'select',
			opt: 'Left|Right',
			name : 'Type',
			description: 'The position of the sidebar.'
		},
		name:{
			type: 'text',
			name : 'Name',
			description: 'The slug name of the sidebar widget area generated from the <strong>Sidebar Generator</strong>.'
		}
	};
	
	/* 11. Slider */
	tf_settings['slider']={
		type:{
			type: 'select',
			opt : 'cycle|nivo',
			name : 'Type',
			description: 'Choose what type of slider you like.'
		},
		category:{
			type: 'select',
			opt : '<?php echo $allcategory ?>',
			name : 'Category',
			description: 'Choose which category to populate in your slider.'
		},
		height:{
			type: 'text',
			name : 'Hieght',
			description: 'Specify the height of your slider in pixel.'
		},
		pagination:{
			type: 'check',
			name: "Pagination",
			description: 'Displays pagination at the bottom of the blog list withouth needing of a plugin.'
		},		
		shadow:{
			type: 'check',
			name: "Shadow",
			description: 'Only used for Nivo type slider.'
		}
	};
	
	/* 12. Table 
	tf_settings['table']={
		
	};*/
	
	/* 13. Typography 
	tf_settings['typography']={
		
	};*/
	
	/* 14. Video */
	tf_settings['video']={
		type:{
			type: 'select',
			opt : 'Flash|Youtube|Vimeo|Dailymotion',
			name : 'Type',
			description: 'Choose what type of video you need to embed.'
		},
		id:{
			type: 'text',
			name : 'ID',
			description: 'Paste here the video ID from the video provider. This is applicable except in <strong>Flash</strong> video type.'
		},
		src:{
			type: 'text',
			name : 'Src',
			description: 'Paste the Source of the video instead of using the ID. Only applicable in <strong>Flash </strong>video type.'
		},
		play:{
			type: 'text',
			name : 'Play',
			description: 'When set to <strong>True</strong>, the video will play automatically after loading. Only applicable in <strong>Flash </strong>video type.'
		}
	};
	
	var xa;
	for (xa in tf_settings[tag]) {
		_type = tf_settings[tag][xa].type;
		_name = tf_settings[tag][xa].name;	
		_desc = tf_settings[tag][xa].description;		
		switch(_type){
			case 'text':
				str += "<p class='tf_list'>"+_name + " <br /><input type='text' name='"+xa+"' id='"+xa+"' class='tf_option' /> <br /><span>"+_desc+"</span></p>\n";
				break;
			case 'select':
				_opt = tf_settings[tag][xa].opt.split('|');
				str += "<p class='tf_list'>"+ _name + " <br /><select name='"+xa+"' id='"+xa+"' class='tf_option'>";
				for(index in _opt){
					str+="<option name='"+jsSlug(_opt[index])+"'>"+_opt[index]+"</option>";
				}
				str += "</select> <br /><span>"+_desc+"</span></p>\n";
				break;
			case 'check':
				str += "<p class='tf_list'><input type='checkbox' name='"+xa+"' id='"+xa+"' class='tf_option' value='true' /> <label for='"+xa+"'>"+_name + "</label> <br /><span>"+_desc+"</span></p>\n";
				break;
			case 'option':
				str += "<p class='tf_list'><input type='radio' name='check_group_1' value='"+jsSlug(_name)+"' class='tf_option' /> <label for='"+xa+"'>"+_name + "</label> <br /><span>"+_desc+"</span></p>\n";
				break;
		}		
	}
	return str;
}
function loadOptions(tag){	
	
	var width = jQuery(window).width(),
		tbHeight = jQuery(window).height(),
		tbWidth = ( 720 < width ) ? 720 : width;
	
	tbWidth = tbWidth - 80;
	tbHeight = tbHeight - 84;
			
	tf_option_fields = loadfields(tag);		
	var tbOptions = "<div id='tf_shortcodes_div'><form id='tf_shortcodes'><table id='shortcodes_table'>";	
	tbOptions += tf_option_fields;
	tbOptions += '</table>\n<p class="submit">\n<input type="button" id="shortcodes-submit" class="button-primary" value="Insert into post" name="submit" /></p>\n</form></div>';
	
	var form = jQuery(tbOptions);	
	var table = form.find('table');
	
	form.appendTo('body');
	tb_show(ucwords(tag)+" Shortcode Generator",'#TB_inline?inlineId=tf_shortcodes_div&width='+tbWidth+'&height='+tbHeight );	
	
	var attr='';
	jQuery("#shortcodes-submit").click(function(){
		_content = "";
		jQuery("#tf_shortcodes .tf_option").each(function(index){
			_name = jQuery(this).attr('name');			
			_val = jQuery(this).val();
			_type = jQuery(this).attr('type');
			_id = "#"+_name.toString();
			if(_name!="content") {								
				if(_type=="checkbox"){
					if(this.checked)
						attr += _name+" ";
				}
				else if(_type=="radio"){
					if(this.checked)
						attr += _val+" ";
				}
				else if(_val==''){
					/*don't do anything*/
				}
				else	
					attr += _name+"='"+_val+"' ";
			}
			else{
				_content=_val;
			}
		});
		
		if(_content!=''){
			sc = "["+tag+" "+attr+"]"+_content+"[/"+tag+"]";
		}
		else{
			sc = "["+tag+" "+attr+"]";
		}
		
		attr='';
		tinyMCE.activeEditor.execCommand('mceInsertContent', 0, sc);		
		tb_remove();
	});
	jQuery('#tf_shortcodes_div').remove();	
}

function jsSlug(str){
	str = str.replace(/^\s+|\s+$/g, ''); // trim
	str = str.toLowerCase();
	
	// remove accents, swap ñ for n, etc
	var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
	var to   = "aaaaeeeeiiiioooouuuunc------";
	for (var i=0, l=from.length ; i<l ; i++) {
	  str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}
	
	str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
	  .replace(/\s+/g, '-') // collapse whitespace and replace by -
	  .replace(/-+/g, '-'); // collapse dashes
	
	return str;
}
</script>
<?php } ?>