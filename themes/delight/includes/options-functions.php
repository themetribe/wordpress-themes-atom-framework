<?php
    include_once($_SERVER['DOCUMENT_ROOT'].'/themepatico/wp-admin/includes/media.php');
    include_once($_SERVER['DOCUMENT_ROOT'].'/themepatico/wp-admin/includes/file.php');
    include_once($_SERVER['DOCUMENT_ROOT'].'/themepatico/wp-admin/includes/image.php');
    
    include_once(SITE_DIR.'/includes/options.php');    
    include_once(SITE_DIR.'/class/element.php');
    
    // lets create an array here to display the tabs and options    
    add_action('admin_head', 'my_admin_head');
    add_action('admin_menu', 'themefanaticsAdminMenu');
    
    // Do the saving  
    if($_POST) {  
        saveThemeOptions();
    }
    
    function my_admin_head() {
        echo '<link rel="stylesheet" type="text/css" href="' .SITE_URL . '/css/admin.css'. '">';
    }
    
    function themefanaticsAdminMenu()
    {
        global $options;        
        add_menu_page(THEMENAME, THEMENAME, 'administrator', basename(__FILE__), 'themefanaticsAdmin');
    }
    
    function processField($field)
    {        
        $themePostOptions = get_option("themePostOptions");
        $savedFiles = get_option("themeFilesOptions");
        
        $attributes = array();
        foreach($field as $attrib=>$value)
        {
            switch($attrib)
            {                
                case "element":
                    $element = $value;
                    break;
                case "function":    // This is set if element = custom
                    $function = $value;
                    break;
                case "label":
                    $label = $value;
                    break;
                case "desc":
                    $desc = $value;
                    break;
                case "options":
                    $options = $value;
                    break;
                case "type":
                    $type = $value;
                    $attributes[$attrib] = $value;
                    break;
                case "id":
                    $id = SHORTNAME . $value;
                    $attributes[$attrib] = $id;
                    $attributes["name"] = $id;
                    break;
                default:
                    $attributes[$attrib] = $value;
                    break;
            }
        }
        
        switch($type)
        {
            case "radio":
            case "checkbox":
            case "button":
                $class = "float-div";
                break;            
            default:
                break;
        }
        
        switch($element)
        {
            case "custom":
                if($function)
                {
                    $function($id);
                    return;
                }
                break;
            case "select":                
                foreach($options as $value=>$text)
				{
					$selected = "";
					
					if($themePostOptions[$id] == $value)
                    	$selected = "selected";
						
					$attributes["options"] .= "<option value='$value' $selected>$text</option>";	
				}
                break;
            case "textarea":
                $attributes["innerHTML"] = $themePostOptions[$id];
                break;
            case "input":
                $attributes["value"] = $themePostOptions[$id];
                break;
            default:
                break;
        }
        
        if($element)
        {
            if($label)
                echo "<div class='field-label'>$label:</div>";
                        
             // Create Elements
            switch($type)
            {
                case "radio":
                case "checkbox":
                    foreach($options as $value=>$text)
                    {
                        unset($attributes["checked"]);
                        unset($attributes["value"]);
                            
                        $attributes["value"] = $value;
                        if($themePostOptions[$id] == $value)
                            $attributes["checked"] = "checked";
                        
                        $elementClass = new CElement($element, $attributes);
                        echo "<div class='options-element $class'>$elementClass<span class='input-desc'>$text</span></div>";
                    }
                    break;
                case "file":                    
                    $fileParts = $savedFiles[$id];
                    if(isset($fileParts["file"]))
                    {                        
                        $currentFile = "<a href='{$fileParts["url"]}' target='_blank'>" . basename($fileParts["file"]) . "</a>";
                        $removeCb = "<input type='checkbox' name='{$id}Remove' value='1'> Remove";
                        $displayFile = "<div class='file-desc'>Current File: $currentFile $removeCb</div>
                                        <div class='clear'></div>";
                        $class = "float-element";
                    }                    
                default:
                    
                    $elementClass = new CElement($element, $attributes);
                    echo "<div class='options-element $class'>$elementClass</div> $displayFile";
                    break;
            }
        }
            
        if($desc)
            echo "<div class='field-desc $class'>$desc</div>";
    }

    // Build Custom Element Functions here
    function createCustomElement($id)
    {
        echo "<fieldset><legend>Custom Element</legend> 
                <input type='text' id='$id' value='$id' />
                This was created using custom element function</fieldset>";
    }
    
    /*
    * Theme Fanatics Theme Options    
    */
    function themefanaticsAdmin() 
    {

        global $options;
    ?>
    <div>
        <h2><?php echo THEMENAME; ?> Settings</h2>
    </div>
    <div>
        <form method="post" action="" enctype="multipart/form-data">
        <?php
            foreach ($options as $key=>$section)
            {
                echo "<div class='options-group-header'>$key</div>";
                echo "<div id='section-$key' class='options-group' style='display:none;'>";
                foreach($section as $field)
                {
                    echo "<div class='options-field'>";
                    processField($field);
                    echo "<div class='clear'></div>";
                    echo "</div>";
                          
                }
                echo "</div>";
            }
        ?>
         <div class="button-submit">
            <input type="submit" value="Save settings" class="button-primary"/>
        </div>
        </form>
    </div>
    
    <script>
        jQuery(".options-group-header").click(function ()
        {
            var html = jQuery(this).html();                
            jQuery("#section-" + html).slideToggle("slow", function(){
                jQuery("#section-" + html).css('height', jQuery("#section-" + html).height() + 'px');
            });
        });
        
        jQuery(function() 
        {
            var height = jQuery(document).height();
            jQuery(".form-container").height(height-100);
        })
    </script>
    
    <?php
    }
    function saveThemeOptions()
    {
        $uploadedFiles = array();
        $savedFiles = get_option("themeFilesOptions");
        foreach($_FILES as $key=>$file)
        {
            if($file['size'] > 0)
            {
                $savedFiles[$key];
                $overrides = array('test_form' => false); 
                $result = wp_handle_upload($file, $overrides);
                // Sample Output
                /*Array (   [file] => /homepages/12/d357832350/htdocs/themeprojs/themefanatics/wp-content/uploads/2012/04/2012-04-24_19464.png 
                            [url] => http://themefanatics.com/wp-content/uploads/2012/04/2012-04-24_19464.png 
                            [type] => image/png )*/
                $uploadedFiles[$key] = $result;
            }
            else // check if saved data already exists
            {
				if(isset($_POST["{$key}Remove"]))
					$uploadedFiles[$key] = array();
				else if($savedFiles[$key])
                    $uploadedFiles[$key] = $savedFiles[$key];
					
            }
        }
        if(sizeof($uploadedFiles) > 0)
            update_option("themeFilesOptions", $uploadedFiles);            
        update_option("themePostOptions", $_POST);
    }
    /*echo "<br /><br /><br /><br /><br /><br /><br /><br /><br />";
    $themePostOptions = get_option("themePostOptions");
    $savedFiles = get_option("themeFilesOptions");
    
	print_r($savedFiles['dl_logoImage']);
	
    print_r($themePostOptions);*/
    
?>