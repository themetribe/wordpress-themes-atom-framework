<?php
function atom_one_half($atts, $content = NULL){return '<div class="one_half">'.do_shortcode($content).'</div>';}
function atom_one_half_last($atts, $content = NULL){return '<div class="one_half last">'.do_shortcode($content).'</div><div class="clear"></div>';}
function atom_one_third($atts, $content = NULL){return '<div class="one_third">'.do_shortcode($content).'</div>';}
function atom_one_third_last($atts, $content = NULL){return '<div class="one_third last">'.do_shortcode($content).'</div><div class="clear"></div>';}
function atom_one_fourth($atts, $content = NULL){return '<div class="one_fourth">'.do_shortcode($content).'</div>';}
function atom_one_fourth_last($atts, $content = NULL){return '<div class="one_fourth last">'.do_shortcode($content).'</div><div class="clear"></div>';}

function atom_two_third($atts, $content = NULL){return '<div class="two_third">'.do_shortcode($content).'</div>';}
function atom_two_third_last($atts, $content = NULL){return '<div class="two_third last">'.do_shortcode($content).'</div><div class="clear"></div>';}

function atom_three_fourth($atts, $content = NULL){return '<div class="three_fourth">'.do_shortcode($content).'</div>';}
function atom_three_fourth_last($atts, $content = NULL){return '<div class="three_fourth last">'.do_shortcode($content).'</div><div class="clear"></div>';}
function atom_one_fifth($atts, $content = NULL){return '<div class="one_fifth">'.do_shortcode($content).'</div>';}
function atom_one_fifth_last($atts, $content = NULL){return '<div class="one_fifth last">'.do_shortcode($content).'</div><div class="clear"></div>';}
function atom_four_fifth($atts, $content = NULL){return '<div class="four_fifth">'.do_shortcode($content).'</div>';}
function atom_four_fifth_last($atts, $content = NULL){return '<div class="four_fifth last">'.do_shortcode($content).'</div><div class="clear"></div>';}
function atom_two_fifth($atts, $content = NULL){return '<div class="two_fifth">'.do_shortcode($content).'</div>';}
function atom_two_fifth_last($atts, $content = NULL){return '<div class="two_fifth last">'.do_shortcode($content).'</div><div class="clear"></div>';}

function atom_three_fifth($atts, $content = NULL){return '<div class="three_fifth">'.do_shortcode($content).'</div>';}
function atom_three_fifth_last($atts, $content = NULL){return '<div class="three_fifth last">'.do_shortcode($content).'</div><div class="clear"></div>';}

function atom_one_sixth($atts, $content = NULL){return '<div class="one_sixth">'.do_shortcode($content).'</div>';}
function atom_one_sixth_last($atts, $content = NULL){return '<div class="one_sixth last">'.do_shortcode($content).'</div><div class="clear"></div>';}

function atom_five_sixth($atts, $content = NULL){return '<div class="five_sixth">'.do_shortcode($content).'</div>';}
function atom_five_sixth_last($atts, $content = NULL){return '<div class="five_sixth last">'.do_shortcode($content).'</div><div class="clear"></div>';}


add_shortcode('one_half','atom_one_half');
add_shortcode('one_half_last','atom_one_half_last');
add_shortcode('one_third','atom_one_third');
add_shortcode('one_third_last','atom_one_third_last');
add_shortcode('one_fourth','atom_one_fourth');
add_shortcode('one_fourth_last','atom_one_fourth_last');
add_shortcode('two_third','atom_two_third');
add_shortcode('two_third_last','atom_two_third_last');
add_shortcode('three_fourth','atom_three_fourth');
add_shortcode('three_fourth_last','atom_three_fourth_last');
add_shortcode('one_fifth','atom_one_fifth');
add_shortcode('one_fifth_last','atom_one_fifth_last');
add_shortcode('four_fifth','atom_four_fifth');
add_shortcode('four_fifth_last','atom_four_fifth_last');
add_shortcode('two_fifth','atom_two_fifth');
add_shortcode('two_fifth_last','atom_two_fifth_last');
add_shortcode('two_fifth','atom_two_fifth');
add_shortcode('two_fifth_last','atom_two_fifth_last');

add_shortcode('three_fifth','atom_three_fifth');
add_shortcode('three_fifth_last','atom_three_fifth_last');

add_shortcode('one_sixth','atom_one_sixth');
add_shortcode('one_sixth_last','atom_one_sixth_last');

add_shortcode('five_sixth','atom_five_sixth');
add_shortcode('five_sixth_last','atom_five_sixth_last');

?>