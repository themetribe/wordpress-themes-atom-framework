<?php
add_action('wp_footer', 'jsfoot');
function jsfoot() { ?>
<script>
	
	/* ===== IE Compatible for HTML5 ===================================================*/
	
	document.createElement('header');
	document.createElement('footer');
	document.createElement('section');
	document.createElement('aside');
	document.createElement('nav');
	document.createElement('article');
	
	/* ===== Dropdown ==================================================================*/
	
	jQuery.noConflict();
	jQuery(document).ready(function(){ 
		jQuery("ul.sf-menu, div.sf-menu ul").supersubs({ 
			minWidth:    13,                                // minimum width of sub-menus in em units 
			maxWidth:    27,                                // maximum width of sub-menus in em units 
			extraWidth:  1                                  // extra width can ensure lines don't sometimes turn over 
															// due to slight rounding differences and font-family 
		}).superfish({ 
			delay:       500,                               // delay on mouseout 
			animation:   {height:'show',opacity:'show'},    // fade-in and slide-down animation 
			speed:       'medium',                            // faster animation speed 
			autoArrows:  false,                             // disable generation of arrow mark-up 
			dropShadows: false                              // disable drop shadows 
		});	
	});
	
</script>
<?php
} ?>