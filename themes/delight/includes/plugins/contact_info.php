<?php
class TF_contact_info extends WP_Widget{
	function TF_contact_info(){
		$widget_ops = array('classname' => 'tf-contact-info', 'description' => 'Displays your company`s contact info.' );
	    $this->WP_Widget('TF_contact_info', 'TF Contact Info', $widget_ops);
	}
	function form($instance){
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'mobile'=>'', 'mail'=>'', 'address' ) );
	    $title = $instance['title'];
		$mobile = $instance['mobile'];
		$mail = $instance['mail'];
		$address = $instance['address'];
		?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
        <p><label for="<?php echo $this->get_field_id('mobile'); ?>">Mobile: <input class="widefat" id="<?php echo $this->get_field_id('mobile'); ?>" name="<?php echo $this->get_field_name('mobile'); ?>" type="text" value="<?php echo attribute_escape($mobile); ?>" /></label></p>
        <p><label for="<?php echo $this->get_field_id('mail'); ?>">Email: <input class="widefat" id="<?php echo $this->get_field_id('mail'); ?>" name="<?php echo $this->get_field_name('mail'); ?>" type="text" value="<?php echo attribute_escape($mail); ?>" /></label></p>
        <p><label for="<?php echo $this->get_field_id('address'); ?>">Address: <input class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" type="text" value="<?php echo attribute_escape($address); ?>" /></label></p>
        <?php
	}
	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['mobile'] = $new_instance['mobile'];
		$instance['mail'] = $new_instance['mail'];
		$instance['address'] = $new_instance['address'];
		return $instance;
	}
	function widget($args, $instance){
		global $post;
		extract($args, EXTR_SKIP);
		echo $before_widget;
		
		$title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
		$mobile = empty($instance['mobile']) ? ' ' : apply_filters('widget_title', $instance['mobile']);
		$mail = empty($instance['mail']) ? ' ' : apply_filters('widget_title', $instance['mail']);
		$address = empty($instance['address']) ? ' ' : apply_filters('widget_title', $instance['address']);
	 
		if (!empty($title))
		  echo $before_title . $title . $after_title;;
	 
		// WIDGET CODE GOES HERE
		?>
        
        <ul class="tf_contact_info">
        	<?php if($mobile!=' ') : ?><li class="mobile"><?php echo html_entity_decode($mobile) ?></li><?php endif; ?>
            <?php if($mail!=' ') : ?><li class="mail"><?php echo html_entity_decode($mail) ?></li><?php endif; ?>
            <?php if($address!=' ') : ?><li class="address"><?php echo html_entity_decode($address) ?></li><?php endif; ?>
        </ul>
        <?php
	 
		echo $after_widget;
	}
}
add_action( 'widgets_init', create_function('', 'return register_widget("TF_contact_info");') ); ?>