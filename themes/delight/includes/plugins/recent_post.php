<?php
class TF_recent_post extends WP_Widget{
	function TF_recent_post(){
		$widget_ops = array('classname' => 'tf-recent-post', 'description' => 'Displays a list of recent posts by Themefanatics ideal for footer widget areas' );
	    $this->WP_Widget('TF_recent_post', 'TF Recent Post', $widget_ops);
	}
	function form($instance){
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'count'=>'3', 'show_meta'=>'', 'show_image' ) );
	    $title = $instance['title'];
		$count = $instance['count'];
		$show_meta = $instance['show_meta'];
		$show_image = $instance['show_image'];
		?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
        <p><label for="<?php echo $this->get_field_id('count'); ?>">Number of posts to show:</label> <input id="<?php echo $this->get_field_id('count'); ?>" name="<?php echo $this->get_field_name('count'); ?>" type="text" value="<?php echo attribute_escape($count); ?>" size="3"></p>
        <p>
            <input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id('show_meta'); ?>" name="<?php echo $this->get_field_name('show_meta'); ?>" <?php if(attribute_escape($show_meta)=="on") echo "checked"; ?> >
            <label for="<?php echo $this->get_field_id('show_meta'); ?>">Show Meta</label><br>            
            <input class="checkbox" type="checkbox" id="<?php echo $this->get_field_id('show_image'); ?>" name="<?php echo $this->get_field_name('show_image'); ?>" <?php if(attribute_escape($show_image)=="on") echo "checked"; ?> >
            <label for="<?php echo $this->get_field_id('show_image'); ?>">Show Thumbnail Image</label>
		</p>
        <?php
	}
	function update($new_instance, $old_instance){		
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['count'] = $new_instance['count'];
		$instance['show_meta'] = $new_instance['show_meta'];
		$instance['show_image'] = $new_instance['show_image'];
		return $instance;
	}
	function widget($args, $instance){
		global $post;
		extract($args, EXTR_SKIP);
		echo $before_widget;
		
		$title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
		$count = empty($instance['count']) ? ' ' : apply_filters('widget_title', $instance['count']);
		$show_meta = empty($instance['show_meta']) ? ' ' : apply_filters('widget_title', $instance['show_meta']);
		$show_image = empty($instance['show_image']) ? ' ' : apply_filters('widget_title', $instance['show_image']);
	 
		if (!empty($title))
		  echo $before_title . $title . $after_title;;
	 
		// WIDGET CODE GOES HERE
		?>
        
        <ul class="tf_recent_posts">
        	<?php 
			query_posts("posts_per_page=$count");
			if(have_posts()) : while(have_posts()): the_post(); ?>
        	<li class="clearfix">
            	<a href="<?php the_permalink(); ?>">
					<?php if(has_post_thumbnail()): ?>
                        <?php get_featured_image($post->ID,'large','h=68&w=98&zc=1') ?>
                    <?php endif; ?>
                    <p class="title"><?php the_title() ?></p>
                </a>
                <p class="date"><?php post_meta_date() ?></p>
            </li>
            <?php endwhile; endif; ?>
        </ul>
        <?php
	 
		echo $after_widget;
	}
}
add_action( 'widgets_init', create_function('', 'return register_widget("TF_recent_post");') ); ?>