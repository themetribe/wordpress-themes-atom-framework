<?php
/*
Plugin Name: Ads 125x125 (banner)
Description: This plugin displays 4 Ads banner 125x125
*/
class Ads_125x125 extends WP_Widget{
	function Ads_125x125(){
		$widget_ops = array('classname' => 'ads-125x125', 'description' => 'Displays 4 125x125 ads banner' );
	    $this->WP_Widget('Ads_125x125', 'Ads 125x125', $widget_ops);
	}
	function form($instance){
		$instance = wp_parse_args( (array) $instance, array( 'title' => '', 'code1'=>'', 'code2'=>'', 'code3'=>'', 'code4'=>'' ) );
	    $title = $instance['title'];
		$code1 = $instance['code1'];
		$code2 = $instance['code2'];
		$code3 = $instance['code3'];
		$code4 = $instance['code4'];
		?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
        <p><label for="<?php echo $this->get_field_id('code1'); ?>">Ads Banner 1 Code: <textarea class="widefat" id="<?php echo $this->get_field_id('code1'); ?>" name="<?php echo $this->get_field_name('code1'); ?>"><?php echo attribute_escape($code1); ?></textarea></label></p>
       	<p><label for="<?php echo $this->get_field_id('code2'); ?>">Ads Banner 2 Code: <textarea class="widefat" id="<?php echo $this->get_field_id('code2'); ?>" name="<?php echo $this->get_field_name('code2'); ?>"><?php echo attribute_escape($code2); ?></textarea></label></p>
        <p><label for="<?php echo $this->get_field_id('code3'); ?>">Ads Banner 3 Code: <textarea class="widefat" id="<?php echo $this->get_field_id('code3'); ?>" name="<?php echo $this->get_field_name('code3'); ?>"><?php echo attribute_escape($code3); ?></textarea></label></p>
        <p><label for="<?php echo $this->get_field_id('code4'); ?>">Ads Banner 4 Code: <textarea class="widefat" id="<?php echo $this->get_field_id('code4'); ?>" name="<?php echo $this->get_field_name('code4'); ?>"><?php echo attribute_escape($code4); ?></textarea></label></p>
        <?php
	}
	function update($new_instance, $old_instance){
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['code1'] = $new_instance['code1'];
		$instance['code2'] = $new_instance['code2'];
		$instance['code3'] = $new_instance['code3'];
		$instance['code4'] = $new_instance['code4'];
		return $instance;
	}
	function widget($args, $instance){
		extract($args, EXTR_SKIP);
		echo $before_widget;
		$title = empty($instance['title']) ? ' ' : apply_filters('widget_title', $instance['title']);
		$code1 = empty($instance['code1']) ? '<img src="'.SITE_URL.'/images/ads_125x125.png" />' : apply_filters('widget_title', $instance['code1']);
		$code2 = empty($instance['code2']) ? '<img src="'.SITE_URL.'/images/ads_125x125.png" />' : apply_filters('widget_title', $instance['code2']);
		$code3 = empty($instance['code3']) ? '<img src="'.SITE_URL.'/images/ads_125x125.png" />' : apply_filters('widget_title', $instance['code3']);
		$code4 = empty($instance['code4']) ? '<img src="'.SITE_URL.'/images/ads_125x125.png" />' : apply_filters('widget_title', $instance['code4']);
		
	 
		if (!empty($title))
		  echo $before_title . $title . $after_title;;
	 
		// WIDGET CODE GOES HERE
		?>
        
        <ul class="ads_125x125">
        	<li><?php echo html_entity_decode($code1) ?></li>
            <li><?php echo html_entity_decode($code2) ?></li>
            <li><?php echo html_entity_decode($code3) ?></li>
            <li><?php echo html_entity_decode($code4) ?></li>
        </ul>
        <?php
	 
		echo $after_widget;
	}
}
add_action( 'widgets_init', create_function('', 'return register_widget("Ads_125x125");') );?>