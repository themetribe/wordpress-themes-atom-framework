(function() {
    tinymce.create('tinymce.plugins.shortcodes', {  
        init : function(ed, url) {  
            ed.addButton('blog', {  
                title : 'Add a Blog',  
                image : url+'/../includes/shortcodes/images/blogger.png',  
                onclick : function() { 
					loadOptions('blog');
                }  
            });  
			ed.addButton('box', {  
                title : 'Add a Box',  
                image : url+'/../includes/shortcodes/images/box.png',  
                onclick : function() {  
                     loadOptions('box');
                }  
            });  
			ed.addButton('button', {  
                title : 'Add a Button',  
                image : url+'/../includes/shortcodes/images/btn.png',  
                onclick : function() {  
                     loadOptions('button');
                }  
            });  
			ed.addButton('gchart', {  
                title : 'Add Google Chart',  
                image : url+'/../includes/shortcodes/images/chart.png',  
                onclick : function() {  
                     loadOptions('gchart');
                }  
            }); 
			ed.addButton('gmap', {  
                title : 'Add Google Map',  
                image : url+'/../includes/shortcodes/images/map.png',  
                onclick : function() {  
                     loadOptions('gmap');
                }  
            });  
			ed.addButton('image', {  
                title : 'Add Image Shortcode',  
                image : url+'/../includes/shortcodes/images/image.png',  
                onclick : function() {  
                     loadOptions('image');
                }  
            });  
			ed.addButton('sidebar', {  
                title : 'Add Sidebar',  
                image : url+'/../includes/shortcodes/images/sidebar.png',  
                onclick : function() {  
                     loadOptions('sidebar');
                }  
            });  
			ed.addButton('slider', {  
                title : 'Add Slider',  
                image : url+'/../includes/shortcodes/images/slider.png',  
                onclick : function() {  
                     loadOptions('slider');
                }  
            });  
			ed.addButton('video', {  
                title : 'Add Video Shortcode',  
                image : url+'/../includes/shortcodes/images/video.png',  
                onclick : function() {  
                     loadOptions('video');
                }  
            });  
        },  
        createControl : function(n, cm) {  
            return null;  
        },  
    });  
    tinymce.PluginManager.add('shortcodes', tinymce.plugins.shortcodes);  
})(); 
function ucwords (str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}