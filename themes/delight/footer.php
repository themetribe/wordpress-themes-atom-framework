<?php global $themePostOptions, $savedFiles; extract($savedFiles); extract($themePostOptions); ?>

<footer id="footer" class="wrapper">
	<?php 
	if($dl_footerWidgets!='disable') : ?>
	<section id="footer-widget" class="wrapper clearfix">
    	<!-- footer 4 widgets here -->
        <?php 
		  $arr = array('first','second','third','fourth');
		  foreach($arr as $key=>$val):
		  if ( is_active_sidebar( $val.'-footer-widget-area' ) ) : ?>
		  <div class="<?php echo $val ?> widget-area">
			  <ul class="xoxo">
				  <?php dynamic_sidebar( $val.'-footer-widget-area' ); ?>
			  </ul>
		  </div>
		  <?php endif; endforeach; ?>
    </section>
    <?php endif; ?>
    <section id="generator" class="wrapper">
    	Powered by <a href="#">Wordpress</a> and <a href="http://themefanatics.com">Themefanatics</a>
    </section>
</footer>

</body>
<?php wp_footer(); ?>
</html>