<?php

global $themePostOptions, $savedFiles;

# =========== Initialization =============================================================================================

//Long posts should require a higher limit.
@ini_set('pcre.backtrack_limit', 500000);

define( 'SITE_URL',get_template_directory_uri());
define( 'SITE_DIR', get_template_directory() );
define( 'SHORTNAME', 'dl_');
define( 'THEMENAME', 'Delight');

include("includes/scripts.php");
include("includes/options-functions.php");

$themePostOptions = get_option("themePostOptions");
$savedFiles = get_option("themeFilesOptions");

#Load Admin Styles
add_action('admin_init','load_admin_styles');
function load_admin_styles(){
	wp_enqueue_style('shortcode',SITE_URL.'/css/admin.css');
}

#Load Build In Settings
include('settings/side_generator.php');
include('settings/shortcode_script.php');

#Load Plugin
include('includes/plugins/ads125x125.php');
include('includes/plugins/recent_post.php');
include('includes/plugins/contact_info.php');
include('includes/plugins/contact_us.php');

#Load Shortcode
include('includes/shortcodes/slider.php');
include('includes/shortcodes/columns.php');
include('includes/shortcodes/image.php');
include('includes/shortcodes/box.php');
include('includes/shortcodes/button.php');
include('includes/shortcodes/sidebar.php');
include('includes/shortcodes/blog.php');
include('includes/shortcodes/typography.php');
include('includes/shortcodes/contactform.php');
include('includes/shortcodes/gallery.php');
include('includes/shortcodes/gmap.php');
include('includes/shortcodes/gchart.php');
include('includes/shortcodes/table.php');
include('includes/shortcodes/video.php');

register_nav_menu( 'primary', __( 'Primary Menu', 'atom' ) );
add_theme_support( 'post-thumbnails', array( 'post' ) );
add_post_type_support('page', 'excerpt');

register_sidebar( array(
	'name' => __( 'Default Sidebar', 'atom' ),
	'id' => 'default-sidebar',
	'description' => __( 'The default sidebar', 'atom' ),
	'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );
register_sidebar( array(
	'name' => __( 'First Footer Widget Area', 'atom' ),
	'id' => 'first-footer-widget-area',
	'description' => __( 'The first footer widget area', 'atom' ),
	'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );
register_sidebar( array(
	'name' => __( 'Second Footer Widget Area', 'atom' ),
	'id' => 'second-footer-widget-area',
	'description' => __( 'The second footer widget area', 'atom' ),
	'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );
register_sidebar( array(
	'name' => __( 'Third Footer Widget Area', 'atom' ),
	'id' => 'third-footer-widget-area',
	'description' => __( 'The third footer widget area', 'atom' ),
	'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );
register_sidebar( array(
	'name' => __( 'Fourth Footer Widget Area', 'atom' ),
	'id' => 'fourth-footer-widget-area',
	'description' => __( 'The fourth footer widget area', 'atom' ),
	'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );

# Include JS Scripts
add_action('get_header','jsinclude');
function jsinclude(){	
	wp_enqueue_script("jquery"); 
	wp_enqueue_script( 'superfish', SITE_URL.'/js/superfish.js');
	wp_enqueue_script( 'supersubs', SITE_URL.'/js/supersubs.js');
	wp_enqueue_script( 'easing', SITE_URL.'/js/easing.js');
	wp_enqueue_script( 'cycle', SITE_URL.'/js/cycle.js');	
}

# Browser Detection for Body Class
add_filter('body_class','browser_body_class');
function browser_body_class($classes) 
{
	global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

	if($is_lynx) $classes[] = 'lynx';
	elseif($is_gecko) $classes[] = 'gecko';
	elseif($is_opera) $classes[] = 'opera';
	elseif($is_NS4) $classes[] = 'ns4';
	elseif($is_safari) $classes[] = 'safari';
	elseif($is_chrome) $classes[] = 'chrome';
	elseif($is_IE) $classes[] = 'ie';
	else $classes[] = 'unknown';

	if($is_iphone) $classes[] = 'iphone';
	return $classes;
}

# Shortcode TinyMCE
add_action('init', 'add_sc_tinymce'); 
function add_sc_tinymce() {  
	if ( current_user_can('edit_posts') &&  current_user_can('edit_pages') )  
	{  
		add_filter('mce_external_plugins', 'add_plugin');  
		add_filter('mce_buttons', 'register_button'); 
		add_action('edit_form_advanced', 'shortcode');
		add_action('edit_page_form', 'shortcode'); 
	}  
}  
function register_button($buttons) {  
   array_push($buttons, "blog");  
   array_push($buttons, "box");  
   array_push($buttons, "button");  
   array_push($buttons, "gchart");  
   array_push($buttons, "gmap");  
   array_push($buttons, "image");  
   array_push($buttons, "sidebar");  
   array_push($buttons, "slider");  
   array_push($buttons, "video");  
   return $buttons;  
}  
function add_plugin($plugin_array) {  
   $plugin_array['shortcodes'] = get_bloginfo('template_url').'/js/shortcodes.js';  
   return $plugin_array;  
}  
function header_code(){
	global $themePostOptions; extract($themePostOptions);
	if($dl_set_header_code=="enable"){
		echo $dl_header_code;
	}
}
function footer_code(){
	global $themePostOptions; extract($themePostOptions);
	if($dl_set_footer_code=="enable"){
		echo $dl_footer_code;
	}
}
add_action('wp_head','header_code');
add_action('wp_footer','footer_code');


# ============= General Functions ========================================================================================

function labtitles()
{
	global $page, $paged, $themePostOptions;
	$metaTitleSep = (trim($themePostOptions['dl_metaTitleSep'])!='') ? $themePostOptions['dl_metaTitleSep'] : '|' ;
	wp_title( $metaTitleSep, true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyeleven' ), max( $paged, $page ) );
}
function get_featured_image($post_id='',$size='full',$ttatr='h=150&w=150&zc=1',$source='',$display=TRUE)
{
	if($post_id==''){
		$img = array($source);
	}
	else{
		$img = wp_get_attachment_image_src(get_post_thumbnail_id($post_id),$size);		
	}
	if($display==TRUE && has_post_thumbnail()){
		
		if($size!='full'){echo "<img src='".SITE_URL."/timthumb.php?src=".$img[0]."&".$ttatr."' alt='".get_the_title()."' title='".get_the_title()."'>";}
		else{the_post_thumbnail();}
	}
	else{return SITE_URL."/timthumb.php?src=".$img[0]."&".$ttatr;}
}
function patico_truncate($content, $limit, $more='&hellip;')
{
	if(strlen($content)<$limit)
	{
		$more="";
	}
	return substr_replace($content, '', $limit)."".$more;
}
function patico_get_the_content() 
{	
	ob_start();
	the_excerpt();
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}
function post_meta_date(){
	global $post;
	?>
        	<span class="M"><?php echo esc_attr( get_the_date('M') ) ?></span>
            <span class="j"><?php echo esc_attr( get_the_date('j') ) ?>,</span>
            <span class="y"><?php echo esc_attr( get_the_date('Y') ) ?></span>
    <?php
}
function post_meta()
{
	global $post;
	$categories_list = get_the_category_list( __( ', ', 'patico' ) );
	?>
    <div class="post_meta_s1"> 
    	<span class="category"><?php echo $categories_list ?></span>   	
    	<span>on</span>
    	<a href="<?php the_permalink() ?>" class="date">
        	<?php post_meta_date() ?>
        </a>
        <span class="comment"><?php echo $comment->approved ?></span>
    </div>
    <?php	
}
function breadcrumbs()
{ ?>
	<div id="breadcrumbs">
    	Browse : 
      <a href="<?php bloginfo('url'); ?>"><?php _e('Home','atom') ?></a> <span class="raquo">&raquo;</span>      
      <?php if( is_tag() ) { ?>
          <?php _e('Posts Tagged ','atom') ?><span class="raquo">&quot;</span><?php single_tag_title(); echo('&quot;'); ?>
      <?php } elseif (is_day()) { ?>
          <?php _e('Posts made in','atom') ?> <?php the_time('F jS, Y'); ?>
      <?php } elseif (is_month()) { ?>
          <?php _e('Posts made in','atom') ?> <?php the_time('F, Y'); ?>
      <?php } elseif (is_year()) { ?>
          <?php _e('Posts made in','atom') ?> <?php the_time('Y'); ?>
      <?php } elseif (is_search()) { ?>
          <?php _e('Search results for','atom') ?> <?php the_search_query() ?>
      <?php } elseif (is_single()) { ?>
          <?php $category = get_the_category();
                $catlink = get_category_link( $category[0]->cat_ID );
                echo ('<a href="'.$catlink.'">'.$category[0]->cat_name.'</a> '.'<span class="raquo">&raquo;</span> '.get_the_title()); ?>
      <?php } elseif (is_category()) { ?>
          <?php single_cat_title(); ?>
      <?php } elseif (is_author()) { ?>
          <?php _e('Posts by ','atom'); echo ' ',$curauth->nickname; ?>
      <?php } elseif (is_page()) { ?>
          <?php wp_title(''); ?>
      <?php }; ?>
    </div> <!-- end #breadcrumbs -->
    <?php 
}
function atom_comment( $comment, $args, $depth ) 
{
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'twentyeleven' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<article id="comment-<?php comment_ID(); ?>" class="comment clearfix">
			<footer class="comment-meta">
				<div class="comment-author vcard">
					<?php
						$avatar_size = 60;
						if ( '0' != $comment->comment_parent )
							$avatar_size = 60;

						echo get_avatar( $comment, $avatar_size );

						/* translators: 1: comment author, 2: date and time */
						printf( __( '%1$s <span class="comment-date">%2$s</span> ', 'atom' ),
							sprintf( '<span class="fn">%s</span>', get_comment_author_link() ),
							sprintf( '<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
								esc_url( get_comment_link( $comment->comment_ID ) ),
								get_comment_time( 'c' ),
								/* translators: 1: date, 2: time */
								sprintf( __( '%1$s at %2$s', 'atom' ), get_comment_date(), get_comment_time() )
							)
						);
					?>

					<?php edit_comment_link( __( 'Edit | ', 'atom' ), '<span class="edit-link">', '</span>' ); ?>
                    <div class="comment-content"><?php comment_text(); ?></div>

                    <div class="reply">
                        <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'atom' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
                    </div><!-- .reply -->
				</div><!-- .comment-author .vcard -->
				
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'atom' ); ?></em>
					<br />
				<?php endif; ?>

			</footer>
		</article><!-- #comment-## -->

	<?php
			break;
	endswitch;
}
function atom_pagination($pages = '', $range = 2, $label = ''){
	$showitems = ($range * 2)+1;
	global $paged;
	if(empty($paged)) 
		$paged = 1;
	 
	if($pages == '')
	{
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if(!$pages)
		{
			$pages = 1;
		}
	}	 
	if(1 != $pages)
	{
		if($label!='')
			echo "<span class='textpage'>$label</span>";
		if($paged > 2 && $paged > $range+1 && $showitems < $pages) 
			echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
		if($paged > 1 && $showitems < $pages) 
			echo "<a href='.get_pagenum_link($paged – 1).'>&lsaquo;</a>";
	 
		for ($i=1; $i <= $pages; $i++)
		{
			if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
			{
				echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
			}
		}
	 
		if ($paged < $pages && $showitems < $pages) 
			echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";
		if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) 
			echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";		
	}
}

# =========== Theme Options  =============================================================================================