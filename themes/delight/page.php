<?php get_header(); ?>
<div id="main" class="wrapper clearfix">
	<div id="content">
    <?php
	if(have_posts()) : while(have_posts()): the_post(); ?>
		<div class="clearfix entry">        	
            <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title() ?></a></h3>             	
            <?php if(has_post_thumbnail()): ?>
            	<div class="thumb"><a href="<?php the_permalink(); ?>"><?php get_featured_image($post->ID,'large','h=180&w=294&zc=1') ?></a></div>            
            <?php endif; ?>
            <div class="excerpt">
            	<?php the_content() ?>
            </div>
        </div>
		<?php endwhile; endif; ?>        
        <?php wp_reset_query(); ?>
    </div>
    <?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>