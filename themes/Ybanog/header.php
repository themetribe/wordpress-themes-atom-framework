<!DOCTYPE HTML>
<html lang="en">
<head>
	<title><?php wp_title( '|', true, 'right' );?></title>
	<meta charset="utf-8">
	<meta name="author" content="Themetribe">
	
	<?php wp_head(); ?>
	
	<link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/base/css/bootstrap.css" />
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" />	
	
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	
</head>
<body <?php body_class(); ?>>

	<section id="header">
		<div class="container">

			<!-- SITE LOGO and Description -->
			<section id='branding'>
                <div class="search-div">
                    <div class='search-box'>
						<img src="<?php bloginfo('template_directory');?>/images/search-icon.png" alt="">
						<input type='text' placeholder='What are you looking for?' />
					</div>
					<div class='clearfix'></div>
                    <div class='cart-icon'>
						<img src="<?php bloginfo('template_directory');?>/images/cart-icon.png" alt="">
						<span><b>0</b> items in your cart.</span>
					</div>
                </div>
				<div class='row centered'>
					<div class='span12'>
                    <!-- <img src='<?php //bloginfo('template_directory');?>/images/logo.png'> -->
					<?php site_title_logo(); ?>
					</div>					
				</div>
				<div class='row centered'>
					<div class='span12'>
						<?php site_description(); ?>
					</div>
				</div>

			</section>
			<!-- end Site Logo and Description -->



			<!-- MENU -->
			<div class='row centered'>
				<nav id="access" class='span12'>
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'sf-menu' ) ); ?>
				</nav>	
			</div>
			<!-- end Menu -->

		</div>
	</section>
