<?php global $post, $product;?>
            <div class="span3 card <?php echo "post-".$post->ID; ?>">
            <div class="face modal-trigger">
			<p class='id' style='display:none;'><?php echo $post->ID?></p>
                <?php if ($product->is_on_sale()) : ?><div class="banner-sale"><img src="<?php bloginfo('template_directory');?>/images/sale.png" alt=""></div><?php endif; ?>
                <?php  if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                    the_post_thumbnail();
                    } ?></div>
            <div class="desc">
		
                <p class='item-title'><?php echo the_title(); ?></p>
                <p class='item-cat'><?php
        $size = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
        echo $product->get_categories( ', ', '<span class="posted_in">' . _n( '', '', $size, 'woocommerce' ) . ' ', '.</span>' );
    ?></p>
                <p class='item-price'>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

    <p itemprop="price" class="price">Price: <?php echo $product->get_price_html(); ?></p>

    <meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
    <link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />

</div></p>
            </div>
        </div>
