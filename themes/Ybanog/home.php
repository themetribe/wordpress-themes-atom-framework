<?php get_header(); ?>
<div class="container">
    <div class='img-slider-space'>
    </div>

    <div class='row product-header'>
        <div class='prod-title'>FEATURED PRODUCTS</div>
        <div class="arrows"> 
            <div class="left-arrow arrow" id="prev_featured"></div>
            <div class="right-arrow arrow"id="next_featured"></div>
        </div>
    </div>
    <?php $loop = getFeatured(); ?>
            <div id="featured_products" class='cycle-slideshow row item-list' 
	data-cycle-fx=carousel
	data-cycle-slides="> div"
    data-cycle-timeout=0
    data-cycle-carousel-visible=4
    data-cycle-next="#next_featured"
	data-cycle-prev="#prev_featured"
>
	   <?php $count_featured = -1;?>
           <?php while($loop->have_posts()) : $loop->the_post(); ?>
		<?php $count_featured++;
			if($count_featured%4 == 0){
			//echo "<div class='row item-list' >";		
			}
		?>
            <?php include('loop-template.php'); ?>
		<?php if($count_featured%4 == 3){
			//echo "</div>";	
			}
				
		?>
            <?php endwhile; ?>
		<?php //if($count_featured%4<3)echo "</div>"?>
 </div>
    
        <div class='row product-header'>
        <div class='prod-title'>LATEST PRODUCTS</div>
        <div class="arrows"> 
            <div class="left-arrow arrow" id="prev_recent"></div>
            <div class="right-arrow arrow" id="next_recent"></div>
        </div>
    </div>
    <?php $loop = getRecent(); ?>
<div id="recent_products" class='cycle-slideshow row item-list' 
	data-cycle-fx=carousel
	data-cycle-slides="> div"
    data-cycle-timeout=0
    data-cycle-carousel-visible=4
    data-cycle-next="#next_recent"
	data-cycle-prev="#prev_recent"
    >
	   <?php $count_recent = -1;?>
           <?php while($loop->have_posts()) : $loop->the_post(); ?>
		<?php $count_recent++;
			if($count_recent%4 == 0){
			//echo "<div class='row item-list' >";		
			}
		?>
            <?php include('loop-template.php'); ?>
		<?php if($count_recent%4 == 3){
			//echo "</div>";	
			}	
		?>
            <?php endwhile; ?>
		<?php //if($count_recent%4<3)echo "</div>"?>
 </div>
    
        <div class='row product-header'>
        <div class='prod-title'>SALE PRODUCTS</div>
        <div class="arrows"> 
            <div class="left-arrow arrow" id="prev_sale"></div>
            <div class="right-arrow arrow" id="next_sale"></div>
        </div>
    </div>
    <?php $on_sale = getOnSale();?>
<div id='sale_products' class='cycle-slideshow row item-list'
	data-cycle-fx=carousel
	data-cycle-slides="> div"
    data-cycle-timeout=0
    data-cycle-carousel-visible=4
    data-cycle-next="#next_sale"
	data-cycle-prev="#prev_sale"
>
    <?php
	$count_sale = -1;
        foreach($on_sale as $sale){
		$count_sale++;
		if($count_sale%4 == 0){
			//echo "<div class='row item-list' >";		
		}
            $product_post = get_product_on_sale_post($sale);?>
            <?php if(!is_null($product_post)) : ?>
            <?php while($product_post->have_posts()) : $product_post->the_post(); ?>
            <?php if ($product->is_on_sale()) : ?>
            <?php include('loop-template.php'); ?>
            <?php endif; ?>
            <?php endwhile; ?>
            <?php endif; ?>
            <?php if($count_sale%4 == 3){
			//echo "</div>";
		}
		
	    ?>
        <?php }echo $count_sale;?>
	<?php //if($count_sale%4<3)echo "</div>"?>    
 </div>
    
    <div class="row">
        <div class="span6">
            <p class='bottom-p'><img src="<?php bloginfo('template_directory');?>/images/bullet.png" alt=""> shipping on orders over $100</p>
            <p class='bottom-p'><img src="<?php bloginfo('template_directory');?>/images/bullet.png" alt=""> Call us! toll free x-xxx-xxx-xxx</p>
        </div>
        <div class="span6"><p>We accpet payments thru the following</p></div>
    </div>

</div>
<?php get_footer(); ?>
