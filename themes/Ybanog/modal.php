<?php //global $post, $product;?>

<h1 class='modal-title'><?php echo the_title(); ?></h1>
            <div class="span7 card-modal <?php echo "post-".$post->ID; ?>">
            <div class="face">
                <?php if ($product->is_on_sale()) : ?><div class="banner-sale"><img src="<?php bloginfo('template_directory');?>/images/sale.png" alt=""></div><?php endif; ?>
                <?php  if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
                    the_post_thumbnail();
                    } ?></div>
            <div class="desc">

                <div><?php echo the_content(); ?></div> 
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">

    <p itemprop="price" class="price"><span>Price:</span> <?php echo $product->get_price_html(); ?></p>

    <!--meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
    <link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" /-->

</div>

	<div>Qty: <input type="text"><button class="btn btn-primary btn-buy" style="margin: 0 0px 0 10px;" data-loading-text="adding to cart..">BUY NOW</button></div>
<!-- AddThis Button BEGIN -->
						<div class="addthis_toolbox addthis_default_style ">
						<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
						<a class="addthis_button_tweet"></a>
						<a class="addthis_button_pinterest_pinit"></a>
						<a class="addthis_counter addthis_pill_style"></a>
						</div>

<!-- AddThis Button END -->
            </div>
        </div>
