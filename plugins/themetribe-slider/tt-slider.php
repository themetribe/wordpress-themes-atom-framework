<?php

/*

Plugin Name: Themetribe Slider
Plugin URI: http://themetribe.com
Description: Themetribe Slider
Version: 2.5.7
Author: Allan S. Cabusora
Author URI: http://themetribe.com

*/

define("PLUGIN_PATH",plugins_url()."/themetribe-slider");


add_filter( 'cmb_meta_boxes', 'cmb_sample_metaboxes' );
/**
 * Define the metabox and field configurations.
 *
 * @param  array $meta_boxes
 * @return array
 */
function cmb_sample_metaboxes( array $meta_boxes ) {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_cmb_';

	$meta_boxes[] = array(
		'id'         => 'tt_slider_metabox',
		'title'      => 'ThemeTribe Slider',
		'pages'      => array( 'tt_slider', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => 'Background Image',
				'desc' => 'Upload an image or enter an URL.',
				'id'   => $prefix . 'background_image',
				'type' => 'file',
			),
			array(
				'name'    => 'Transition Effect',
				'desc'    => 'field description (optional)',
				'id'      => $prefix . 'transition',
				'type'    => 'radio',
				'options' => array(
					array( 'name' => 'fadeInX', 'value' => 'fadeInX', ),
					array( 'name' => 'bounceInLeft', 'value' => 'bounceInLeft', ),
					array( 'name' => 'bounceInRight', 'value' => 'bounceInRight', ),
					array( 'name' => 'rotateInUpLeft', 'value' => 'rotateInUpLeft', ),
					array( 'name' => 'rotateInUpRight', 'value' => 'rotateInUpRight', ),
					array( 'name' => 'lightSpeedIn', 'value' => 'lightSpeedIn', ),
				),
			),
		),
	);

	$meta_boxes[] = array(
		'id'         => 'about_page_metabox',
		'title'      => 'About Page Metabox',
		'pages'      => array( 'page', ), // Post type
		'context'    => 'normal',
		'priority'   => 'high',
		'show_names' => true, // Show field names on the left
		'show_on'    => array( 'key' => 'id', 'value' => array( 2, ), ), // Specific post IDs to display this metabox
		'fields' => array(
			array(
				'name' => 'Test Text',
				'desc' => 'field description (optional)',
				'id'   => $prefix . 'test_text',
				'type' => 'text',
			),
		)
	);

	// Add other metaboxes as needed

	return $meta_boxes;
}

add_action( 'init', 'cmb_initialize_cmb_meta_boxes', 9999 );
/**
 * Initialize the metabox class.
 */
function cmb_initialize_cmb_meta_boxes() {

	if ( ! class_exists( 'cmb_Meta_Box' ) )
		require_once 'lib/metabox/init.php';

}



function tt_enqueue_scripts(){
	
	wp_enqueue_script( 'modernizr', PLUGIN_PATH. '/js/modernizr.3dtransforms.touch.js', array(), '1.0', true );
	
	wp_dequeue_script('jquery');
	wp_enqueue_script('jquery');
	
	wp_enqueue_script( 'jquery-ui-widget');
	
	
	wp_enqueue_script( 'jquery.event.drag', PLUGIN_PATH. '/js/jquery.event.drag.js', array('jquery'), '1.0', true );
	wp_enqueue_script( 'jquery.translate3d', PLUGIN_PATH. '/js/jquery.translate3d.js', array('jquery'), '1.0', true );
	
	
	wp_enqueue_script( 'jquery.rs.carousel', PLUGIN_PATH. '/js/carousel/jquery.rs.carousel-min.js', array('jquery'), '1.0', true );
	wp_enqueue_script( 'jquery.rs.carousel-autoscroll', PLUGIN_PATH . '/js/carousel/jquery.rs.carousel-autoscroll-min.js', array(), '1.0', true );
	wp_enqueue_script( 'jquery.rs.carousel-continous', PLUGIN_PATH . '/js/carousel/jquery.rs.carousel-continuous-min.js', array(), '1.0', true );
	wp_enqueue_script( 'jquery.rs.carousel-touch', PLUGIN_PATH . '/js/carousel/jquery.rs.carousel-touch-min.js', array(), '1.0', true );
	wp_enqueue_script( 'main.js', PLUGIN_PATH . '/js/main.js', array(), '1.0', true );
	
	
	wp_enqueue_style('carousel.base',PLUGIN_PATH.'/css/base.css');
	wp_enqueue_style('jquery.rs.carousel',PLUGIN_PATH.'/css/rs-carousel-min.css');
	wp_enqueue_style('carousel.demo',PLUGIN_PATH.'/css/demo.css');
	wp_enqueue_style('animate',PLUGIN_PATH.'/css/animate.min.css');
	
}

add_action('wp_enqueue_scripts','tt_enqueue_scripts');


function tt_add_slider(){
	$labels = array(
						'name'	=>	'Slides',
						'singular_name'	=> 'Slider',
						'add_new'	=> 'Add New',
						'add_new_item' => 'Add New Slide',
						'edit_item'	=>	'Edit Slide',
						'new_item' => 'New Slide',
						'view_item' => 'View Slide',
						'search_items' => 'Search  Slides',
						'not_found' => 'No slides found',
						'not_fount_in_trash' =>	'No slides found in Trash',
						'parent_item_colon' => ''
					);
	
	$args = array(
						'labels' => $labels,
						'public' => false,
						'publicly_queryable' => false,
						'show_ui' => true,
						'query_var' => true,
						'rewrite' => true,
						'capability_type' => 'post',
						'hierarchical' => false,
						// 'menu_icon' => '',
						// 'menu_position' => null,
						'supports' => array('title','editor')
					);
	
	register_post_type('tt_slider', $args);
}
add_action('init','tt_add_slider');

function tt_slider($atts){
	$args = array(
					'post_type' => 'tt_slider',
					'post_status' => 'publish',
					'orderby'=>'date',
					'order'=>'asc',
					'posts_per_page' => -1,
				);
	$slider = new WP_Query($args);
	ob_start();
	?>
		<div id="rs-carousel" class="module">
			<ul>
	<?php
		if($slider->have_posts()){
			while($slider->have_posts()): $slider->the_post();
			$post_meta = get_post_meta(get_the_ID());
	?>
				<li data-transition='<?php echo $post_meta['_cmb_transition'][0]; ?>'>					
					
					<span style="position:absolute;min-width: 100%;min-height: 100%;">
						<img src="<?php echo $post_meta['_cmb_background_image'][0]; ?>" style="min-width: 100%; min-height:100%;" />
					</span>					
					<div style="position: relative;padding: 20px 60px;"><?php the_content(); ?></div>
				</li>
	<?php
			endwhile;
	}else{
		// NO POST
	}
	return ob_get_clean();
}
add_shortcode('tt_slider','tt_slider');

?>
