var effects = new Array(
									'fadeIn',
									'fadeInUp',
									'fadeInDown',
									'fadeInLeft',
									'fadeInRight',
									'fadeInUpBig',
									'fadeInDownBig',
									'fadeInLeftBig',
									'fadeInRightBig',
									'fadeOut',
									'fadeOutUp',
									'fadeOutDown',
									'fadeOutLeft',
									'fadeOutRight',
									'fadeOutUpBig',
									'fadeOutDownBig',
									'fadeOutLeftBig',
									'fadeOutRightBig',
									'bounceIn',
									'bounceInDown',
									'bounceInUp',
									'bounceInLeft',
									'bounceInRight',
									'bounceOut',
									'bounceOutDown',
									'bounceOutUp',
									'bounceOutLeft',
									'bounceOutRight',
									'rotateIn',
									'rotateInDownLeft',
									'rotateInDownRight',
									'rotateInUpLeft',
									'rotateInUpRight',
									'rotateOut',
									'rotateOutDownLeft',
									'rotateOutDownRight',
									'rotateOutUpLeft',
									'rotateOutUpRight',
									'lightSpeedIn',
									'lightSpeedOut',
									'hinge',
									'rollIn',
									'rollOut'
								);
								
	var effectsIn= new Array(
									// 'flash',
									// 'bounce',
									// 'shake',
									// 'tada',
									// 'swing',
									// 'wobble',
									// 'wiggle',
									// 'pulse',
									// 'fadeIn',
									// 'fadeInUp',
									// 'fadeInDown',
									// 'fadeInLeft',
									// 'fadeInRight',
									// 'fadeInUpBig',
									// 'fadeInDownBig',
									// 'fadeInLeftBig',
									// 'fadeInRightBig',
									'fadeInX',
									// 'bounceIn',
									// 'bounceInDown',
									// 'bounceInUp',
									'bounceInLeft',
									'bounceInRight',
									// 'rotateIn',
									// 'rotateInDownLeft',
									// 'rotateInDownRight',
									'rotateInUpLeft',
									'rotateInUpRight',
									'lightSpeedIn'
									// 'hinge',
									// 'rollIn'
								);


		jQuery(document).ready(function() {
			curr = 0;
					jQuery('#rs-carousel').find('.rs-carousel-item-active').find("*").each(function(){
						
					trans = jQuery(this).parent().attr('data-transition');
						jQuery(this).addClass("animated "+trans);
					});
			// jQuery('body').removeClass('no-js');
			var trans;
			jQuery('#rs-carousel').carousel({				
				before: function(event, data) {					
					trans = jQuery(this).find('.rs-carousel-item-active').attr('data-transition');
					
					if(curr != jQuery(':rs-carousel').carousel('getIndex')){
						jQuery(this).find('.rs-carousel-item').find("*").each(function(){
							jQuery(this).removeClass();
							rand = effectsIn[Math.floor((Math.random()*effectsIn.length))];
						});
						curr = jQuery(':rs-carousel').carousel('getIndex');
					}
					jQuery(this).find('.rs-carousel-item-active').find("*").each(function(){
						jQuery(this).addClass("animated "+ trans);
					});
				},
				create: function(event, data){
					jQuery(this).find('.rs-carousel-item-active').find("*").each(function(){
						jQuery(this).addClass("animated "+trans);
					});
				},
				autoScroll: false,
				pause: 5000,
				translate3d: true,
				continuous: true,
				touch: true,
				itemsPerTransition: 1,
				pagination: true,
				nextPrevActions: false
			});
		   	// demo.init($('#container'));
		   	jQuery(".rs-carousel-pagination-link").on('click',function(){
		   		jQuery(':rs-carousel').carousel('goToItem', jQuery(this).index())	
		   	})
			
		});